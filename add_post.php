<?php


### TODO
# Enter the correct url to where to change your password.

include("roster_functions.php");
require('htpasswd.php'); 

### SETTINGS
global $reps_file;
global $adminEmail;



# mail to the reps with ids in $ids and the admin
function add_user($name, $email, $areas){
	
	global $reps_file;
	global $alerts;
	global $url;
	global $adminEmail;
	
	$username = "";
	$password = "";
	
	# check if the email address is already in use
	
	$pwmanager = new htpasswd('.htpasswd'); 
	//~ $pwmanager->create('test', 'pass');
	
	# check if the email is already used
	if( in_array(strtolower($email), $pwmanager->emails()) ){
		
		# abort if it is
		return "Unable to add $name: Email already in use.<br><br>\n";
	}
	
	
	
	# load the current rep file
	# read the new reps file to an array
	$reps = array(); # initiate
	$i = 0; # initiate
	$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
	while (!feof($file_handle)) { # for each line in the file
		$line = fgets($file_handle); # get the line
		$reps[$i] = trim($line); # remove all whitespace padding and newlines
		
		# increase the index if the previous line was not empty
		if($reps[$i]){
			$i++; # increase the index
		}
	}
	fclose($file_handle); # close the file handle
	array_pop($reps); # removes the last empty line
	
	
	# open the reps file for writing
	$file_handle = fopen($reps_file, 'w') or die("Can't open file for reading\n"); # open the file
	$added = 0;
        for($i = 0; $i < count($reps); $i++){
		
		# if it is the week after the 'alert zone', add the new user
		# i.e. the new user will be the next user to be alerted about support duty
		if($i == (max($alerts)+1)){
			
			# write the new rep
			fwrite($file_handle, "$name\t$email\t$areas\n");
			$added = 1;
		}
		
		# write the line
		fwrite($file_handle, $reps[$i]."\n");
	}

        # if there were too few users in the list
        if($added == 0){
            fwrite($file_handle, "$name\t$email\t$areas\n");
        }
	fclose($file_handle); # close the file handle
	
	
	# add the user to the htpasswd file
	$htpass = ""; # reset
	$htpass = htpasswd_add(strtolower($name), strtolower($email));
	
	# add the user to the htpasswd file as well
	if($htpass == ""){
		
		# if the adding failed
		return "Unable to add $name.<br>\nSomething went wrong. Obviously...<br><br>\n";
		
	}
	
	
	
	$tmp = explode(" ", $name);
	$firstname = ucfirst($tmp[0]);
	
	mail($email, "Biosupport.se roster: You have been added.", "Hello $firstname\n\nYou have been added to the Biosupport roster. You will get emails reminding you when it is your turn to have support duty. To view the roster, please visit the roster page:\n\n$url\n\nUser: $htpass[0]\nPassword: $htpass[1]\n\nPlease change the password at $url/authman/index.php?page=edit \n\n\nHave a nice day.");
	mail($adminEmail, "Biosupport.se roster: $name has been added.", "Hello\n\n$name has been added to the Biosupport roster.\n\nUser: $htpass[0]\n\n\nHave a smashing day.");
	
	# return the ok signal
	return '0';
		
}




# function to add users to the htpasswd file
function htpasswd_add($name, $email){
	
	# import the htpasswd functions
	#require('htpasswd.php'); 
	
	$pwmanager = new htpasswd('.htpasswd'); 
	//~ $pwmanager->create('test', 'pass');
	
	# get the first name
	$tmp = explode(" ", $name);
	$firstname = $tmp[0];
	$lastname = end($tmp);
	
	
	# check if the username is already taken
	while( (in_array($firstname, $pwmanager->users())) ){
		
		# if it is taken, add a letter from the last name
		$firstname .= $lastname[0];
		
		# remove the first letter in the last name for next iteration
		$lastname =  substr($lastname, 1);
		
	}
	
	
	# generate a password
	$password = generatePassword();
	
	# create the user
	$pwmanager->create($firstname, $password, $name, $email);
	
	# save the file
	$pwmanager->save(); 
	
	return array($firstname, $password);
	
}




# check that both reps are selected
if($_POST['add'] == ""){
	die("Error: Nothing in the add field.");
}


# rename variables
$add = $_POST['add'];
 
$add = explode("\n", $add);

# save the names of the added people
$added = "";

print "<html>";

foreach($add as $row){
	
	# check that the row has the right format
	if( preg_match('/(.+)\t(.+)\t(.+)/', $row, $matches) ){
		
		# add the user
		$error = add_user(cleanStr($matches[1]), $matches[2], $matches[3]);
		
		#print "error: $error<br>";
		
		# add the user, and report if the user was unable to be added
		if($error != '0' ){
			
			# print error message
			print $error;
			
		}else{
			
			# save the name
			$added .= cleanStr($matches[1])."<br>";
		}
		
		
		
	# if it doesnt have the correct format, die
	}else{
		
		die("Error: invalid format<br><br>$row<br><br><br>Should be tab-delimited:<br><br>Name&emsp;&emsp;Email&emsp;&emsp;Expertise<br><br><br>(Do not copy the example, it is not tab signs in it.<br>Write it in a text editor and copy/paste it.)");
		
	}

	
}


# redirect
Print "<h1>Users added:</h1>$added<br><br>Redirecting to <a href=\"addrem.php\">addrem page</a> in 15 seconds.</html>";
header('Refresh: 15; url=addrem.php');




//~ 
//~ # mail the affected users
//~ mail_addAndAdmin($orgReps, $r1, $adminEmail);
//~ 
//~ # redirect to addrem page
//~ header('Refresh: 4; url=addrem.php');

?>
