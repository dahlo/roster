<!DOCTYPE html>
<html><head>

<!-- change the title of the page -->
<title>SBS - Add/Remove Users</title>

<!-- edit header in head.php -->
<?php include('head.php');?>

<!-- edit menu in menu.php -->
<?php include('menu.php');?>

<h4>Add Users</h4>
<form action="add_post.php" method="post">
Tab separated list of people to add, one person per row:<br>
<i>('name'&emsp;&emsp;'email'&emsp;&emsp;'areas of interest'<br>
Ex.<br>
Erik Eriksson&emsp;&emsp;erik@eriksdomain.se&emsp;&emsp;rnaSeq, exomes, php)</i><br>
<textarea name="add" rows="5" cols="65"></textarea><br>
<input type="submit" value="Add users"/>
</form>
<br><br>

<h4>Remove Users</h4>
<form action="rem_post.php" method="post">
Check the users you want to remove <b>on both side of the name</b>:<br>


<?php

### SETTINGS
include("roster_functions.php");
global $reps_file;
$dev = 0; # devel variable. Adds $dev weeks to the time
$week = 604800; # seconds in a week
$currentDate = time() + $dev*$week;

# read the reps file to an array
$reps = array(); # initiate
$i = 0; # initiate
$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	$reps[$i] = trim($line); # remove all whitespace padding and newlines
	
	# increase the index if the previous line was not empty
	if($reps[$i]){
		$i++; # increase the index
	}
}
fclose($file_handle); # close the file handle


# prepare the data
array_pop($reps); # removes the last empty line

# sort the array after name, keeping the name index numbers
asort($reps);

# print the table
print "<table class='addrem'><tr> <td></td>  <td><b>Name</b></td>  <td></td> </tr>";

# for each name
foreach($reps as $pos => $name){
	
	# remove the email
	$tmp = explode("\t", $name);
	$name = $tmp[0];
	
	# print the name
	print "<tr>  <td><input type=\"checkbox\" name=\"rem1[]\" value=\"$pos\"></td>  <td>$name</td>   <td><input type=\"checkbox\" name=\"rem2[]\" value=\"$pos\"></td>  </tr>\n";
	
}

# end the table and print the length control
print "</table>"

?>


<input type="submit" value="Remove users"/>
</form>
<br><br>




<!-- edit footer in foot.php -->
<?php include("foot.php");?>
