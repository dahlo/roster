<!DOCTYPE html>
<html><head>

<!-- change the title of the page -->
<title>SBS - Switch Weeks</title>

<!-- edit header in head.php -->
<?php include('head.php');?>

<!-- edit menu in menu.php -->
<?php include('menu.php');?>

<h4>Switch Weeks</h4>
<br>
Here you can switch weeks with someone else. You will both receive an e-mail informing about the switch. Please do not use this without asking the other person first.
<br><br>
To switch two persons weeks, select one of the persons in the left list, and select the other person in the second list.
<br><br>

<?php

### SETTINGS
include("roster_functions.php");
require('htpasswd.php'); 
global $reps_file;
$dev = 0; # devel variable. Adds $dev weeks to the time
$week = 604800; # seconds in a week
$currentDate = time() + $dev*$week;


# get the full name of the logged in user
$currentUsername = $_SERVER["REMOTE_USER"];
$currentFullname = ""; # initiate
$file_handle = fopen('.htpasswd', 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	
	$line = explode(":", $line);
	
	if($line[0] == $currentUsername){
		$currentFullname = $line[2];
	}
	
}
fclose($file_handle); # close the file handle



# read the reps file to an array
$reps = array(); # initiate
$i = 0; # initiate
$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	$reps[$i] = trim($line); # remove all whitespace padding and newlines
	
	# increase the index if the previous line was not empty
	if($reps[$i]){
		$i++; # increase the index
	}
}
fclose($file_handle); # close the file handle


# prepare the data
array_pop($reps); # removes the last empty line



# start the form
print "<form action='switch_users.php' method='post' enctype='multipart/form-data'>";

# print a row for each user
print "<table class='switch'>\n"; # start the table
print "<tr><td><center><b></b></center></td><td><b><p class='switch' style=\"text-align: left; color:black;\">Person 1</p></b></td><td><b><p class='switch' style=\"text-align: right; color:black;\">Person 2</p></b></td><td><center><b></b></center></td></tr>\n"; # print header

# sort the array after name, keeping the name index numbers
asort($reps);

# for each name
foreach($reps as $pos => $name){
	
	# remove the email
	$tmp = explode("\t", $name);
	$name = $tmp[0];
	
	# check the current users name
	$checked = "";
	if($currentFullname == strtolower($name)){
		$checked = "checked=\"checked\"";
	}
	
	# print the name
	print "<tr><td><center><input type=\"radio\" name=\"rep1\" value=\"$pos\" $checked></center></td><td><p class='switch' style=\"text-align: left; color:black;\">$name</p></td><td><p class='switch' style=\"text-align: right;color:black;\">$name</p></td><td><center><input type=\"radio\" name=\"rep2\" value=\"$pos\"></center></td></tr>\n";
	
}

# end the table and print the length control
print "</table>

<center><input type='submit' value='Switch ' style='height: auto; width: auto'/>

</form></center><br><br>\n";




?>


<!-- edit footer in foot.php -->
<?php include("foot.php");?>
