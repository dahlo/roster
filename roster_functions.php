<?php

# import the settings
require("settings.php");


# get the rep who has support duty in $offset weeks
function getRep($reps, $offset){
	
	# adjust negative values
	if($offset < 0){
		$offset = count($reps) + $offset;
	}
	
	# which array index does that give?
	$i = $offset %  count($reps);
	
	# return the reps name
	return $reps[$i];
	
}



# get the name of the rep who has support duty in $offset weeks
function getRepName($reps, $offset){
	
	# adjust negative values
	if($offset < 0){
		$offset = count($reps) + $offset;
	}
	
	# which array index does that give?
	$i = $offset %  count($reps);
	
	# return the reps name
	$tmp = explode("\t", $reps[$i]);
	return $tmp[0];
	
}


# get the index of the rep who has support duty in $offset weeks
function getRepIndex($reps, $offset){
	
	# get the current date
	$currentDate = time();
	
	# store the number of seconds in a week
	$week = 604800;
	
	# which week does the offset give?
	$weeksSinceEpoch = $currentDate/$week + $offset;
	
	# which array index does that give?
	$i = $weeksSinceEpoch %  count($reps);
	
	# return the reps index number
	return $i;
	
}


# get the email of the rep with index number $i
function getRepEmail($reps, $i){
	
	# return the reps email
	$tmp = explode("\t", $reps[$i]);
	return $tmp[1];
	
}


# get the $n weeks for the rep with index $i
function getRepWeeks($reps, $index, $nrWeeks){
	
	# get the current date
	$currentDate = time();
	
	# store the number of seconds in a week
	$week = 604800;
	
	# initate the array to store the weeks
	$weeks = array();
	for($i = 0; $i < $nrWeeks; $i++){
		
		# store the week
		$weeks[$i] = date("Y-W",($currentDate + ($index + $i*(count($reps)))*$week));

	}
	
	# return the weeks
	return implode(", ", $weeks);
	
}



# get the reps who has support duty in the next $weeks weeks
function getRepInInterval($reps, $weeks){
	
	# initialize
	$repsInterval = array();
	
	# for each rep who has been mailed
	for($i = 0; $i <= $weeks; $i++){
		$repsInterval[$i] = getRep($reps, $i);
	}
	
	
	# return the reps index number
	return $repsInterval;
	
}


# remove special characters
function cleanStr($toClean) {
	$toClean     =     str_replace('&', '-and-', $toClean);
	$toClean     =     str_replace('--', '-', $toClean);
	$toClean     =     str_replace('\'', '', $toClean);

					# array with char replacements
				$GLOBALS['normalizeChars'] = array(
					'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
					'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
					'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
					'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
					'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
					'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
					'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f'
				);

	return strtr($toClean, $GLOBALS['normalizeChars']);
}



//~ $reps_file = "reps.txt";
//~ # read the reps file to an array
//~ $reps = array(); # initiate
//~ $i = 0; # initiate
//~ $file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
//~ while (!feof($file_handle)) { # for each line in the file
	//~ $line = fgets($file_handle); # get the line
	//~ $reps[$i] = trim($line); # remove all whitespace padding and newlines
	//~ #print "$reps[$i]\n";
	//~ 
	//~ # increase the index if the previous line was not empty
	//~ if($reps[$i]){
		//~ $i++; # increase the index
	//~ }
//~ }
//~ fclose($file_handle); # close the file handle
//~ 
//~ 
//~ # prepare the data
//~ array_pop($reps); # removes the last empty line (where does it come from?)
//~ 
//~ print getRepWeeks($reps, 0, 10)."\n";




?>
