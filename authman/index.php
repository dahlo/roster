<?php

/**
 * AuthMan Free
 *
 * @copyright  Copyright (c) 2008 Authman Inc. (http://www.authman.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @link       http://www.authman.com
 * @version    1.0.0
 */

ini_set('zlib.output_compression','Off');
define('AUTHMAN_DIR', dirname(__FILE__));



/**
 * AuthMan Class (Free Version)
 *
 * @copyright  Copyright (c) 2008 Authman Inc. (http://www.authman.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @link       http://www.authman.com
 */
class Authman
{
	/**
     * Class version, const
     *
     * @access private 
     */
	var $AUTHMAN_VERSION = '1.1.0';

	/**
     * Apache ServerRoot
     *
     * @access private 
     * @var string
     */
	var $_serverRoot = '/etc/httpd';

	/**
     * Authman directory
     *
     * private @var string
     */
	var $_basePath;

	/**
     * Default Ini file
     *
     * @access private 
     * @var string
     */
	var $_iniFile = 'config/config.ini';

	/**
     * Config settings
     *
     * @access private 
     * @var array
     */
	var $_configArray = array('language'      => 'english',
								  'login'         => 'admin',
								  'password'      => 'admin',
								  'demo'          => 0,
								  'manual_edit'   => 1,
								  'access_file'   => '.htaccess',
								  'authuser_file' => '.htpasswd',
								  'authname'      => 'Protected by Authman',
								  'allowsignup'   => 0,
                                  'autoapprove'   => 0,
								  'encryption'    =>'md5');

	/**
     * Language array
     *
     * @access private 
     * @var array
     */
	var $_langArray = array('messages'=>array(),
								'warnings'=>array(),
								'errors'=>array());

	/**
     * File Paths
     *
     * @access private 
     * @var array
     */
	var $_filePathsArray = array('configfile'=>null,
									 'langfile'=>null,
									 'accessfile'=>null,
									 'authadminfile'=>null,
									 'authuserfile' =>null,
									 'authgroupfile'=>null,
									 'accessfile_dist'=>null,
									 'authuserfile_dist' =>null,
									 'authgroupfile_dist'=>null,
									 'signupfile'=>null);

	/**
     * Raw File Contents (by url hash)
     * 
     * @access private 
     * @var array
     */
	var $_fileContentsArray = array();

	/**
     * Parsed File Data (by url hash)
     * 
     * @access private 
     * @var array
     */
	var $_fileDataArray = array();

	/**
     * Access Rules Information (by url hash)
     * 
     * @access private 
     * @var array
     */
	var $_accessRulesArray = array();

	/**
	 * Runtime variables
	 * 
     * @access private 
     * @var array
	 */
	var $_runtimeArray = null;

	/**
     * Error message
     *
     * @access private 
     * @var string
     */
	var $_error = null;

    /**
     * @access private
     * @var array
     */
	var $_tplsArray = array();

	/**
     * Authman Constructor
     *
     * @access public
     * @params array|null $config
     * @return void
     */
	function Authman( $config=array() )
	{
		// base prefix
		$this->_basePath = dirname(__FILE__);


		// overwrite ini file name
		if (isset($config['ini'])) {
			$this->_iniFile = $config['ini'];
		}

		// loading ini file
		$iniFilePath = $this->makePath( $this->_basePath, $this->_iniFile );
		$this->_filePathsArray[ 'configfile' ] = $iniFilePath;
		if (is_file($iniFilePath) && is_readable($iniFilePath) ) {
			$cfg = @parse_ini_file( $iniFilePath, false );
			if (isset($cfg)) {
				$this->_configArray = array_merge($this->_configArray, $cfg);
			}
		}

		// overwrite language
		if (isset($config['language'])) {
			$this->_configArray['language'] = $config['language'];
		}

		// loading language file
		$langFilePath = $this->makePath($this->_basePath . DIRECTORY_SEPARATOR . 'languages',
		$this->_configArray['language'] . '.lng' );
		$this->_filePathsArray[ 'langfile' ] = $langFilePath;

		if (!is_file($langFilePath) || !is_readable($langFilePath) ) {
			// we will trying load default language file
			$langFilePath = $this->makePath($this->_basePath . DIRECTORY_SEPARATOR . 'languages',
			'english.lng' );
		}

		if (is_file($langFilePath) && is_readable($langFilePath) ) {
			$this->_langArray = array_merge($this->_langArray, @parse_ini_file($langFilePath, true));
		}

		// set access file path
		$path = $this->makePath( $this->_basePath . DIRECTORY_SEPARATOR . '..',
		$this->_configArray['access_file'] );
		$this->_filePathsArray[ 'accessfile' ] = $path;
		#if (is_file($path) && is_readable($path)) {
		$this->readFileByType( 'accessfile' );
		#}

		// set admin htpasswd file path
		$basedir = $this->_basePath . DIRECTORY_SEPARATOR . 'var';

		$path = $this->makePath( $basedir, '.htadmin' );
		$this->_filePathsArray[ 'authadminfile' ] = $path;

		// set default file paths
		$path = $this->makePath( $basedir, 'htaccess_dist' );
		$this->_filePathsArray[ 'accessfile_dist' ] = $path;

		$path = $this->makePath( $basedir, 'htpasswd_dist' );
		$this->_filePathsArray[ 'authuserfile_dist' ] = $path;

		$path = $this->makePath( $basedir, 'htgroup_dist' );
		$this->_filePathsArray[ 'authgroupfile_dist' ] = $path;

		$path = $this->makePath( $basedir, 'signups' );
		$this->_filePathsArray[ 'signupfile' ] = $path;
		// loading runtime stats
	}

	/**
     * Return class version
     *
     * @access public 
     * @return string
     */
	function getVersion()
	{
		return $this->AUTHMAN_VERSION;
	}

	/**
     * Return true if demo mode is on
     *
     * @access public
     * @return string
     */
	function isDemo()
	{
		return $this->_configArray['demo'] == 1;
	}

	/**
     * Return true if menual edit is allowed
     *
     * @access public
     * @return string
     */
	function isManualEdit()
	{
		return $this->_configArray['manual_edit'] == 1;
	}

    /**
     * Return configuration value by name
     *
     * @access public
     * @param string $name
     * @return string
     */
	function getConfigValue( $name )
	{
		if (!isset($name) || !isset($this->_configArray[$name])) {
			return false;
		}

		return $this->_configArray[$name];
	}

	/***************************************************************************
	* Member zone related functions
	**************************************************************************/

    /**
     * Logging as user
     *
     * @access public
     * @param string $username
     * @param string encpass
     * @return bool
     */
	function loginAs( $username, $encpass )
	{
		$_SESSION['am_u'] = base64_encode( $username );
		$_SESSION['am_c'] = md5( $encpass );
	
		$user = $this->fetchRecordByType( 'authuserfile', $username );
		if (false == $user) {
			$user = $this->fetchRecordByType( 'authadminfile', $username, true );
			if (false == $user) {
				return false;
			}
		
			$err = $this->getError();
	        $this->setRuntimeValue('lastloggedin_ts', time());
	        $this->setRuntimeValue('lastloggedin_ip', $_SERVER['REMOTE_ADDR'], true);
			$this->setError($err);
		}
		
		return true;
	}	
	
	/**
     * Return authenticated user data 
     *
     * @access public
     * @return array|false 
     */
	function getAuthenticatedUser()
	{
		// checking session cookie
		$isadmin = false;
		$user = null;
		$username = $codedpass = $rawpass = null;
		
		if (isset($_SESSION) && isset($_SESSION['am_u']) 
		                     && isset($_SESSION['am_c'])) {

			$username = base64_decode($_SESSION['am_u']);
			$codedpass  = $_SESSION['am_c']; // md5
		} else if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
			$username = $_SERVER['PHP_AUTH_USER'];
			$rawpass  = $_SERVER['PHP_AUTH_PW'];
		}
		if (!isset($username) || $username=='') {
			return false;
		}
		
		// checking admins first
		$user = $this->fetchRecordByType('authadminfile', $username, true);
		if ($user) {
			$isadmin = true;
		} else {
			$user = $this->fetchRecordByType('authuserfile', $username);
		}

		if (false == $user || !isset($user['pass'])) {
			return false;
		}
		
		if (isset($rawpass)) {
			$encpass = $this->htcrypt($rawpass, $user['pass']);
			if ($encpass != $user['pass']) {
				return false;
			}
		} else if (md5($user['pass']) != $codedpass) {
			return false;
		}
		
		$user['isadmin'] = $isadmin;
		return $user;
	}

	/**
     * Return true if user is authenticated
     *
     * @access public
     * @return bool
     */
	function isAuthenticated()
	{
		return false == $this->getAuthenticatedUser() ? false : true;
	}

    /**
    * Return true if user is authenticated by basic auth method
    * 
    * @access public
    * @return bool
    */
    function isAuthenticatedByBasicAuth()
    {
        $user = $this->getAuthenticatedUser();
        if (false == $user) {
            return false;
        }
        
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
            return false;
        }
        
        $encpass = $this->htcrypt($_SERVER['PHP_AUTH_PW'], $user['pass']);
        if ($encpass != $user['pass']) {
                return false;
        }
        
        return true;
    }

	/**
     * Return true if current authenticated user has administrator priviledges
     *
     * @access public
     * @return bool
     */
	function isAdmin()
	{
		$user = $this->getAuthenticatedUser();
		if (false == $user) {
			return false;
		}
		return isset($user['isadmin']) && $user['isadmin'] ? true : false;
	}

	/***************************************************************************
	* Crypt Utilties
	**************************************************************************/

	/**
     * Encrypt text with crypt function
     *
     * @access public
     * @param string $text
     * @param string|null $salt
     * @param string|null $prefix
     * @return string
     */
	function encrypt_saltcrypt( $text, $salt='', $prefix='' )
	{
		if ($salt == 'DES') {
			$salt = CRYPT_STD_DES == 1 ? 'r1' : '';
		}

		if ($salt == 'EXT_DES') {
			$salt = CRYPT_EXT_DES == 1 ? '_J9..pre' : '';
		}

		if ($salt == 'MD5') {
			$salt = CRYPT_MD5 == 1 ? '$1$pre$' : '';
		}

		if ($salt == '') {
			mt_srand((double)microtime()*1000000);

			for ($i=0; $i<CRYPT_SALT_LENGTH; $i++)
			$salt .= substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./",
			mt_rand() & 63, 1);
		}

		return $prefix . crypt($text, $salt);
	}

	/**
     * Encrypt text with crypt function
     *
     * @access public
     * @param string $text
     * @param string|null $salt
     * @return string
     */
	function htcrypt( $text, $salt=null ) 
	{
		$method = $this->_configArray['encryption'];
		if (isset($salt) && substr($salt, 0, 6) == '$apr1$') {
			$method = 'md5';
			$salt = substr($salt, 6);
		}
		
		// apr1-md5
		if ($method == 'md5') {
			if (CRYPT_MD5 == 1) {
				return $this->crypt_apr1_md5($text, $salt);
			}
		}
		
		// DES
		if (!isset($salt)) {
			mt_srand((double)microtime()*1000000);
			for ($i=0; $i<CRYPT_SALT_LENGTH; $i++) {
				$salt .= substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./", mt_rand() & 63, 1);
			}
		}
				
		return crypt($text, $salt);
	}

    /**
     * Encryption function
     *
     * @access public
     * @param string $plainpasswd
     * @param string $salt
     * @return string
     */
	function crypt_apr1_md5($plainpasswd, $salt) {
		if (!isset($salt)) {
		    $salt = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 8);
		} else {
			$salt = substr($salt, 0, 8);
		}
		    
	    $len = strlen($plainpasswd);
	    $text = $plainpasswd.'$apr1$'.$salt;
	    $bin = pack("H32", md5($plainpasswd.$salt.$plainpasswd));
	    for($i = $len; $i > 0; $i -= 16) { $text .= substr($bin, 0, min(16, $i)); }
	    for($i = $len; $i > 0; $i >>= 1) { $text .= ($i & 1) ? chr(0) : $plainpasswd{0}; }
	    $bin = pack("H32", md5($text));
	    for($i = 0; $i < 1000; $i++) {
	        $new = ($i & 1) ? $plainpasswd : $bin;
	        if ($i % 3) $new .= $salt;
	        if ($i % 7) $new .= $plainpasswd;
	        $new .= ($i & 1) ? $bin : $plainpasswd;
	        $bin = pack("H32", md5($new));
	    }
	    $tmp = '';
	    for ($i = 0; $i < 5; $i++) {
	        $k = $i + 6;
	        $j = $i + 12;
	        if ($j == 16) $j = 5;
	        $tmp = $bin[$i].$bin[$k].$bin[$j].$tmp;
	    }
	    $tmp = chr(0).chr(0).$bin[11].$tmp;
	    $tmp = strtr(strrev(substr(base64_encode($tmp), 2)),
	    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
	    "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
	    return "$"."apr1"."$".$salt."$".$tmp;
	}
	
	/***************************************************************************
	* File System Utilties
	**************************************************************************/

	/**
     * Construct path from base path and file name
     *
     * @access public
     * @param string $basePath
     * @param string $fileName
     * @param string|null $dirsep
     * @return string
     */
	function makePath($basePath, $fileName, $dirsep=DIRECTORY_SEPARATOR)
	{
		// Windows
		if (preg_match('/^[A-Z]:\\\/i', $fileName)) {
			return $this->getNormalizedPath( $fileName );
		}
		// Unix and other
		if (substr($fileName,0,1)=='/' || substr($fileName,0,1)==$dirsep) {
			return $this->getNormalizedPath( $fileName );
		}

		return $this->getNormalizedPath( $basePath . $dirsep . $fileName );
	}

	/**
     * Normalize given path
     *
     * @access public
     * @param mixed $path
     * @param string|null $dirsep
     * @return mixed
     */
	function getNormalizedPath( $path, $dirsep=DIRECTORY_SEPARATOR )
	{
        if (is_array($path)) {
            $pathArray = array();
            foreach($path as $p) {
                $pathArray[] = $this->getNormalizedPath($p, $dirsep);
            }
            return $pathArray;
        }
        
		$prefix = '';
		
        // if not Windows
		if (!preg_match('/^[A-Z]:\\\/i', $path)) {
			$prefix = $dirsep;
		}

		$path = str_replace('/',  $dirsep, $path);
		$path = str_replace('\\', $dirsep, $path);

		$parts = explode($dirsep, $path);

		$todown=0;
		for ($i = count($parts)-1; $i >= 0; $i--) {
			if (empty($parts[$i]) || $parts[$i] == '.') {
				array_splice( $parts, $i, 1);
				continue;
			}
			if ($parts[$i] == '..') {
				array_splice( $parts, $i, 1);
				if ($i > 0) {
					$todown++;
				}
				continue;
			}
			if ($todown) {
				# warning: not works for complex paths like /root/path/../path/../../the/end
				array_splice( $parts, $i-($todown-1), $todown);
				$todown = 0;
			}
		}

		return $prefix . implode( $dirsep, $parts );
	}

	/**
     * Return full path to a file specified by type
     * 
     * @access public
     * @param string $filetype
     * @param string|null $dir
     * @return string
     */
	function getPathByType( $filetype, $dir=DIRECTORY_SEPARATOR )
	{
		$ln = strtolower($filetype);

		if ($ln == 'protecteddirectory') {
			return $this->getNormalizedPath($this->_basePath . $dir . '..');
		}

		if ($ln == 'phpmailer') {
			return implode($dir, array($this->_basePath, 'contrib',
			               'phpmailer', 'class.phpmailer.php'));
		}
		if ($ln == 'tinymcejs') {
			return implode($dir, array($this->_basePath,
			               'contrib', 'tinymce', 'jscripts',
			               'tiny_mce', 'tiny_mce.js'));
		}
        if ($ln == 'magpierss') {
            return implode($dir, array($this->_basePath, 'contrib',
                           'magpierss', 'rss_fetch.inc'));
        }
		if (!isset($this->_filePathsArray[$filetype])) {
			return false;
		}

		return $this->_filePathsArray[$filetype];
	}

    /**
     * Return default file path by type
     *
     * @access public
     * @param string $filetype
     * @return string
     */
	function getDefaultFilePathByType( $filetype )
	{
		if ($filetype == 'authuserfile') {
			return dirname($this->getPathByType('accessfile'))
			. DIRECTORY_SEPARATOR
			. $this->getConfigValue('authuser_file');
		}
		return false;
	}

    /**
     * Returns url by type 
     *
     * @access public
     * @param string $filetype
     * @param string|null $fulurl
     * @return strung
     */
	function getUrlByType( $filetype, $fullurl=false )
	{
		// base uri
		$server = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
		$port   = isset($_SERVER['SERVER_PORT']) ? $_SERVER['SERVER_PORT'] : 80;

		$uri_self = $_SERVER['PHP_SELF'];
		$uri = dirname($uri_self);

		if ($filetype == 'base') {
			$uri .= '/';
		}
		if ($filetype == 'protected') {
			$uri = $this->getNormalizedPath($uri . '/../', '/');
		}
		if ($filetype == 'errordocument401') {
			$uri = $uri_self .'?page=401';
		}

		if ($filetype == 'login') {
			$uri = $uri_self .'?page=login';
		}

		if ($filetype == 'tinymcejs') {
			$uri .= '/contrib/tinymce/jscripts/tiny_mce/tiny_mce.js';
		}

		if (!$fullurl) {
			return $uri;
		}

		return 'http://'.$server . ($port != 80 ? ':'.$port : '') . $uri;
	}

    /**
     * Set file path
     *
     * @access public
     * @param string $filetype
     * @param string $filepath
     * @return void
     */
	function setFilePathByType( $filetype, $filepath )
	{
		if (!isset($filepath)) {
			return false;
		}
		$hash = md5( $filepath );

		$this->_fileContentsArray[ $hash ] = null;
		$this->_fileDataArray[ $hash ] = null;
		$this->_filePathsArray[ $filetype ] = $filepath;
	}

	/**
     * Return temporary file name
     *
     * @access public
     * @return string
     */
	function getTempFilePath()
	{
		return $this->_basePath . DIRECTORY_SEPARATOR . 't_' . time() . rand(0, 1000);
	}

	/***************************************************************************
	* String functions
	**************************************************************************/


	/***************************************************************************
	* Disk Level File Operations
	**************************************************************************/

    /**
     * Write default deny file to disk
     *
     * @access private
     * @param string $filePath
     * @return bool
     */
	function writeDenyFile( $filePath )
	{
		$fh = @fopen($filePath, 'w');
		if (false == $fh) {
			return false;
		}
		@fwrite($fh, "# Automatically Created by Autman Free\n");
		@fwrite($fh, "order deny,allow\n");
		@fwrite($fh, "deny from all\n");
		@fclose($fh);

		return true;
	}

	/**
     * Read and parse file 
     *
     * @access private
     * @param string $filetype
     * @return bool 
     */
	function readFileByType( $filetype )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			return false;
		}

		$hash = md5($path);
		$this->_fileContentsArray[$hash] = array();
		$this->_fileDataArray[$hash] = array();

		if (!is_file($path)) {
			$this->setError( $this->E('NOSUCHFILE', $path) );
			return false;
		}

		if (is_file($path) && !is_readable($path)) {
			$this->setError( $this->E('FILENOTREADBLE', $path) );
			return false;
		}

		$fh = @fopen($path, 'r');
		if (false == $fh) {
			$this->setError( $this->E('FILEOPENFAILED', $path) );
			return false;
		}

		$contents = $buf = null;
		$maxbytes = 2048000;
		$bytes = 0;
		while (!feof($fh) && $bytes < $maxbytes && ($buf = fread($fh, $maxbytes-$bytes))) {
			$contents .= $buf;
		}

		fclose($fh);

		return $this->setFileContentsByType( $filetype, $contents );
	}

	/**
     * Save file 
     *
     * @access public
     * @param string $filetype
     * @param bool|null $saveRawData
     * @param string|null $contents
     * @return bool 
     */
	function saveFileByType( $filetype, $saveRawData=false, $contents=null, $parseTemplates=false )
	{
		$path = $this->getPathByType( $filetype );
		if (!$path) {
			return false;
		}

		if (!isset($contents)) {
			$contents = $this->getFileContentsByType( $filetype, $saveRawData, $parseTemplates );
		}

		// Saving in DEMO mode is disabled
		if ($this->isDemo()) {
			$this->setError( $this->W('DEMOISON') );
			return false;
		}

		if (!is_file($path) && !is_writable(dirname($path))) {
			$this->setError( $this->E('DIRNOTWRITABLE', dirname($path)) );
			return false;
		}

		if (is_file($path) && !is_writable($path)) {
			$this->setError( $this->E('FILENOTWRITABLE', $path) );
			return false;
		}

		$fh = @fopen($path, 'w');
		if (!$fh) {
			$this->setError( $this->E('FILEOPENFAILED', $path) );
			return false;
		}

		if ($contents != '') {
			if (!fwrite($fh, $contents)) {
				$this->setError( $this->E('WRITEFAILED', $path) );
				return false;
			}
		}

		@fclose($fh);

		if (!$this->readFileByType( $filetype )) {
			return false;
		}

		return true;
	}

    /**
     * Reset protected directory
     *
     * @access public
     * @retrun bool
     */
	function resetProtectedDirectory()
	{
		$src = $this->getPathByType( 'accessfile_dist');
		$dst = $this->getPathByType( 'accessfile' );

		if (false == $this->copyFile( $src, $dst )) {
			return false;
		}
		$this->readFileByType( 'accessfile' );

		$result = true;

		$filesArray = array('authuserfile'); // , 'authgroupfile');
		foreach ($filesArray as $file) {
			$src = $this->getPathByType( $file . '_dist');
			$dst = $this->getPathByType( $file );

			if (isset($src) && isset($dst) && is_file($src)) {
				if (false == $this->copyFile( $src, $dst )) {
					$result = false;
				}
			}
		}

		return $result;
	}

    /**
     * Copy file 
     *
     * @access public
     * @param string $src
     * @param string $dst
     * @return bool
     */
	function copyFile( $src, $dst )
	{
		if ($src == false) {
			$this->setError( $this->E('INVALIDREQUEST') . ' [source]');
			return false;
		}
		if ($dst == false) {
			$this->setError( $this->E('INVALIDREQUEST') . ' [destination]');
			return false;
		}

		// Saving in DEMO mode is disabled
		if ($this->isDemo()) {
			$this->setError( $this->W('DEMOISON') );
			return false;
		}

		if (!is_file($dst) && !is_writable(dirname($dst))) {
			$this->setError( $this->E('DIRNOTWRITABLE', dirname($dst)) );
			return false;
		}

		if (is_file($dst) && !is_writable($dst)) {
			$this->setError( $this->E('FILENOTWRITABLE', $dst) );
			return false;
		}

		if (!is_file($src) || !is_readable($src)) {
			$this->setError( $this->E('FILENOTREADBLE', $src) );
			return false;
		}

		if (false == copy( $src, $dst )) {
			$this->setError( $this->E('FILECOPYFAILED', $src) );
			return false;
		}

		return true;
	}

	/***************************************************************************
	* Contents
	**************************************************************************/

	/**
     * Return file contents
     *
     * @access public
     * @param string $filetype
     * @param bool|null $getRawData
     * @return string
     */
	function getFileContentsByType( $filetype, $getRawData=false, $parseTemplates=false )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			return false;
		}
		$hash = md5( $path );

		if (false == $getRawData) {
			$data = $this->getFileDataByType( $filetype );
			$text = '';

			// build contents from parsed data
			if ($filetype == 'accessfile') {

				if ($parseTemplates) {
					foreach ($data as $k=>$v) {
						$data[$k] = preg_replace('/%PROTECTEDDIRECTORY%/', $am->getPathByType('protecteddirectory'), $v);
					}
				}
				$text = implode("\n", $data);

			} else if (($filetype=='authuserfile' || $filetype=='authadminfile' ||
			            $filetype=='signupfile') && is_array($data)) {

				foreach ($data as $user) {
					if (isset($user['pass_raw'])) {
//						$pass = $this->encrypt_saltcrypt( $user['pass_raw'], 'DES' );
						$pass = $this->htcrypt( $user['pass_raw'] ); // salt automatically generated
					} else {
						$pass = $user['pass'];
					}

					$text .= $user['name'] . ':' . $pass;
					if (isset($user['info']) || isset($user['email'])) {
						$text .= ':';
						$text .= isset($user['info']) ? $user['info'] . ':' : '';
						if (isset($user['email'])) {
							$text .= $user['email'];
						}
					}

					if ($filetype=='signupfile') {
						$text .= ':' . (isset($user['ts']) ? $user['ts'] : '');
						$text .= ':' . (isset($user['remoteaddr']) ? $user['remoteaddr'] : '');
						$text .= ':' . (isset($user['referer']) ? $user['referer'] : '');
					}

					$text .= "\n";
				}
			}

			return $text;
		}

		/* fetching raw data */

		if (!isset($this->_fileContentsArray[ $hash ])) {
			if (false == $this->readFileByType( $filetype )) {
				return false;
			}
		}

		if (!isset($this->_fileContentsArray[ $hash ])) {
			return '';
		}

        if (!is_array($this->_fileContentsArray[$hash])) {
            return $this->_fileContentsArray[$hash];
        }
        
		return implode("\n", $this->_fileContentsArray[$hash]); 
	}

	/**
     * Parse files
     *
     * @access public
     * @param string $filetype
     * @param string $contents
     * @return bool
     */
	function parseFileContentsByType( $filetype, $contents )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			$this->setError( $this->W('NOTDEFINED', $filetype) );
			return false;
		}
		$hash = md5( $path );

		$this->_fileDataArray[$hash] = array();
        $this->_accessRulesArray[$hash] = array('order'=>null);

		$lArray = split("[\n\r]+", $contents);

		// no data
		if (count($lArray) < 0) {
			return true;
		}

		if ($filetype == 'accessfile') {
			$rows = array();
			$hash = md5( $this->getPathByType( $filetype ) );

			foreach($lArray as $l) {
				if (preg_match('/^\s*Auth(User|Group)File\s+(.+)/i', $l, $matches)) {
					$authfiletype = 'auth' . strtolower($matches[1]) . 'file';
					$path = $this->makePath( $this->_serverRoot, $matches[2] );
					$this->_filePathsArray[ $authfiletype ] = $path;
				}

				if (preg_match('/^\s*AuthType\s+(.+)/i', $l, $matches)) {
					$val = strtolower(trim($matches[1]));
					$val = stripslashes($val);
					$this->_accessRulesArray[$hash]['authtype'] = $val;
				}

				if (preg_match('/^\s*AuthName\s+"?(.+?)"?\s*$/i', $l, $matches)) {
					$val = trim($matches[1]);
					$val = stripslashes( $val );
					$this->_accessRulesArray[$hash]['authname'] = $val;
				}
                
                // ip/domain access rules
                if (preg_match('/^\s*Order\s+(Allow|Deny),\s*(Allow|Deny)\s*$/i', $l, $matches)) {
                    $this->_accessRulesArray[$hash]['order'] = strtolower($matches[2]);
                }
                if (preg_match('/^\s*(allow|deny)\s+from\s+(.+)$/i', $l, $matches)) {
                    foreach(explode(' ', $matches[2]) as $s) {
                        $s = trim($s);
                        if (empty($s)) {
                            continue;
                        }
                        $rule = strtolower($matches[1]);
                        $this->_accessRulesArray[$hash][$rule][] = $s;
                    }
                }

                // error document rules
				if (preg_match('/^\s*ErrorDocument\s+401\s+(.+)$/i', $l, $matches)) {
					$val = trim($matches[1]);
					$this->_accessRulesArray[$hash]['errordocument401'] = $val;
				}
				$rows[] = $l;
			}

			$this->_fileDataArray[$hash] = $rows;
		}

		if ($filetype=='authuserfile' || $filetype=='authadminfile' || $filetype=='signupfile') {
			$users = array();
			foreach($lArray as $l) {
				if (preg_match('/^\s*#/', $l)) {
					continue;
				}

				$ldArray = split(':', $l);
				if (count($ldArray) < 2) {
					continue;
				}

				$data = array('name'  => $ldArray[0],
				'pass'  => $ldArray[1],
				'info'  => isset($ldArray[2]) ? $ldArray[2] : null,
				'email' => isset($ldArray[3]) ? $ldArray[3] : null);

				if ($filetype == 'signupfile') {
					$data['ts']         = isset($ldArray[4]) ? $ldArray[4]: null;
					$data['remoteaddr'] = isset($ldArray[5]) ? $ldArray[5] : null;
					$data['referer']    = implode(':', array_slice($ldArray, 6));
				}

				$users[] = $data;
			}

			$this->_fileDataArray[$hash] = $users;
		}

		if ($filetype == 'authgroupfile') {
		}


		return true;
	}

	/**
     * Set, Parse given file contents and optionaly Save it 
     *
     * @access public
     * @param string $filetype
     * @param string $contents
     * @param bool|null $saveData
     * @param bool|null $saveRawData
     * @return bool
     */
	function setFileContentsByType( $filetype, $contents, $saveData=false, $saveRawData=false )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			$this->setError( $this->W('NOTDEFINED', $filetype) );
			return false;
		}
		$hash = md5( $path );

		$this->_fileContentsArray[$hash] = $contents;

		// parse contents
		if (false == $this->parseFileContentsByType( $filetype, $contents )) {
			return false;
		}

		if ($saveData) {
			if (false == $saveRawData) {
				// build contents from parsed data
			}

			if (false == $this->saveFileByType( $filetype, $contents )) {
				return false;
			}
		}

		return true;
	}

	/***************************************************************************
	* File Data Routes
	**************************************************************************/

	/**
     * Return parsed data
     *
     * @access public
     * @param string $filetype
     * @return array
     */
	function getFileDataByType( $filetype )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			$this->setError( $this->W('NOTDEFINED', $filetype) );
			return false;
		}

		$hash = md5( $path );

		// not readed yet
		if (!isset( $this->_fileDataArray[$hash] )) {
			if (false == $this->readFileByType( $filetype )) {
				return false;
			}
		}

		return $this->_fileDataArray[$hash];
	}

	function getDefaultRecordsBytype( $filetype )
	{
		if ($filetype == 'authadminfile') {
			$pass_raw = $this->_configArray['password'];
			return array(array('name'=>$this->_configArray['login'],
			                   //'pass'=>$this->encrypt_saltcrypt( $pass_raw, 'DES' ),
			                   'pass'=>$this->htcrypt($pass_raw, 'adminpass'),
			                   'pass_raw'=>$pass_raw,
			                   'info'=>'Administration',
			                   'email'=>'support'));
		}

		return array();
	}

	/**
     * Update file record
     *
     * @access public
     * @param string $filetype
     * @param string|null $sortby
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
	function getRecordsByType( $filetype, $sortby=null, $limit=99999, $offset=0, $where=false )
	{
		$recArray = $this->getFileDataByType( $filetype );
		if (false == $recArray) {
			// return default values
			return $this->getDefaultRecordsByType( $filetype );
		}

		if (false != $where && !is_array($where)) {
			$where = array($where);
		}

		// TODO sortby

		// limit, offset
		$outArray = array();
		for ($i=$offset; $i < count($recArray); ++$i) {
			if ($i-$offset >= $limit) {
				break;
			}
			
			$rec = $recArray[$i];
			
			// filtering
			if (false != $where) {
				$valid = false;
				foreach ($where as $field=>$patten) {
					foreach($rec as $k=>$v) {
						if (!is_int($field) && strcasecmp($field, $k) != 0) {
							continue;
						}
						if (preg_match("/$patten/i", $v)) {
							$valid = true;
							break;
						}
					}
					if ($valid) {
						break;
					}
				}
				if (!$valid) {
					continue;
				}
			}

			$outArray[] = $recArray[$i];
		}

		return $outArray;
	}

	function getTotalByType( $filetype, $where=false )
	{
		$recArray = $this->getFileDataByType( $filetype );
		if (false == $recArray) {
			if ($filetype == 'authadminfile') {
				return 1;
			}
			return 0;
		}

		if (false == $where) {
			return count($recArray);
		}
		
		if (!is_array($where)) {
			$where = array($where);
		}
		
		$count = 0;
				
		foreach($recArray as $rec) {
			$valid = false;
			foreach ($where as $field=>$patten) {
				foreach($rec as $k=>$v) {
					if (!is_int($field) && strcasecmp($field, $k) != 0) {
						continue;
					}
					if (preg_match("/$patten/i", $v)) {
						$valid = true;
						break;
					}
				}
				if ($valid) {
					break;
				}
			}
			if (!$valid) {
				continue;
			}
			
			$count++;
		}

		return $count;
	}

	/**
     * Fetch record by Id
     *
     * @access public
     * @param string $filetype
     * @param string $recordId
     * @param bool $checkDefault
     * @return string|array|bool
     */
	function fetchRecordByType( $filetype, $recordId, $checkDefault=false )
	{
		$dataArray = $this->getFileDataByType( $filetype );
		if (false == $dataArray || count($dataArray) < 1) {
			if (false == $checkDefault) {
				return false;
			}

			$dataArray = $this->getDefaultRecordsByType( $filetype );
		}

		if (is_null($recordId)) {
			// reset first record for authadminfile
			if ($filetype == 'authadminfile') {
				return $this->getDefaultRecordsByType( $filetype );
			}
			return false;
		}

		$found = false;

		foreach( $dataArray as $rec ) {
			if ($filetype == 'accessfile') {
				if ($recordId == 'errordocument401') {
					if (preg_match('/^\s*ErrorDocument\s+401\s+/i', $rec)) {
						$found = $rec;
						break;
					}
				} else if (preg_match('/^\s*'.$recordId.'(\s+.+)?\s*$/i', $rec)) {
					$found = $rec;
					break;
				}
				continue;
			}

			if (isset($rec['name']) && strcmp($recordId, $rec['name'])==0) {
				$found = $rec;
				break;
			}
		}

		return $found;
	}

	/**
     * Check record by Id
     *
     * @access public
     * @param string $filetype
     * @param string $recordId
     * @return bool
     */
	function isRecordByType( $filetype, $recordId )
	{
		$rec = $this->fetchRecordByType($filetype, $recordId);
		return $rec == false ? false : true;
	}

	/**
     * Update record in file
     *
     * @access public
     * @param string $filetype
     * @param string $recordId
     * @param array data
     * @return bool
     */
	function updateRecordByType( $filetype, $recordId, $data )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			$this->setError( $this->W('NOTDEFINED', $filetype) );
			return false;
		}
		$hash = md5( $path );

		if (!is_array($this->_fileDataArray[$hash])) {
			$this->_fileDataArray[$hash] = null;
		}

		if ($filetype == 'accessfile') {
			$found = false;
			$recordId = strtolower($recordId);

			foreach ($this->_fileDataArray[$hash] as $id=>$rec) {
				$rec = trim($rec);
				$args = isset($data[$recordId]) ? $data[$recordId] : '';

				if ($recordId == 'errordocument401') {
					if (preg_match('/^(\s*ErrorDocument\s+401)\s+/i', $rec, $matches)) {
						if (!isset($data)) {
							unset( $this->_fileDataArray[$hash][$id] );
							continue;
						}

						$this->_fileDataArray[$hash][$id] = $matches[1] . ' ' . $args;
					}

                // remove all allow or deny commands
                } else if (preg_match('/^\s*'.$recordId.'\s+from\s+.*$/i', $rec)) {
                    unset( $this->_fileDataArray[$hash][$id] );

                } else if (preg_match('/^(\s*'.$recordId.')(.*)$/i', $rec, $matches)) {

					if (!isset($data)) {
						unset( $this->_fileDataArray[$hash][$id] );
						continue;
					}

					if ($recordId == 'authname') {
						$this->_fileDataArray[$hash][$id] = $matches[1] . ' "' . addSlashes($args) . '"';
					} else if ($recordId == 'authuserfile') {
						$this->_fileDataArray[$hash][$id] = $matches[1] . ' ' . $args;
					} else if ($recordId == 'authtype') {
						$this->_fileDataArray[$hash][$id] = $matches[1] . ' basic';
                    } else if ($recordId == 'order') {
                        $this->_fileDataArray[$hash][$id] = 'Order ' . ($args=='deny' ? 'Allow,Deny' : 'Deny,Allow');
					}

					$found = true;
					break;
                }
			}
            
            // special case: allow or deny commands
            if ($recordId == 'allow' || $recordId == 'deny') {
                foreach($data as $item) {
                    $this->_fileDataArray[$hash][] = $recordId . ' from ' . $item;
                }
            }
            
			return $found;
		}

        // other types
		foreach ($this->_fileDataArray[$hash] as $id=>$rec) {
			if (is_null($recordId) || isset($rec['name']) && strcmp($recordId, $rec['name'])==0) {

				if (is_null($data)) {
					unset( $this->_fileDataArray[$hash][$id] );

				} else if (is_array($data)) {
					foreach ($data as $k=>$v) {
						$this->_fileDataArray[$hash][$id][$k] = $v;
					}
				}

				break;
			}
		}

		return true;
	}

	/**
     * Update file record
     *
     * @access public
     * @param string $filetype
     * @param array $data
     * @param string $curRecordId
     * @param bool|null $saveFile
     * @param bool|null $saveForce
     * @return bool
     */
	function setRecordByType( $filetype, $curRecordId, $data, $saveFile=false, $saveForce=false )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			$this->setError( $this->W('NOTDEFINED', $filetype) );
			return false;
		}
		$hash = md5( $path );

		if (false != $this->isRecordByType( $filetype, $curRecordId )) {
			// replaceing
			$this->updateRecordByType( $filetype, $curRecordId, $data );

		} else if (is_array($data)) {

			// inserting
			if ($filetype == 'accessfile') {
				$args = isset($data[$curRecordId]) ? $data[$curRecordId] : '';
				$hasAddedBy = false;

				foreach( $this->_fileDataArray[$hash] as $rec) {
					if (preg_match('/^# added by authman/i', $rec)) {
						$hasAddedBy = true;
					}
				}
				if (!$hasAddedBy) {
					$this->_fileDataArray[$hash][] = "# Added By Authman";
				}

				if ($curRecordId == 'authname') {
					$this->_fileDataArray[$hash][] = 'AuthName "' . $args . '"';
				} else if ($curRecordId == 'authtype') {
					$this->_fileDataArray[$hash][] = 'AuthType ' . $args;
				} else if ($curRecordId == 'require') {
					$this->_fileDataArray[$hash][] = 'require ' . $args;
				} else if ($curRecordId == 'authuserfile') {
					$this->_fileDataArray[$hash][] = 'AuthUserFile ' . $args;
				} else if ($curRecordId == 'errordocument401') {
					$this->_fileDataArray[$hash][] = 'ErrorDocument 401 ' . $args;
                } else if ($curRecordId == 'order') {
                    $this->_fileDataArray[$hash][] = 'Order ' . ($args=='deny' ? 'Allow,Deny' : 'Deny,Allow');
                } else if ($curRecordId == 'allow' || $curRecordId == 'deny') {
                    foreach($data as $item) {
                        $this->_fileDataArray[$hash][] = $curRecordId . ' from ' . $item;
                    }
				} else {
					$this->_fileDataArray[$hash][] = '# '. $curRecordId . ' "' . $args . '"';
				}

				// other files types
			} else {
				$this->_fileDataArray[$hash][] = $data;
			}
		}

		if ($saveFile) {
			$strerr = null;

			// save parsed data
			if (false == $this->saveFileByType( $filetype, false )) {
				$strerr = $this->getError();
			}

			$this->readFileByType( $filetype );

			if (isset($strerr)) {
				$this->setError($strerr);
				return false;
			}
		}

		return true;
	}

	/**
     * Clear all records from the file
     *
     * @access public
     * @param string $filetype
     * @param bool|null $saveFile
     * @return bool
     */
	function clearAllRecordsByType( $filetype, $saveFile=false )
	{
		$path = $this->getPathByType( $filetype );
		if (false == $path) {
			$this->setError( $this->W('NOTDEFINED', $filetype) );
			return false;
		}
		$hash = md5( $path );

		$this->_fileDataArray[$hash] = array();

		if ($saveFile) {
			$strerr = null;

			// save _RAW_ data
			if (false == $this->saveFileByType( $filetype, true, '' )) {
				$strerr = $this->getError();
			}

			$this->readFileByType( $filetype );

			if (isset($strerr)) {
				$this->setError($strerr);
				return false;
			}
		}

		return true;
	}

	function getAccessRuleByType( $filetype, $ruleName )
	{
		$hash = md5( $this->getPathByType( $filetype ) );

		if (!isset($this->_accessRulesArray[$hash])) {
			return false;
		}

		$ruleName = strtolower($ruleName);

		if (!isset($this->_accessRulesArray[$hash][$ruleName])) {
			return false;
		}

		return $this->_accessRulesArray[$hash][$ruleName];
	}

	/***************************************************************************
	*
	**************************************************************************/

    /**
     * Get template list
     *
     * @access public
     * @return array
     */
	function getTemplates()
	{
		$this->_tplsArray = array();

		$tplPath = $this->_basePath . DIRECTORY_SEPARATOR . 'templates';
		$dh = @opendir( $tplPath );
		if (false == $dh) {
			return $this->_tplsArray;
		}

		while ($e = readdir($dh)) {
			if ($e == '.' || $e == '..') {
				continue;
			}

			if (!preg_match('/^(.+)\.tpl(\.dist)?$/i', $e, $matches)) {
				continue;
			}

			$tplId = $matches[1];
            $tplDefault = isset($matches[2]) && $matches[2] == '.dist';
        
			$filePath = $tplPath . DIRECTORY_SEPARATOR . $e;
			$fh = @fopen($filePath, 'r');
			if (false == $fh) {
				continue;
			}

			$data = array();
			while ($l = fgets($fh)) {
				$data[] = $l;
			}
			@fclose($fh);

			$role = 'undefinied';
			if (preg_match('/^(useradd|useredit|userdel|userfgt|userrcv|memberaa|memberdel|memberreq)$/i', $tplId, $matches)) {
				$role = strtolower($matches[1]);
			}

			$tArray = array('path'=>$filePath,
			                'id'=>$tplId,
			                'role'=>$role,
			                'name'=>trim($data[0]),
			                'type'=>( trim($data[1]) == 'html' ? 'html' : 'plaintext' ),
			                'subject'=>trim($data[2]),
			                'contents'=>join('', array_slice($data, 3)) );
                            
            if (!$tplDefault || !array_key_exists($tplId, $this->_tplsArray)) {
			    $this->_tplsArray[ $tplId ] = $tArray;
            }
		}

		closedir($dh);

		return $this->_tplsArray;
	}

	function getTemplateById( $id )
	{
		if (!isset($id) || $id=='') {
			return false;
		}

		$templates = $this->getTemplates();

		if (false==$templates || !isset($templates[$id])) {
			return false;
		}
		return $templates[$id];
	}

	function getTemplateByRole( $role )
	{
		$res = false;

		$templates = $this->getTemplates();
		foreach($templates as $id=>$tpl) {
			if ($tpl['role'] == $role) {
				$res = $tpl;
			}
		}

		return $res;
	}

	function saveTemplateAs( $id, $data, $force=false )
	{
		if (!isset($id) || $id=='' || !is_array($data)) {
			$this->setError( $this->E('INVALIDREQUEST') );
			return false;
		}

		// Saving in DEMO mode is disabled
		if ($this->isDemo()) {
			$this->setError( $this->W('DEMOISON') );
			return false;
		}

		$path = $this->_basePath . DIRECTORY_SEPARATOR . 'templates';

		if (!is_dir($path)) {
			if (false == @mkdir($path, 0755)) {
				$this->setError( $this->E('MKDIRFAILED', $path) );
				return false;
			}

			$denyPath = $path . DIRECTORY_SEPARATOR
			          . $this->_configArray['access_file'];
			$this->writeDenyFile( $denyPath );
		}

		$id = $this->getNormalizedPath( $id );
		$filePath = $path . $id . '.tpl';

		if (is_file($filePath) && false == $force) {
			$this->setError( $this->E('FILEEXISTS', $filePath) );
			return false;
		}

		if (is_file($filePath) && !is_writable($filePath)) {
			$this->setError( $this->W('FILEWRITABLE') );
			return false;
		}

		$fh = @fopen($filePath, 'w');
		if (false == $fh) {
			$this->setError( $this->E('FILEOPENFAILED', $filePath) );
			return false;
		}

		@fwrite($fh, $data['name'] . "\n");
		@fwrite($fh, $data['type'] . "\n");
		@fwrite($fh, $data['subject'] . "\n");
		@fwrite($fh, $data['contents'] . "\n");

		@fclose($fh);

		return true;
	}

    /**
    * @desc Delete e-mail template
    * 
    * @access public
    * @param string $id
    * @param bool $force
    * @return bool
    */
	function deleteTemplate( $id, $force=false )
	{
		// Deleting in DEMO mode is disabled
		if ($this->isDemo()) {
			$this->setError( $this->W('DEMOISON') );
			return false;
		}

		// Check if
		$template = $this->getTemplateById( $id );
		if (false == $template) {
			$this->setError( $this->E('TPLNOTFOUND', $id) );
			return false;
		}

		if ($force == false) {
			if ($template['role'] != 'undefinied') {
				$this->setError( $this->E('TPLISSYSTEM') );
				return false;
			}
		}

		$path = $this->_basePath . DIRECTORY_SEPARATOR . 'templates';
		$filePath = $path . DIRECTORY_SEPARATOR . $id . '.tpl';

		if (!is_file($filePath)) {
			$this->setError( $this->E('NOSUCHFILE', $filePath) );
			return false;
		}

		if (false == @unlink($filePath)) {
			$this->setError( $this->E('FILEDELFAILED', $filePath) );
			return false;
		}

		return true;
	}

	/***************************************************************************
	* E-mail routes
	**************************************************************************/

	/**
	 * Send e-mail to the user
	 *
     * @access public
	 * @param string|array $templateId
	 * @param array $userArray
	 * @return bool
	 */
	function sendMail( $templateId, $userArray )
	{
		if (false == $this->hasFeature('PHPMailer')) {
			$this->setError(  'SendMail: ' . $this->W('SENDMAILFAIL_INSTALLPHPMAILER'));
			return false;
		}

		// no spam here
		if ($this->isDemo()) {
			$this->setError( 'SendMail: ' . $this->W('DEMOISON') );
			return false;
		}

		if (is_array($templateId)) {
			$tplArray = $templateId;
			$templateId = 'manual';
		} else {
			$tplArray = $this->getTemplateById( $templateId );
			if (false == $tplArray) {
				$this->setError( 'SendMail: ' . $this->E('INCORRECTARGS') . ' [template]' );
				return false;
			}
		}

		if (!isset($userArray['email']) || $userArray['email'] == '') {
			$this->setError( 'SendMail: ' . $this->E('INCORRECTARGS') . ' [email]' );
			return false;
		}

		$args = array('PROTECTEDURL'=> $this->getUrlByType('protected', true),
		              'BASEURL'     => $this->getUrlByType('base', true),
	 	              'MEMBERURL'   => $this->getUrlByType('login', true),
		              'REMOTEADDR'  => $_SERVER['REMOTE_ADDR'],
		              'DATETIME'    => gmdate('D dS \of M Y H:i:s e') );

		list($admin) = $this->getRecordsByType( 'authadminfile', null, 1, 0 );

		$args['ADMINREALNAME'] = $admin['info'] != '' ? $admin['info'] : "Administrator";
		$args['ADMINEMAIL']    = $admin['email'] != '' ? $admin['email'] : "postmaster@localhost";

		foreach($userArray as $k=>$v) {
			$kk = 'USER' . strtoupper($k);
			$args[$kk] = $v;
		}
		$args['USERREALNAME'] = isset($userArray['info']) && $userArray['info'] != ''
		? $userArray['info'] : $userArray['name'];
		$args['USERPASSWORD'] = isset($userArray['pass_raw']) && $userArray['pass_raw'] != '' ? $userArray['pass_raw'] : '[ENCRYPTED: '. $userArray['pass'] .']';

		$subject = $tplArray['subject'];
		$body    = $tplArray['contents'];

		foreach(array('subject', 'body') as $vname) {
			foreach( $args as $k=>$v ) {
				$$vname = str_replace('%'.$k.'%', $v, $$vname);
			}
		}

		// sending
		include_once( $this->getPathByType('PHPMailer') );

		$mail = new PHPMailer();
		$mail->CharSet = "UTF-8";

		// from administrator
		$mail->FromName   = $args['ADMINREALNAME'];
		$mail->From       = $args['ADMINEMAIL'];
		$mail->AddReplyTo($args['ADMINEMAIL'], $args['ADMINREALNAME']);

		$isHtml = $tplArray['type'] == 'html';
		if ($templateId=='memberdel' || $templateId=='memberaa') {
			$mail->AddAddress($args['ADMINEMAIL'], $args['ADMINREALNAME']);
		} else {
			if (!isset($args['USEREMAIL']) || $args['USEREMAIL']=='') {
				if ($isHtml) {
					$body = '<p><strong>USER ' . $userArray['name'] . ' have not E-mail address</strong></p><br />' . $body;
				} else {
					$body = 'USER ' . $userArray['name'] . " have not E-mail address\n" . $body;
				}
				$mail->AddAddress($args['ADMINEMAIL'], $args['ADMINREALNAME']);

			} else {
				$mail->AddAddress($args['USEREMAIL'], $args['USERREALNAME']);
			}
		}

		$mail->Subject = $subject;
		$mail->Body    = $body;

		if ($isHtml) {
			$mail->IsHTML( true );
			$mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
		}

		if (false == $mail->Send()) {
			$this->setError(  'SendMail: ' . $mail->ErrorInfo );
			return false;
		}

		return true;
	}

	function hasFeature( $feature )
	{
		$ln = strtolower($feature);

		if ($ln == 'phpmailer' || $ln == 'tinymcejs' || $ln = 'magpierss') {
			$filePath = $this->getPathByType( $ln );

			if (!is_file($filePath) || !is_readable($filePath)) {
				return false;
			}
			return true;
		}

		return true;
	}

	/***************************************************************************
	* Messages/Warnings/Errors Reporting
	**************************************************************************/

	/**
     * Messages wrapper
     *
     * @access public
     * @return string
     */
	function M()
	{
		$args = func_get_args();
		return $this->getMessage( 'messages', $args );
	}

	/**
     * Warnings wrapper
     *
     * @access public
     * @return string
     */
	function W()
	{
		$args = func_get_args();
		return $this->getMessage( 'warnings', $args );
	}

	/**
     * Errors wrapper
     *
     * @access public
     * @return string
     */
	function E()
	{
		$args = func_get_args();
		return $this->getMessage( 'errors', $args );
	}

	/**
     * Return formated message 
     *
     * @access private
     * @param string $type 
     * @param array $args
     * @return string
     */
	function getMessage( $type, $args )
	{
		$fmt = strtoupper(array_shift($args));

		if (isset($this->_langArray[$type]) && isset($this->_langArray[$type][$fmt])) {
			$s = vsprintf( $this->_langArray[$type][$fmt], $args);
		} else {
			$s = '[' . $type . ': ' . $fmt;
			if (count($args)) {
				$s .= ':';
				foreach ($args as $t) {
					$s .= ' ' . $t;
				}
			}
			$s .= ']';
		}

		return $s;
	}

	/**
     * Set error message
     *
     * @access protected
     * @param string|null
     */
	function setError( $error=null )
	{
		$this->_error = $error;

		if (!isset($error)) {
			return;
		}

		if (isset($php_errormsg))
		$this->_error .= ': ' . $php_errormsg;

		return;
	}

	/**
     * Return true if error string is set
     *
     * @access public
     * @return bool
     */
	function isError()
	{
		return $this->_error == '' ? false : true;
	}

	/**
     * Get error message
     *
     * @access public
     * @return string
     */
	function getError()
	{
		if (!isset($this->_error)) {
			return $this->M('UNKNOWNERROR');
		}

		return $this->_error;
	}

	/**
     * Clear error variable
     *
     * @access public
     * @return void
     */
	function clearError()
	{
		$this->_error = null;
	}

	/**
     * Return runtime errors and warnings
     *
     * @access public
     * @param bool $onlyCritical (default is false)
     * @return array
     */
    function getRuntimeErrors( $onlyCritical=false )
    {
		$wArray = array();

		// 1001 Config file doesn't exist
		//      Recoverable: Yes
		//      Action: start configuration wizard
		$configPath = $this->_filePathsArray['configfile'];
		if (!is_file($configPath)) {
			$wArray[] = array('errno'=>1001,
			'message'=>'Configuration file does not exist: ' . $configPath,
			'recover'=>false, // true
			'critial'=>false);
		}

		// 1002 Config file is not readble
		//      Recoverable: No
		//      Recomendation: change file rights manually
		if (is_file($configPath) && !is_readable($configPath)) {
			$wArray[] = array('errno'=>1002,
			'recover'=>false,
			'message'=>'Configuration file is not readable: ' . $configPath,
			'critial'=>false);
		}

		// 1003 Language file doesn't exist
		//      Recoverable: Yes
		//      Action: download language file
		//      Recomendation: or change config settings
		$langFilePath = $this->_filePathsArray['langfile'];
		if (!is_file($langFilePath)) {
			$wArray[] = array('errno'=>1003,
            'recover'=>false, // true
			'message'=>'Language file does not exist: ' . $langFilePath);
		}

		// 1004 Default (English) Language file doesn't exist
		//      Recoverable: Yes
		//      Action: download default language file
		$langFilePath = $this->makePath($this->_basePath . DIRECTORY_SEPARATOR . 'languages',
		'english.lng' );
		if (!is_file($langFilePath)) {
			$mesg = 'Default language (ENGLISH) file does not exist: ' . $langFilePath;

			$wArray[] = array('errno'=>1004,
            'recover'=>false, // true
			'message'=>$mesg,
			'critical'=>true);
		}

		// File languages/.htaccess does not exist
		// File templates/.htaccess does not exist
		// File config/.htaccess does not exist

        if (!$onlyCritical) {
            return $wArray;
        }

        $cArray = array();
        foreach($wArray as $wItem) {
            if (!isset($wItem['critical']) || !$wItem['critical']) {
                continue;
            }
            $cArray[] = $wItem;
        }
        return $cArray;
	}

	/***************************************************************************
	* Runtime Variables
	**************************************************************************/
	function hasRuntimeValue( $name )
	{
		if (!isset($this->_runtimeArray)) {
			// loading runtime variables
			if (false == $this->loadRuntime()) {
				return false;
			}
		}
		if (!array_key_exists($name, $this->_runtimeArray)) {
			return false;
		}
		return true;
	}

    function loadRuntime()
    {
		$this->_runtimeArray = array();

		$basedir = $this->_basePath . DIRECTORY_SEPARATOR . 'var';
		$filePath = $this->makePath( $basedir, 'runtime.ini' );

		if (is_file($filePath) && is_readable($filePath)) {
			$rt = @parse_ini_file( $filePath );
			if ($rt != false) {
				$this->_runtimeArray = $rt;
			}
		}

		return true;
	}

	function getRuntimeValue( $name )
	{
		if (false == $this->hasRuntimeValue($name)) {
			return false;
		}
		return $this->_runtimeArray[$name];
	}

    function setRuntimeValue( $name, $value, $saveFile=false )
    {
        if (is_string($value)) {
            $value = trim($value, "\r\n");
        }

		if ($this->hasRuntimeValue($name) && $this->getRuntimeValue( $name ) == $value) {
			return true;
		}

		if (is_null($value)) {
			if (!isset($this->_runtimeArray[$name])) {
				return true;
			}
			unset($this->_runtimeArray[$name]);
		} else {
			$this->_runtimeArray[$name] = $value;
		}

		if (false == $saveFile) {
			return true;
		}

		// Saving in DEMO mode is disabled
		if ($this->isDemo()) {
			//$this->setError( $this->W('DEMOISON') );
			//return false;
            return true;
		}

		$basedir = $this->_basePath . DIRECTORY_SEPARATOR . 'var';
		$filePath = $this->makePath( $basedir, 'runtime.ini' );

		if (!is_dir($basedir)) {
			if (false == @mkdir($basedir, 0755)) {
				$this->setError( $this->E('MKDIRFAILED', $basedir) );
				return false;
			}

			$denyPath = $basedir . DIRECTORY_SEPARATOR
			          . $this->_configArray['access_file'];
			$this->writeDenyFile( $denyPath );
		}

		if (!is_file($filePath) && !is_writable($basedir)) {
			$this->setError( $this->E('DIRNOTWRITABLE', $basedir) );
			return false;
		}

		if (is_file($filePath) && !is_writable($filePath)) {
			$this->setError( $this->E('FILENOTWRITABLE', $path) );
			return false;
		}

		$fh = @fopen($filePath, 'w');
		if (false == $fh) {
			$this->setError( $this->E('FILEOPENFAILED', $filePath) );
			return false;
		}

		@fwrite($fh, "# Automatically Created by Autman Free\n");
		@fwrite($fh, "[runtime]\n");

		foreach( $this->_runtimeArray as $k=>$v) {
			@fwrite($fh, $k .'=' );
			if (is_bool($v)) {
				@fwrite($fh, $value ? '"1"' : '"0"');
			} else {
				@fwrite($fh, '"'.$v.'"');
			}

			@fwrite($fh, "\n");
		}

		@fclose($fh);

		return true;
	}
    
    /***************************************************************************
    * RSS, NEWs
    **************************************************************************/
    /**
     * Returns RSS product news as HTML code
     * 
     * @access public
     * @params void
     * @return string
     */
    function newsFetchAsHTML() 
    {
        $text = '';
        
        if (!$this->hasFeature('magpierss')) {
            return 'Server does not support this feature';
        }

        $disableCaching = false;

        $cachedir = $this->_basePath . DIRECTORY_SEPARATOR . 'var' 
                  . DIRECTORY_SEPARATOR . 'cache'; 

        if (!defined('MAGPIE_CACHE_DIR')) {
            define('MAGPIE_CACHE_DIR', $cachedir);
        }
        
        // increase default cache time (1h) up to 24h
        if (!defined('MAGPIE_CACHE_AGE')) {
            define('MAGPIE_CACHE_AGE', 3600*24); 
        }
            
        if (!is_dir($cachedir)) {
            if (!@mkdir($cachedir, 0755)) {
                $disableCaching = true;
            } else {
                $denyPath = $cachedir . DIRECTORY_SEPARATOR
                          . $this->_configArray['access_file'];
                $this->writeDenyFile( $denyPath );
            }
        }
        if (!is_dir($cachedir) && !is_writable($cachedir)) {
            $disableCaching = true;
        }

        if ($disableCaching) {
            define('MAGPIE_CACHE_ON', false);
        }

        include_once( $this->getPathByType('magpierss') ); // rss_fetch.inc
        
        $url = 'http://www.authman.com/rss/free/';
        #$rss = fetch_rss( $url );
        if (!$rss) {
            return 'Sorry, disable due to timeout';
        }
        
        foreach ( $rss->items as $item ) {
            $text .= '<div id="item">';
            $text .= sprintf('<div id="title"><a href="%s" base="_top">%s</a></div>',
                             $item['link'], $item['title']);
            if (isset($item['content']) && isset($item['content']['encoded'])) {
                $description  = $item['content']['encoded'];
            } else {
                $description  = '<pre>' . $item['description'] .'</pre>';
            }
            $text .= '<div id="description">' . $description . '</div>';
            $text .= '</div>';
        }
            
        return $text;
    }
}
// the end of authman class


/**
 * Users and group management
 *
 * @return void
 */
function showPage_users()
{
	global $am;

	// Checking access rights
	if (!$am->isAdmin()) {
		$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
		showPage_401( true );
		exit;
	}

	$error = $message = null;
	$action = getParam('action', null, 'attribute');

    // ------------- Ajax Actions ----------------    
        
	// Ajax Acitons: Sending email to an user
	if ($action == 'mailtouser') {
		$body = getParam('body', null, 'htmlcode');
		$subject = getParam('subject', 'Subject not set', 'subject');
		
		// building pseudo-template
		$templateArray = array('role'=>'undefinied',
		                       'type'=>getParam('type', 'plaintext', 'attribute'),
							   'subject'=>$subject,
							   'contents'=>$body);

		$userData = array();
		
		$username = getParam('username', null, 'username');
		if (!isset($username) || $username == '') {
			$error = $am->E('INVALIDREQUEST') . ' [username]';
		}

		// checking for special username: SELECTED
		if (!isset($error)) {
			if (getParam('username') == 'SELECTED') {
				$usernames = getParam('usernames', null, 'username');
				if (!is_array($usernames)) {
					$error = $am->E('INVALIDREQUEST') . ' [usernames]';
				}
			} else {
				$usernames = array($username);
			}
		}
		
		// real sending emal to the user(s)
		if (!isset($error)) {			
			foreach($usernames as $uname) {
				if (!($userData = $am->fetchRecordByType('authuserfile', $uname))) {
					$error = $am->E('USERNOTEXISTS', $uname);
					break;
				}
			
				if (!$am->sendMail($templateArray, $userData)) {
					$error = $am->E('SENDINGFAILED') . ': ' . $am->getError();
					break;
				}
			}
		}
				
		// follow-up
		if (!isset($error)) {
            $message = $am->M('INFO_EMAILSENT');
	    }

        if (!isset($error)) {
            echo '<div class="message">' . fmt_message($message) . "</div>\n";
        } else {
            echo '<div class="warning">' . fmt_message($error) . "</div>\n";
        }
        
		return;
	}

    // Ajax Acitons: Checking given username
	if ($action == 'checkusername') {
		$username = getParam('username', null, 'username');

		if ($am->isRecordByType('authuserfile', $username)) {
			echo 'USERFOUND';
		} else {
			echo 'NOSUCHUSER';
		}
		return;
	}
	
	// ------------- General variables ----------------

	$limit = 10;
	if (hasParam('limit')) {
		$limit  = getParam('limit', 10, 'int');
		$_SESSION['user_limit'] = $limit;
	} else if (isset($_SESSION['user_limit'])) {
		$limit = $_SESSION['user_limit'];
	}

	$p = 1;
	if (hasParam('p')) {
		$p = getParam('p', 1, 'int' );
		$_SESSION['user_p'] = $p;
	} else if (isset($_SESSION['user_p'])) {
		$p = $_SESSION['user_p'];
	}

	if (empty($limit)) {
		$p = 1;
	}

	$offset = ($p-1) * $limit;

	// ------------- Actions ----------------

	// Action: Add user
	if ($action == 'adduser') {
		$username = getParam('username', null, 'username');
        if (!isset($username) || $username == '') {
			$error = $am->E('INVALIDREQUEST') . ' [username]';
		
        } else if ($am->isRecordByType('authuserfile', $username)) {
			$error = $am->E('USEREXISTS', $username);
		}

		$password = getParam('password', null, 'password');
		if (!isset($error))  {
			if ($password == '') {
				$error = $am->E('INVALIDREQUEST') . ' [password]';
			} else if (strlen($password) < 4) {
				$error = $am->E('PASSWORDTOOSHORT', 4);
			}
		}
		
		$data = array('name'    => $username,
		              'pass_raw'=> $password,
		              'info'    => getParam('realname', null, 'field'),
		              'email'   => getParam('email', null, 'email'));

		if (!isset($error)) {
			if (!$am->setRecordByType('authuserfile', null,  $data, true)) {
				$error = $am->E('USERINSERTFAILED', $username)
				       . ': ' . $am->getError();
			}
		}

		if (!isset($error)) {
			$message = $am->M('USERINSSUCCESS', $username);
			$action = 'updateuser';
		}

		// sending notification via e-mail
		$am->setRuntimeValue('useradd_sendmail', hasParam('sendmail'), true);
		if (!isset($error) && hasParam('sendmail')) {
			$template = getParam('template', 'useradd', 'attribute');

			if (false == $am->sendMail($template, $data )) {
				$error = $am->getError();
			}
		}

	// Action: Edit user
	} else if ($action == 'updateuser') {
		$oldusername = getParam('oldusername', null, 'username');
		if (!$am->isRecordByType('authuserfile', $oldusername)) {
			$error = $am->E('INVALIDREQUEST') . ' [curusername]';
		}

		$username = getParam('username', null, 'username');
		if (!isset($username) || $username == '') {
			$error = $am->E('INVALIDREQUEST') . ' [username]';
		}

		if ($oldusername != $username && $am->isRecordByType('authuserfile', $username)) {
			$error = $am->E('USEREXISTS', $username);
		}

		$data = array('name' =>$username,
			          'info' =>getParam('realname', null, 'field'),
            		  'email'=>getParam('email', null, 'email'));

		if (hasParam('password')) {
			$pass = getParam('password', null, 'password');
			if (isset($pass) && $pass != '') {
				$data['pass_raw'] = $pass;
			}
		}

		if (!isset($error)) {
			if (!$am->setRecordByType( 'authuserfile', $oldusername, $data, true)) {
				$error = $am->E('USERUPDATEFAILED', $username) 
                       . ': ' . $am->getError();
			}
		}

		if (!isset($error)) {
			$message = $am->M('USERUPDSUCCESS', $username);
		}
		
		// sending notification e-mail
		$am->setRuntimeValue('useredit_sendmail', hasParam('sendmail'), true);
		if (!isset($error) && hasParam('sendmail')) {
			$template = getParam('template', 'useredit', 'attribute');

			if (!$am->sendMail($template, $data)) {
				$error = $am->getError();
			}
		}
			
	// Action: Delete user
	} else if ($action == 'deleteuser') {
		$username = getParam('username', null, 'username');
		if (!$am->isRecordByType('authuserfile', $username)) {
			$error = $am->E('USERNOTEXISTS', $username);
		}

		if (!isset($error)) {
			if (!$am->setRecordByType('authuserfile', $username, null, true)) {
				$error = $am->E('USERDELETEFAILED', $username)
				       . ': ' . $am->getError();
			}
		}
		if (!isset($error)) {
			$message = $am->M('USERDELETED', $username);
		}
	
	// Action: delete selected users
	} else if ($action == 'deleteselusers') {
		$usernames = getParam('usernames', null, 'username');
		foreach($usernames as $username) {

			if (!$am->isRecordByType('authuserfile', $username)) {
				$error = $am->E('USERNOTEXISTS', $username);
				break;
			}

			if (!$am->setRecordByType('authuserfile', $username, null, true)) {
				$error = $am->E('USERDELETEFAILED', $username) 
                       . ': ' . $am->getError();
				break;
			}

		}
		if (!isset($error)) {
			$message = $am->M('USERSSELDELETED');
		}

	// Action: Delete All Users
	} else if ($action == 'deleteallusers') {

		if (!isset($error)) {
			if (false == $am->clearAllRecordsByType( 'authuserfile', true )) {
				$error = $am->E('USERSDELALLFAILED') . ': ' . $am->getError();
			}
		}
		if (!isset($error)) {
			$message = $am->M('USERSALLDELETED');
		}
	
	}

    // ------------- HTML Preparing ----------------

	$searchtext = '';
	if (hasParam('searchtext')) {
		$searchtext = getParam('searchtext', null, 'field');
		$_SESSION['searchtext'] = $searchtext;
	} else if (isset($_SESSION['searchtext'])) {
		$searchtext = $_SESSION['searchtext'];
	}
	
	$usersArray = $am->getRecordsByType('authuserfile', 'name', $limit, $offset, $searchtext);
	$groupsArray = $am->getRecordsByType('authgroupfile', 'g_name');

	web_header( $am->M('SUBTITLE_USERS') );
	web_menu();
	echo '<div id="righty">';

	// ------------- Information ----------------

	echo '<div class="pagenotes">';
	echo '<h4>' . $am->M('SUBTITLE_USERS') . '</h4>';

	echo '<div><div class="name">AuthUserFile</div>';
	echo '<div class="value">'; 
	$file = $am->getPathByType('authuserfile');
	echo $file == '' ? $am->W('NOTDEFINED', 'AuthUserFile') : $file;
	echo '</div></div>';

	echo '<div><div class="name">' . $am->M('TOTALMEMBERS') . '</div>';
	echo '<div class="value">' . $am->getTotalByType('authuserfile', $searchtext) . '</div></div>';

	echo '</div>';

	// ------------- Message ----------------
	web_message( $message, $error );
	
?>
<h1><?php echo $am->M('SUBTITLE_USERS') ?></h1>

 <?php
$authuserfile = $am->getPathByType('authuserfile');
if (false == $authuserfile) {
    echo '<div class="block">';
    echo $am->M('NOTES_NODEFAUTHUSERFILE');
	echo '</div>';
}
 ?>
 
 <div id="userSearch">
    <form id="userSearch_form" name="userSearch_form">
	<fieldset>
 		<input id="userSearch_searchtext" type="text" name="searchtext" maxlength="255" class="required" value="<?php echo $searchtext ?>" />
    	<input type="hidden" name="page" value="users" />
        <input type="hidden" name="action" value="search" />
        <input type="submit" id="userMail_submit" name="submit" value="<?php echo $am->M('BTN_FILTER') ?>" />
 		<input type="submit" id="userMail_submit" name="submit" value="<?php echo $am->M('BTN_RESET') ?>" onClick="javascript:$($(userSearch_form).searchtext).value = ''; return true;" />
	</fieldset>
    </form>
 </div>
 <div class="spacer30"></div>
 	
<!-- USERS add form -->
<div id="userAdd" style="display: none;">
    <form id="userAdd_form" name="userAdd_form">

    <div id="userAdd_checkUsername" style="float:right;"></div>
    <p id="userAdd_form_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>

    <fieldset>
    <legend><?php echo $am->M('LEGEND_USERNEW') ?></legend>
        <div>
            <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
            <input id="userAdd_username" type="text" name="username" maxlength="255" class="required" />
            <input id="userAdd_check" name="check" type="button" value="<?php echo $am->M('CHECK') ?>" onclick="javascript:usersCheckUsername('userAdd');" />
        </div>
        <div>
        <label for="password" class="required"><?php echo $am->M('FIELD_PASSWORD') ?></label>
            <input id="userAdd_password" type="text" name="password" maxlength="255" class="required" />
            <input type="button" value="<?php echo $am->M('GENERATE') ?>" onclick="javascript:mainGeneratePassword('userAdd');" />
        </div>
        <div>
        	<label for="realname"><?php echo $am->M('FIELD_REALNAME') ?></label>
            <input id="userAdd_realname" type="text" name="realname" size="40" maxlength="255" />
        </div>
        <div>
        <label for="email"><?php echo $am->M('FIELD_EMAIL') ?></label>
            <input id="userAdd_email" type="text" name="email" size="40" maxlength="255" />
        </div>
        
        <div class="dashed"></div>
<?php
        $sendMail = true;
        if (!$am->hasFeature('PHPMailer')) {
	        $sendMail = false;

        } else if ($am->hasRuntimeValue('useradd_sendmail') &&
                   $am->getRuntimeValue('useradd_sendmail') != "1") {
	        $sendMail = false;
        }
?>        
        <div>
        	<label for="sendmail"><?php echo $am->M('FIELD_WELCOMEEMAIL') ?></label>
            <input type="checkbox" id="userAdd_sendmail" name="sendmail" value="1" 
                   <?php if ($sendMail) { echo ' checked="checked" '; } ?>
                   onClick="javascript:usersOnChangeSendMail('userAdd');" /> <?php echo $am->M('SEND') ?>
        </div>
<?php 
        if (!$am->hasFeature('PHPMailer')) {        
		    echo '<div class="block">';
		    echo $am->W('SENDMAILFAIL_INSTALLPHPMAILER');
		    echo '</div>';
        }
?>	 
        <div id="userAdd_showtemplate" style="display:<?php echo $sendMail ? 'block' : 'none' ?>;">
        	<label for="template"><?php echo $am->M('FIELD_EMAILTEMPLATE') ?></label>
        	<select id="userAdd_template" name="template">
        		<option value=""><?php echo $am->M('SELECTONE') ?></option>
<?php 
        		foreach ($am->getTemplates() as $id=>$tpl) {
        			echo '<option value="' . addslashes($id).'"';
        			if ($tpl['role'] == 'useradd') {
        				echo ' selected="selected"';
        			}
        			echo '>';
        			echo $tpl['name'] . '</option>';
        		}
?>
        	</select>
        </div>        
    </fieldset>
    
    <div class="buttonrow">
        <input type="hidden" name="action" value="adduser" />
        <input type="hidden" name="page" value="users" />
        <?php echo htmlSubmit('submit'); ?>
        <?php echo htmlReset('reset', 'onClick="javascript:usersResetForm(\'userAdd\'); return false;"' ); ?>
        <input type="submit" id="userAdd_cancel" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:usersShowForm('userAdd','hide'); return false;" />
    </div>
    </form>
</div>
<!-- end of USERS add form -->

<!-- USERS edit form -->
<div id="userEdit" style="display: none;">
    <form id="userEdit_form" name="userEdit_form">
    <p id="userEdit_form_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>
    <fieldset>
       <legend><?php echo $am->M('LEGEND_USEREDIT') ?></legend>

        <div>
        <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
            <input id="userEdit_username" name="username" type="text" maxlength="255" class="required" />
        </div>
        <div>
        <label for="password"><?php echo $am->M('FIELD_NEWPASSWORD') ?></label>
            <input id="userEdit_password" name="password" type="text" maxlength="255" />
            <input type="button" value="<?php echo $am->M('GENERATE') ?>" onclick="javascript:mainGeneratePassword('userEdit');" />
        </div>
        <div>
        <label for="realname"><?php echo $am->M('FIELD_REALNAME') ?></label>
            <input id="userEdit_realname" name="realname" type="text" size="40" maxlength="255" />
        </div>
        <div>
        <label for="email"><?php echo $am->M('FIELD_EMAIL') ?></label>
            <input id="userEdit_email" name="email" type="text" size="40" maxlength="255" />
        </div>

        <div class="dashed"></div>
<?php
        # by default sending email is off
        $sendMail = false;
        if (!$am->hasFeature('PHPMailer')) {
	        $sendMail = false;

        } else if ($am->hasRuntimeValue('useredit_sendmail') && 
                   $am->getRuntimeValue('useredit_sendmail') == "1") {
	        $sendMail = true;
        }
?> 
	    <div>
        	<label for="sendmail"><?php echo $am->M('FIELD_UPDATEEMAIL') ?></label>
            <input type="checkbox" id="userEdit_sendmail" name="sendmail" value="1" 
                   <?php if ($sendMail) { echo ' checked="checked" '; } ?>
                   onClick="javascript:usersOnChangeSendMail('userEdit');" /> <?php echo $am->M('SEND') ?>
        </div>
<?php 
        if (!$am->hasFeature('PHPMailer')) {        
		    echo '<div class="block">';
		    echo $am->W('SENDMAILFAIL_INSTALLPHPMAILER');
		    echo '</div>';
        }
?>	 
        <div id="userEdit_showtemplate" style="display:<?php echo $sendMail ? 'block' : 'none' ?>;">
        	<label for="template"><?php echo $am->M('FIELD_EMAILTEMPLATE') ?></label>
        	<select id="userEdit_template" name="template">
        		<option value=""><?php echo $am->M('SELECTONE') ?></option>
<?php 
        		foreach ($am->getTemplates() as $id=>$tpl) {
        			echo '<option value="' .addslashes($id).'"';
        			if ($tpl['role'] == 'useredit') {
        				echo ' selected="selected"';
        			}
        			echo '>';
        			echo $tpl['name'] . '</option>';
        		}
?>
        	</select>
        </div>  
        
    </fieldset>
    
    <div class="buttonrow">
        <input type="hidden" id="userEdit_oldusername" name="oldusername" value="" />

        <input type="hidden" name="action" value="updateuser" />
        <input type="hidden" name="page" value="users" />

        <?php echo htmlSubmit('submit'); ?>
        <?php echo htmlReset('reset', 'onClick="javascript:usersResetForm(\'userEdit\'); return false;"' ); ?>

        <input type="submit" id="userEdit_cancel" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:usersShowForm('userEdit',''); return false;" />
    </div>
    </form>
</div>
<!-- end of USERS edit form -->
        
<!-- USER delete form -->
<div id="userDelete" style="display: none;">
    <form id="userDelete_form" name="userDelete">
    <fieldset>
    <legend><?php echo $am->M('LEGEND_USERDELETE') ?></legend>
       <p id="userDelete_form_msg" class="formmessage"><?php echo $am->M('Q_USERDELETE'); ?></p>
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="username" value="" />
        <input type="hidden" name="page" value="users" />
        <input type="hidden" name="action" value="deleteuser" />
        <input type="submit" id="userDelete_submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
        <input type="submit" id="userDelete_cancel" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:usersShowForm('userDelete','hide'); return false;" />
    </div>
    </form>
</div>
<!-- end of USER delete form -->
     
<!-- USERS delete selected form -->
<div id="usersDelSel" style="display: none;">
    <form id="usersDelSel_form" name="usersDelSel">
    <fieldset>
    <legend><?php echo $am->M('LEGEND_USERDELSEL') ?></legend>
       <p id="usersDelSel_form_msg" class="formmessage"><?php echo $am->M('Q_USERDELSEL'); ?></p>
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="page" value="users" />
        <input type="hidden" name="action" value="deleteselusers" />
        <input type="submit" id="usersDelSel_submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
        <input type="submit" id="usersDelSel_cancel" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:usersShowForm('usersDelSel','hide'); return false;" />
    </div>
    </form>
</div>
<!-- end of USERS delete selected form -->

<!-- USERS delete all form -->
<div id="usersDelAll" style="display: none;">
    <form id="usersDelAll_form" name="usersDelSel">
    <fieldset>
    <legend><?php echo $am->M('LEGEND_USERDELALL') ?></legend>
       <p id="usersDelAll_form_msg" class="formmessage"><?php echo $am->M('Q_USERDELALL'); ?></p>
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="page" value="users" />
        <input type="hidden" name="action" value="deleteallusers" />
        <input type="submit" id="usersDelAll_submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
        <input type="submit" id="usersDelAll_cancel" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:usersShowForm('usersDelAll','hide'); return false;" />
    </div>
    </form>
</div>
<!-- end of USERS delete all form -->
   
<!-- USERS userMail form -->
<div id="userMail" style="display: none;">
    <form id="userMail_form" name="userMail">
    <fieldset>
      <legend><?php echo $am->M('LEGEND_BASETEMPLATESEL') ?></legend>
      <div>
        <label for="templateid" class=""><?php echo $am->M('FIELD_BASETPLNAME') ?></label>
        <select id="templateid" onChange="javascript:usersOnChangeTemplate( 'userMail', this.value );">
            <option id="userMail_default_templateid" value=""><?php echo $am->M('OPTS_CUSTOMEMAIL') ?></option>
            <option value="">------</option>
<?php
		$tplsArray = $am->getTemplates();		
        foreach($tplsArray as $id=>$tpl) {
            print '<option value="' . addslashes($id) . '">' . $tpl['name'] . '</option>';
        }
?>
        </select>
      </div>
    </fieldset>
	
	<p id="userMail_form_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>

    <!-- subject, body etc -->
    <fieldset>
      <legend><?php echo $am->M('LEGEND_MAILSEND') ?></legend>
      <div>
        <label for="userMail_mailto" class=""><?php echo $am->M('FIELD_MAILTO') ?></label>
        <span id="userMail_mailto"></span>
      </div>
      <div>
        <label for="subject" class="required"><?php echo $am->M('FIELD_MAILSUBJECT') ?></label>
        <input id="userMail_subject" name="subject" type="text" maxlength="255" class="required" />
      </div>
      <div>
        <label for="type" class="required"><?php echo $am->M('FIELD_MAILTYPE') ?></label>
        <input checked="checked" onClick="javascript:usersOnChangeTemplateType('userMail');" type="radio" class="required" id="userMail_type" name="type" value="plaintext" />PlainText
        <input onClick="javascript:usersOnChangeTemplateType('userMail');" type="radio" class="required" id="userMail_type" name="type" value="html" />HTML
      </div>
      <div>
        <label for="body" class="required"><?php echo $am->M('FIELD_MAILBODY') ?></label>
      </div>
      <div>
          <textarea class="htmlcontent required" id="userMail_body" name="body" wrap="off"></textarea>
          <div id='userMail_templatevar'>
                <?php echo $am->M('INSERT') . ': ';  echo htmlSelectTemplateVar('userMail_body'); ?>
          </div>      
      </div>
    </fieldset>
    <div class="buttonrow">
        <div id="userMail_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SENDING') ?></span></div>
        <input type="hidden" name="username" value="" />
    	<input type="hidden" name="page" value="users" />
        <input type="hidden" name="action" value="mailtouser" />
        <input type="submit" id="userMail_submit" name="submit" value="<?php echo $am->M('BTN_SEND') ?>" />
        <input type="submit" id="userMail_clear" name="clear" value="<?php echo $am->M('BTN_CLEAR') ?>" onClick="javascript:usersResetForm('userMail'); return false;" />
        <input type="submit" id="userMail_cancel" name="cancel" value="<?php echo $am->M('BTN_CANCEL') ?>" onClick="javascript:usersShowForm('userMail','hide'); return false;" />
    </div>
    </form>
</div>
<!-- end of USERS userMail form -->

<div class="spacer30"></div>

<?php
    $prefix = 'index.php?page=users&';
    web_stepper( $prefix, $am->getTotalByType('authuserfile', $searchtext), $limit, $p);
?>

    <form id="usersList" name="usersList" method="post" action="index.php">
    <table class="list">
        <tr class="header">
            <th><input type="checkbox" name="usernames" value="" onClick="javascript:usersCheckUsers();"/></th>
            <th><?php echo $am->M('FIELD_USERNAME') ?></th>
            <th><?php echo $am->M('FIELD_REALNAME') ?></th>
            <th><?php echo $am->M('FIELD_EMAIL') ?></th>
            <th>Action</th>
        </tr>

<?php
        foreach ($usersArray as $user) {
	        $uname = addslashes($user['name']);
		        
	        print '<tr>';
	        print '<td id="check"><input type="checkbox" name="usernames[]" value="' . $uname . '" /></td>';
	        print '<td id="name">' . $user['name'] . '</td>';
	        print '<td>'. $user['info'] .'&nbsp;</td>';
	        print '<td>';
	        if (isset($user['email']) && $user['email'] != '') {
		        echo '<a href="javascript:usersShowForm(\'userMail\', \''.$uname.'\');"> '. $user['email'] . '</a>';
	        } else { 
		        echo $user['email'];
	        }
	        print '&nbsp;</td>';
	        print '<td id="action"><a href="javascript:usersShowForm(\'userEdit\', \''.$uname.'\');"><img src="images/edit.gif" border="0" alt="Edit"></a>';
	        print ' | <a href="javascript:usersShowForm(\'userDelete\', \''.$uname.'\');"><img src="images/decline.gif" border="0" alt="Delete"></a></td>';
	        print '</tr>';
        }
?>
    </table>

    <div class="listActions">
        <div class="listLeft">
        <select id="groupaction" onChange="javascript:usersShowForm(this.value, 'show'); $('deflistUserActions').selected=true;">
            <option id="deflistUserActions" value=""><?php echo $am->M('SELECTACTION') ?></option>
            <option value="usersMailSel" class="yellow-item"><?php echo $am->M('SENDMAILSEL') ?></option>
            <option value="usersDelSel" class="warn-item"><?php echo $am->M('DELETESELECTED') ?></option>
            <option value="usersDelAll" class="warn-item"><?php echo $am->M('DELETEALL') ?></option>
        </select>
        </div>

        <div class="listRight">
        <a href="javascript:usersShowForm('userAdd','show');"><?php echo $am->M('ADDNEWUSER') ?></a>
        </div>
    </div>

    <input type="hidden" name="page" value="users" />
    <input type="hidden" name="action" value="" />
    </form>
    <div class="spacer30"></div>
    
<?php if ($am->hasFeature('tinymcejs')) { ?>
<script type="text/javascript" xml:space="preserve">
//<![CDATA[
	tinyMCE.init({
	    mode : "none",
        <?php if ($am->isDemo()) { echo "plugins : \"noneditable\",\n"; } ?>
	    theme : "simple"
	});
//]]>
</script>
<?php } ?>
    
<?php
print "\n<script type=\"text/javascript\" xml:space=\"preserve\">\n//<![CDATA[\n";
print "var users = new Array();\n";
foreach ($usersArray as $user) {
	$uname = addslashes($user['name']);

	print 'users["'.$uname.'"] = new Array();' . "\n";
	foreach ($user as $k=>$v) {
		if ($k == 'pass' || $k == 'pass_raw') {
			continue;
		}
		echo ' users["'.$uname.'"]["'.$k.'"] = "'. addslashes($v) . '";' . "\n";
	}
}

echo getJsArray('emailTemplates');

// ------------- EDIT USERS - JS values --------------
if ($action == 'updateuser' && hasParam('username')) {
	$uname = addslashes( getParam('username', null, 'username') );
	echo "usersShowForm('userEdit', '" . $uname . "');\n";
}

// ------------- ADD USER - JS values --------------
if ($action == 'adduser' && isset($error)) {
	echo "usersShowForm('userAdd', 'show');\n";
}

print "//]]>\n</script>\n";
echo '</div>'; // righty
web_footer();
}
/* the end of 'users' module */


/**
 * Manage new user requests
 * 
 * @return void
 */
function showPage_signups()
{
	global $am;

	// Checking access rights
	if (!$am->isAdmin()) {
		$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
		showPage_401( true );
		exit;
	}

	$error = $message = null;
	$action = getParam('action', null, 'attribute');

    // ------------- Ajax Actions ----------------    

	// Ajax Action: checkusername
	if ($action == 'checkusername') {
		$username = getParam('username', null, 'username');

		if ($am->isRecordByType('authuserfile', $username)) {
			echo 'USERFOUND';
		} else {
			echo 'NOSUCHUSER';
		}
		return;
	}
	
	
	// ------------- General variables ----------------

	$limit = 10;
	if (hasParam('limit')) {
		$limit  = getParam('limit', 10, 'int');
		$_SESSION['user_limit'] = $limit;
	} else if (isset($_SESSION['user_limit'])) {
		$limit = $_SESSION['user_limit'];
	}

	$p = 1;
	if (hasParam('p')) {
		$p = getParam('p', 1, 'int' );
		$_SESSION['user_p'] = $p;
	} else if (isset($_SESSION['user_p'])) {
		$p = $_SESSION['user_p'];
	}

	if (empty($limit)) {
		$p = 1;
	}

	$offset = ($p-1) * $limit;

	// ------------- Actions ----------------

	// Action: approve
	if ($action == 'approve') {
		
		$username = getParam('username', null, 'username');
		if (!isset($username) || $username == '') {
			$error = $am->E('INVALIDREQUEST') . ' [username]';
		} else if ($am->isRecordByType('authuserfile', $username)) {
			$error = $am->E('USEREXISTS', $username);
		}

		$password = getParam('password', null, 'password');
		if (empty($password)) {
			$error = $am->E('INVALIDREQUEST') . ' [password]';
		}

		$data = array('name'=>$username,
		              'pass_raw'=>$password,
		              'info'=>getParam('realname', null, 'field'),
		              'email'=>getParam('email', null, 'email'));

		if (!isset($error)) {
			if (false == $am->setRecordByType( 'authuserfile', null,  $data, true )) {
				$error = $am->E('USERINSERTFAILED', $username)
				       . ': ' . $am->getError();
			}
		}

		if (!isset($error)) {
			if (!$am->setRecordByType('signupfile', $username, null, true)) {
				$error = $am->E('USERDELETEFAILED', $username) 
				       .  ': ' . $am->getError();
			}
		}
				
		if (!isset($error)) {
			$message = $am->M('USERINSSUCCESS', $username);
			$action = false;
		}

		// sending notification e-mail
		$am->setRuntimeValue('useradd_sendmail', hasParam('sendmail'), true);
		if (!isset($error) && hasParam('sendmail')) {
			$template = getParam('template', 'useradd', 'attribute');

			if (false == $am->sendMail($template, $data )) {
				$error = $am->getError();
			}
		}
		
	// Action: delete
	} else if ($action == 'delete') {

		$username = getParam('username', null, 'username');
		if (!$am->isRecordByType('signupfile', $username)) {
			$error = $am->E('USERNOTEXISTS', $username);
		}

		if (!isset($error)) {
			if (false == $am->setRecordByType( 'signupfile', $username, null, true )) {
				$error = $am->E('USERDELETEFAILED', $username) .
				': ' . $am->getError();
			}
		}
		if (!isset($error)) {
			$message = $am->M('MSG_SIGNUPUSERDELETED', $username);
		}
	
	// Action: delete selected users
	} else if ($action == 'deleteselected') {
		$usernames = getParam('usernames', null, 'username');
		foreach($usernames as $username) {

			if (!$am->isRecordByType('signupfile', $username)) {
				$error = $am->E('USERNOTEXISTS', $username);
				break;
			}

			if (false == $am->setRecordByType('signupfile', $username, null, true)) {
				$error = $am->E('USERDELETEFAILED', $username) .  ': ' . $am->getError();
				break;
			}

		}
		if (!isset($error)) {
			$message = $am->M('USERSSELDELETED');
		}

	// Action: delete ALL
	} else if ($action == 'deleteall') {

		if (!isset($error)) {
			if (false == $am->clearAllRecordsByType('signupfile', true)) {
				$error = $am->E('SIGNUPUSERSDELALLFAILED') 
				       . ': ' . $am->getError();
			}
		}
		if (!isset($error)) {
			$message = $am->M('MSG_SIGNUPUSERSALLDELETED');
		}
	
	}

	$signupsArray = $am->getRecordsByType( 'signupfile', 'name', $limit, $offset );

	web_header( $am->M('SUBTITLE_SIGNUPS'));
	web_menu();
	echo '<div id="righty">';

	// ------------- Information ----------------
	echo '<div class="pagenotes">';
	echo '<h4>' . $am->M('SUBTITLE_SIGNUPS') . '</h4>';

	echo '<div><div class="name">' . $am->M('SINGUPREQUESTS') . '</div>';
	echo '<div class="value">' . $am->getTotalByType('signupfile') . '</div></div>';

	echo '</div>';

	// ------------- Message ----------------
	web_message( $message, $error );
	
?>
    <h1><?php echo $am->M('SUBTITLE_SIGNUPS') ?></h1>

 <?php
    $file = $am->getPathByType('signupfile');
    if (false == $file) {
    	echo '<div class="block">';
    	echo 'No signup file found';
		echo '</div>';
    }
 ?>
 	<div class="spacer30"></div>
 	
 	
    <!-- approve / add form -->
    <div id="approve_container" style="display: none;">
    <form id="approve" name="approve" method="post">

    <fieldset>
    <legend><?php echo $am->M('LEGEND_SIGNUPINFORMATION') ?></legend>
        
        <div>
            <label for="datetime"><?php echo $am->M('FIELD_DATETIME') ?></label>
            <span id="approve_datetime" name="datetime"></span>
        </div>

        <div>
            <label for="remoteaddr"><?php echo $am->M('FIELD_REMOTEADDR') ?></label>
            <span id="approve_remoteaddr" name="remoteaddr"></span>
        </div>
        
        <div>
            <label for="referer"><?php echo $am->M('FIELD_REFERER') ?></label>
            <span id="approve_referer" name="referer"></span>
        </div>

    </fieldset>

    <div id="approve_checkusername" style="float:right;"></div>
    <p id="approve_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>

    
    <fieldset>
    <legend><?php echo $am->M('LEGEND_SIGNUPAPPROVE') ?></legend>
    
        <div>
            <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
            <input id="approve_username" type="text" name="username" maxlength="255" class="required" />
            <input id="approve_check" name="check" type="button" value="<?php echo $am->M('CHECK') ?>" onclick="javascript:mainCheckUsername2('approve');" />
        </div>
        <div>
        <label for="password" class="required"><?php echo $am->M('FIELD_PASSWORD') ?></label>
            <input id="approve_password" type="text" name="password" maxlength="255" class="required" />
            <input type="button" value="<?php echo $am->M('GENERATE') ?>" onclick="javascript:mainGeneratePassword2('approve');" />
        </div>
        <div>
        	<label for="realname"><?php echo $am->M('FIELD_REALNAME') ?></label>
            <input id="approve_realname" type="text" name="realname" size="40" maxlength="255" />
        </div>
        <div>
        <label for="email"><?php echo $am->M('FIELD_EMAIL') ?></label>
            <input id="approve_email" type="text" name="email" size="40" maxlength="255" />
        </div>
        
        <div class="dashed"></div>
<?php
$sendMail = true;
if (false == $am->hasFeature('PHPMailer')) {
	$sendMail = false;

} else if ($am->hasRuntimeValue('useradd_sendmail') && $am->getRuntimeValue('useradd_sendmail') != "1") {
	$sendMail = false;
}
?>        
        <div>
        	<label for="sendmail"><?php echo $am->M('FIELD_WELCOMEEMAIL') ?></label>
            <input type="checkbox" id="approve_sendmail" name="sendmail" value="1" 
                   <?php if ($sendMail) { echo ' checked="checked" '; } ?>
                   onClick="javascript:mainOnChangeSendMail2('approve');" /> <?php echo $am->M('SEND') ?>
        </div>

<?php if (false == $am->hasFeature('PHPMailer')) {        
		echo '<div class="block">';
		echo $am->W('SENDMAILFAIL_INSTALLPHPMAILER');
		echo '</div>';
      }
?>	 
       
        <div id="approve_showtemplate" style="display:<?php echo $sendMail ? 'block' : 'none' ?>;">
        	<label for="template"><?php echo $am->M('FIELD_EMAILTEMPLATE') ?></label>
        	<select id="approve_template" name="template">
        		<option value=""><?php echo $am->M('SELECTONE') ?></option>
        		<?php 
        		foreach ($am->getTemplates() as $id=>$tpl) {
        			echo '<option value="' . addslashes($id).'"';
        			if ($tpl['role'] == 'useradd') {
        				echo ' selected="selected"';
        			}
        			echo '>';
        			echo $tpl['name'] . '</option>';
        		}
        		?>
        	</select>
        </div>        
    </fieldset>
    
    <div class="buttonrow">
        <input type="hidden" name="action" value="approve" />
        <input type="hidden" name="page" value="signups" />
        <?php echo htmlSubmit('submit'); ?>
        <?php echo htmlReset('reset', 'onClick="javascript:signupsResetForm(\'approve\'); return false;"' ); ?>
        <?php echo htmlInput('submit', 'approve_cancel', $am->M('CANCEL'), "onClick=\"javascript:signupsShowForm('approve','hide'); return false;\"") ?>
    </div>
    </form>

    <div class="spacer30"></div>
    </div>
    <!-- end of approve / add form -->

            
    <!-- Signup delete form -->
    <div id="delete_container" style="display: none;">
    <form id="delete" name="delete">
    <fieldset>
    <legend><?php echo $am->M('LEGEND_SIGNUPDELETE') ?></legend>
       <p id="delete_msg" class="formmessage"><?php echo $am->M('Q_SIGNUPDELETE'); ?></p>
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="username" value="" />
        <input type="hidden" name="page" value="signups" />
        <input type="hidden" name="action" value="delete" />
        <input type="submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
        <input type="submit" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:signupsShowForm('delete','hide'); return false;" />
    </div>
    </form>

    <div class="spacer30"></div>
    </div>
    <!-- end of Signup delete form -->
     
    <!-- USERS delete selected form -->
    <div id="deleteselected_container" style="display: none;">
    <form id="deleteselected" name="deleteselected" method="post">
    <fieldset>
    <legend><?php echo $am->M('LEGEND_USERDELSEL') ?></legend>
       <p id="deleteselected_msg" class="formmessage"><?php echo $am->M('Q_USERDELSEL'); ?></p>
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="page" value="signups" />
        <input type="hidden" name="action" value="deleteselected" />
        <input type="submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
        <input type="submit" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:signupsShowForm('deleteselected','hide'); return false;" />
    </div>
    </form>
    <div class="spacer30"></div>
    </div>
    <!-- end of USERS delete selected form -->

    <!-- USERS delete all form -->
    <div id="deleteall_container" style="display: none;">
    <form id="deleteall" name="deleteall" method="post">
    <fieldset>
    <legend><?php echo $am->M('LEGEND_SIGNUPUSERDELALL') ?></legend>
       <p id="deleteall_msg" class="formmessage"><?php echo $am->M('Q_USERDELALL'); ?></p>
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="page" value="signups" />
        <input type="hidden" name="action" value="deleteall" />
        <input type="submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
        <input type="submit" name="cancel" value="<?php echo $am->M('CANCEL') ?>" onClick="javascript:signupsShowForm('deleteall','hide'); return false;" />
    </div>
    </form>
    <div class="spacer30"></div>
    </div>
    <!-- end of USERS delete all form -->
   

<?php
$prefix = 'index.php?page=signups&';
web_stepper( $prefix, $am->getTotalByType('signupfile'), $limit, $p);
?>

    <form id="usersList" name="usersList" method="post" action="index.php">
    <table class="list">
        <tr class="header">
            <th><input type="checkbox" name="usernames" value="" onClick="javascript:usersCheckUsers();"/></th>
            <th><?php echo $am->M('FIELD_USERNAME') ?></th>
            <th><?php echo $am->M('FIELD_REALNAME') ?></th>
            <th><?php echo $am->M('FIELD_EMAIL') ?></th>
            <th><?php echo $am->M('FIELD_REMOTEADDR') ?></th>
            <th>Action</th>
        </tr>

<?php
foreach ($signupsArray as $data) {
	$uname = addslashes($data['name']);
		
	print '<tr>';
	print '<td id="check"><input type="checkbox" name="usernames[]" value="' . $uname . '" /></td>';
	print '<td id="name">' . $data['name'] . '</td>';
	print '<td>'. $data['info'] .'&nbsp;</td>';
	print '<td>';
	echo $data['email'];
	print '&nbsp;</td>';
	
	// remoteaddr
	echo '<td>';
	echo $data['remoteaddr'];
	// echo '<br />';
	// echo htmlspecialchars($data['referer']);
	echo '</td>';
	
	print '<td id="action"><a href="javascript:signupsShowForm(\'approve\', \''.$uname.'\');"><img src="images/approve.gif" border="0" alt="Approve"></a>';
	print ' | <a href="javascript:signupsShowForm(\'delete\', \''.$uname.'\');"><img src="images/decline.gif" border="0" alt="Decline"></a></td>';
	print '</tr>';
}

?>
    </table>

    <div class="listActions">
        <div class="listLeft">
        <select id="groupaction" onChange="javascript:signupsShowForm(this.value, 'show'); $('deflistActions').selected=true;">
            <option id="deflistActions" value=""><?php echo $am->M('SELECTACTION') ?></option>
            <option value="">-------------------</option>
            <option value="deleteselected"><?php echo $am->M('DELETESELECTED') ?></option>
            <?php if (count($signupsArray) > 0) { ?>
            	<option value="deleteall"><?php echo $am->M('DELETEALL') ?></option>
            <?php } ?>
        </select>
        </div>
    </div>

    <input type="hidden" name="page" value="signups" />
    <input type="hidden" name="action" value="" />
    </form>

    <div class="spacer30"></div>
    
   
<?php
print "\n<script type=\"text/javascript\" xml:space=\"preserve\">\n//<![CDATA[\n";
print "var signups = new Array();\n";
foreach ($signupsArray as $data) {
	$uname = addslashes($data['name']);

	print 'signups["'.$uname.'"] = new Array();' . "\n";
	foreach ($data as $k=>$v) {
		if ($k == 'referer') {
			$v = strlen($v) > 40 ? substr($v, 0, 40) . '...' : $v;
		}
		if ($k == 'ts') {
			$v = gmdate('D, d M Y H:i:s', $v) . ' GMT';
		}
 		echo ' signups["'.$uname.'"]["'.$k.'"] = "'. addslashes($v) . '";' . "\n";
	}
}

// ------------- EDIT Signup JS values --------------
if ($action == 'approve' && hasParam('username')) {
	$uname = addslashes( getParam('username', null, 'username') );
	echo "signupsShowForm('approve', '" . $uname . "');\n";
}

print "//]]>\n</script>\n";
echo '</div>'; // righty
web_footer();
}
/* the end of signups module */


/**
 * System Operations
 *
 * @return void
 */
function showPage_settings()
{
    global $am;

    if (!$am->isAdmin( true )) {
    	$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
        showPage_401();
        exit;
    }

    $message = $error = null;
    $action = getParam('action', null, 'attribute');

    ### ADMIN: update
    if ($action == 'updateadmin') {

        $username = getParam('username', null, 'username');
        if ($username == '') {
            $error = $am->E('INVALIDREQUEST');
        }

        if (!isset($error)) {
            list($admin) = $am->getRecordsByType( 'authadminfile', null, 1, 0 );

            $data = $admin;

            $data['name'] = $username;
            $data['info'] = getParam('realname', null, 'field');
            $data['email'] = getParam('email', null, 'email');

            if (hasParam('password')) {
                $rawpass = getParam('password', null, 'password');
                if (isset($rawpass) && $rawpass != '') {
                    $data['pass'] = $am->htcrypt($rawpass);
                }
            }

            # update / insert
            if (!$am->setRecordByType( 'authadminfile', null, $data, true, true )) {
                $error = $am->E('ADMINUPDATEFAILED', $username).': '.$am->getError();
            }
        }

        if (!isset($error)) {
            $am->loginAs( $data['name'], $data['pass']);
        	$message = $am->M('ADMINUPDSUCCESS', $username);
        }
    }

    ### ADMIN: download file
    if ($action == 'downloadfile') {
        $filetype = getParam('filetype', null, 'attribute');

        $contents = $am->getFileContentsByType($filetype, true); // raw
        if (!$am->isError()) {
            header('Content-Type: text/plain');
            header('Content-Disposition: attachment; filename='.$filetype.'.txt');

            echo $contents;
            return;
        }
        $error = $am->getError();
    }
  
    ### ADMIN: download apache files
    if ($action == 'downloadapache') {

        $zip_path = '/usr/bin/zip';
        if (!is_file($zip_path) || !is_executable($zip_path)) {
            $error = 'ZIP archiver not found';
        }

        $filesArray = array($am->getPathByType('accessfile'),
                            $am->getPathByType('authadminfile'),
                            $am->getPathByType('authuserfile'),
                            $am->getPathByType('accessfile_dist'),
                            $am->getPathByType('authuserfile_dist'));
//                            $am->getPathByType('authgroupfile'),
//                            $am->getPathByType('authgroupfile_dist'));

        $zip_file = $am->getTempFilePath() . '.zip';

        ob_start();
        foreach ($filesArray as $filepath) {
            $cmd = $zip_path . ' -u ' . $zip_file . ' ' . $filepath;
            
            $retval = null;
            passthru( $cmd, $retval );
        }
        // $val = ob_get_contents();
        ob_end_clean();

        if (!isset($error)) {
            header('Content-Type: application/x-zip-compressed');
            header('Content-Disposition: attachment; filename=authman_files.zip');
            header('Content-Transfer-Encoding: binary');

            readfile( $zip_file );
            unlink( $zip_file );

            return;
        }
    }
 
    ### ADMIN: update file (ajax)
    if ($action == 'savefile') {
        $filetype = getParam('filetype', null, 'attribute');
        $contents = getParam('contents', null, 'htmlcode');

        $filepath = $am->getPathByType($filetype);
        // save contents as raw data
        if (!isset($error)) {
            $results = $am->setFileContentsByType( $filetype, $contents, true, true); 
            if (!$results) {
                $error = $am->getError();
            }
        }

        if (!isset($error)) {
                $message = $am->M('FILEUPDSUCCESS', $filepath);
        }

        if (!isset($error)) {
            print '<div class="message">' . fmt_message($message) . "</div>\n";
        } else {
            print '<div class="warning">' . fmt_message($error) . "</div>\n";
        }

        return; // ajax only
    }
  
    ### ADMIN: reset protected (ajax)
    if ($action == 'resetprotected') {
        if (false == $am->resetProtectedDirectory()) {
            $error = $am->getError();
        }
       
        if (!isset($error)) {
            $message = $am->M('REBLDSUCCESS');
        }

        if (!isset($error)) {
            print '<div class="message">' . fmt_message($message) . "</div>\n";
        } else {
            print '<div class="warning">' . fmt_message($error) . "</div>\n";
        }

        exit;
    }
   
    ### ADMIN: adding & updating email template
    if ($action == 'addtemplate' || $action == 'updatetemplate') {
        if ($action == 'updatetemplate') {
            $templateid = getParam('templateid', null, 'field');

            if (!isset($templateid) || $templateid=='') {
                $error = $am->E('INVALIDREQUEST') . ' [templateid]';
            }
        } else {
            $templateid = time();
        }

        $name    = getParam('templatename', null, 'field');
        $type    = getParam('templatetype', null, 'attribute');
        $subject = getParam('templatesubject', null, 'subject');
        $body    = getParam('templatebody', null, 'htmlcode');

        if (!isset($error)) {  
            if (!isset($name) || $name=='') {
                $error = $am->E('INVALIDREQUEST') . ' [name]';
            } else if (!isset($type) || $type=='') {
                $error = $am->E('INVALIDREQUEST') . ' [type]';
            }
        }


        if (!isset($error)) {  
            $data = array('name'=>$name,
                          'type'=>$type,
                          'contents'=>$body,
                          'subject'=>$subject);

            $force = $action == 'updatetemplate';
            if (false == $am->saveTemplateAs( $templateid, $data, $force )) {
                $error = $am->getError();
            }
        }

        if (!isset($error)) {  
            if ($action == 'addtemplate') {
                $action = 'updatetemplate';
                $message = $am->M('MSG_TPLCREATED');
            } else {
                $message = $am->M('MSG_TPLUPDATED');
            }
        }

    }

    ### ADMIN: adding & updating email template
    if ($action == 'deletetemplate') {

        $templateid = getParam('templateid', null, 'field');
        if (!isset($templateid) || $templateid=='') {
            $error = $am->E('INVALIDREQUEST') . ' [templateid]';
        }

        if (false  == $am->deleteTemplate( $templateid )) {
            $error = $am->getError();
        }

        if (isset($error)) {  
            $action = 'updatetemplate';
        } else {
            $templateid = null;
            $message = $am->M('MSG_TPLDELETED');
        }
    }

    $tplsArray = $am->getTemplates();

    web_header( $am->M('SUBTITLE_SETTINGS') );
    web_menu();
?>
<div id="righty">

<!-- Top Page Notes -->
<div class="pagenotes">
  <h4><?php echo $am->M('SUBTITLE_SETTINGS') ?></h4>

  <div><div class="name"><?php echo $am->M('PROTECTEDDIRECTORY') ?></div>
  <div class="value"><?php echo $am->getPathByType('protecteddirectory') ?></div></div>

  <div><div class="name">SystemAuthFile</div>
  <div class="value"><?php echo $am->getPathByType('authadminfile') ?></div></div>
</div>

<!-- Submenu -->
<div id="#menuright" class="actions">
  <ul>
    <li><a href="javascript:void(0);" onClick="javascript:setsShowForm('editAdmin','show');"><?php echo $am->M('MENU_EDITADMIN') ?></a></li>
    <li><a href="javascript:void(0);" onClick="javascript:setsShowForm('chooseTpl','show');"><?php echo $am->M('MENU_EMAILTPLS') ?></a></li>
    <li><a href="javascript:void(0);" onClick="javascript:setsShowForm('downFiles','show');"><?php echo $am->M('MENU_DOWNLOAD') ?></a></li>
    <li><a href="javascript:void(0);" onClick="javascript:setsShowForm('prefFiles','show');"><?php echo $am->M('MENU_PREFFILES') ?></a></li>
    <li><a href="javascript:void(0);" onClick="javascript:setsShowForm('resetProtected','show');"><?php echo $am->M('MENU_PROTDIR') ?></a></li>
  </ul>
</div>

<!-- Page Message -->
<?php
web_message( $message, $error );
?>

<!-- Settings FORM: EDIT ADMIN Account -->
<div id="editAdmin" style="display: none;">
  <h1><?php echo $am->M('EDITADMIN') ?></h1>
  <form id="editAdmin_form" name="editAdmin_form" method="post">
  <p id="editAdmin_form_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>

  <fieldset>
    <legend>Account Details</legend>
    <div>
      <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
      <input id="username" type="text" name="username" maxlength="255" class="required" />
    </div>
    <div>
      <label for="password"><?php echo $am->M('FIELD_NEWPASSWORD') ?></label>
      <input id="password" type="text" name="password" maxlength="255" />
      <input type="button" value="<?php echo $am->M('GENERATE') ?>" onclick="javascript:mainGeneratePassword('editAdmin');" />
    </div>
    <div>
      <label for="realname"><?php echo $am->M('FIELD_REALNAME') ?></label>
      <input id="realname" type="text" name="realname" size="40" maxlength="255" />
    </div>
    <div>
      <label for="email"><?php echo $am->M('FIELD_EMAIL') ?></label>
      <input id="email" type="text" name="email" size="40" maxlength="255" />
    </div>
  </fieldset>

  <div class="buttonrow">
      <div id="editAdmin_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" />
        <span><?php echo $am->M('SAVING') ?></span>
      </div>
      <input type="hidden" name="page" value="settings" />
      <input type="hidden" name="action" value="updateadmin" />
      <?php echo htmlSubmit('submit') ?>
      <?php echo htmlReset('reset', 'onClick="javascript:setsResetForm(\'editAdmin\'); return false;"') ?>
      <input type="submit" id="cancel" value="<?php echo $am->M('CANCEL') ?>" 
             onClick="javascript:setsShowForm('editAdmin','hide'); return false;" />
  </div>

  </form>
</div>

<!-- Settings FORM: DOWNLOAD Apache Files -->
<div id="downFiles" style="display: none;">
  <h1><?php echo $am->M('DOWNLOADFILES') ?></h1>
  <div class="block">
  <?php 
      echo '<a href="index.php?page=settings&action=downloadapache">';
      echo  $am->M('CLICKHERE') .'</a> '. $am->M('TODOWNAPACHEFILES');
  ?>
  </div>
</div>

<!-- Settings Form: Prefered Files (ajax) -->
<div id="prefFiles" style="display: none;">
<h1><?php echo $am->M('PREFFILES') ?></h1>

    <div class="block"><?php echo $am->M('DESCR_PREFFILES') ?></div>

    <!-- DIST: ACCESS FILE -->
    <form id="daccessfile_form" name="daccessfile_form" onSubmit="javascript:setsPrefFilesUpdate('daccessfile');return false;" method="post">
<?php
        $filepath = $am->getPathByType('accessfile_dist');

        print '<div id="daccessfile_message">';
        if (false == $filepath) {
            print '<div class="warning">' . $am->W('NOTDEFINED', 'AccessFile (prefered)') . '</div>';
        }
        print '</div>';
?>
    <fieldset>
    <legend>AccessFile</legend>
        <div><textarea class="filecontent" name="contents" id="daccessfile_contents" 
            wrap="off"><?php echo $am->getFileContentsByType('accessfile_dist', true); ?></textarea>
        </div>
        <div class="fieldnotes"><?php echo $am->M('PREFFILERESTAS', $am->getPathByType('accessfile')) ?></div>
    </fieldset>
    <div class="buttonrow">
        <div id="daccessfile_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" name="filetype" value="accessfile_dist">
        <input type="hidden" name="ajax" value="1">
        <input type="hidden" name="action" value="savefile">
        <input type="hidden" name="page" value="settings">
        <?php echo htmlSubmit('daccessfile_submit') ?>
        <?php echo htmlReset('daccessfile_reset') ?>
<?php if (is_file($am->getPathByType('accessfile_dist'))) { ?>
        | <a href="javascript:setsPrefFilesDownload('accessfile_dist');"><?php echo $am->M('DOWNLOAD') ?></a>
<?php } else { ?>
        | <strong style="color: red;">File not found</strong>
<?php } ?>
    </div>
    </form>
    
	<div class="spacer30"></div>
    
    <!-- DIST: AUTH USER FILE -->
    <form id="dauthuserfile_form" name="dauthuserfile_form" onSubmit="javascript:setsPrefFilesUpdate('dauthuserfile');return false;" method="post">
<?php
        $filepath = $am->getPathByType('authuserfile_dist');

        print '<div id="dauthuserfile_message">';
        if (false == $filepath) {
            print '<div class="warning">' . $am->W('NOTDEFINED', 'AccessFile (prefered)') . '</div>';
        }
        print '</div>';
?>
    <fieldset>
    <legend>AuthUserFile</legend>
    <div><textarea class="filecontent" name="contents" id="dauthuserfile_contents" 
                   wrap="off"><?php echo $am->getFileContentsByType('authuserfile_dist', true); ?></textarea>
    </div>
    <div class="fieldnotes">
    	<?php 
    		echo $am->M('PREFFILERESTAS', $am->getPathByType('authuserfile'));
    		echo '<br />' . $am->M('DEPENSONACCESSFILE');
    	?></div>
    </fieldset>
    <div class="buttonrow">
        <div id="dauthuserfile_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" name="filetype" value="authuserfile_dist">
        <input type="hidden" name="ajax" value="1">
        <input type="hidden" name="action" value="savefile">
        <input type="hidden" name="page" value="settings">
        <?php echo htmlSubmit('dauthuserfile_submit') ?>
        <?php echo htmlReset('dauthuserfile_reset') ?>
<?php if (is_file($am->getPathByType('authuserfile_dist'))) { ?>
        | <a href="javascript:setsPrefFilesDownload('authuserfile_dist');"><?php echo $am->M('DOWNLOAD') ?></a>
<?php } else { ?>
        | <strong style="color: red;">File not found</strong>
<?php } ?>        
    </div>
    </form>

	<div class="spacer30"></div>
    
    <!-- DIST: AUTH GROUP FILE -->
<!--
    <form id="dauthgroupfile_form" name="dauthgroupfile_form" onSubmit="javascript:setsPrefFilesUpdate('dauthgroupfile');return false;">
<?php
        $filepath = $am->getPathByType('authgroupfile_dist');

        print '<div id="dauthgroupfile_message">';
        if (false == $filepath) {
            print '<div class="warning">' . $am->W('NOTDEFINED', 'AccessFile (prefered)') . '</div>';
        } else if (!is_file($filepath)) {
            print '<div class="warning">' . $am->E('NOSUCHFILE', $filepath) . '</div>';
        }
        print '</div>';
?>
    <fieldset>
        <legend>AuthGroupFile</legend>
        <div><textarea class="filecontent" name="contents" id="dauthgroupfile_contents" 
                       wrap="off"><?php echo $am->getFileContentsByType('authgroupfile_dist', true); ?></textarea>
        </div>
        <div class="fieldnotes"><?php echo $am->M('PREFFILERESTAS', $am->getPathByType('authgroupfile')) ?></div>
    </fieldset>
    <div class="buttonrow">
        <div id="dauthgroupfile_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" name="filetype" value="authgroupfile_dist">
        <input type="hidden" name="ajax" value="1">
        <input type="hidden" name="action" value="savefile">
        <input type="hidden" name="page" value="settings">
        <?php echo htmlSubmit('dauthgroupfile_submit') ?>
        <?php echo htmlReset('dauthgroupfile_reset') ?>
        | <a href="javascript:setsPrefFilesDownload('authgroupfile_dist');"><?php echo $am->M('DOWNLOAD') ?></a>
    </div>
    </form>
-->

</div>

<!-- Settings Form: Reset Protected Directory (ajax) -->
<div id="resetProtected" style="display: none;">
    <h1><?php echo $am->M('REBLPROTDIR') ?></h1>
    <div class="block"><?php echo $am->M('DESCR_REBLDPROTDIR') ?></div>

    <form id="resetProtected_form" name="resetProtected_form" method="post">
    <fieldset>
        <legend><?php echo $am->M('NOTES') ?></legend>
        <?php echo $am->M('NOTES_REBLDPROTDIR') ?>
    </fieldset>
    <div class="buttonrow">
        <div id="resetProtected_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" name="ajax" value="1" />
        <input type="hidden" name="page" value="settings" />
        <input type="hidden" name="action" value="resetprotected" />
        <?php echo htmlInput( 'submit', 'resetProtected_submit', $am->M('CONFIRM') ) ?>
        <?php echo htmlInput( 'submit', 'resetProtected_cancel', $am->M('CANCEL'), 'onClick="javascript:setsShowForm(\'resetProtected\',\'hide\'); return false;"') ?>
    </div>
    </form>
</div>

<!-- Settings Form: Edit Email Templates -->
<div id="chooseTpl" style="display: none;">
    <h1><?php echo $am->M('EDITEMAILTPLS') ?></h1>
    <form id="chooseTpl_form" name="chooseTpl_form">
    <fieldset>
      <legend><?php echo $am->M('LEGEND_TEMPLATESEL') ?></legend>
      <div>
        <label for="templateid" class="required"><?php echo $am->M('FIELD_TPLNAME') ?></label>
        <select id="templateid" onChange="javascript:setsOnChangeTemplate( this.value );">
            <option id="chooseTpl_default" value=""><?php echo $am->M('SELECTONE') ?></option>
<?php
        foreach($tplsArray as $id=>$tpl) {
            print '<option value="' . $id . '">' . $tpl['name'] . '</option>';
        }
?>
        </select>
        | <a href="javascript:setsOnNewTemplate();">Add New</a>
      </div>
    </fieldset>
    </form>
    
    <div class="spacer30"></div>
</div>

<div id="addTpl" style="display: none;">
    <form id="addTpl_form" name="addTpl_form" method="post" method="post">
    <p id="addTpl_form_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>
    <fieldset>
      <legend><?php echo $am->M('LEGEND_TEMPLATENEW') ?></legend>
      <div>
        <label for="templatename" class="required"><?php echo $am->M('FIELD_TPLNAME') ?></label>
        <input id="templatename" name="templatename" type="text" maxlength="255" class="required" />
      </div>
      <div>
        <label for="type" class="required"><?php echo $am->M('FIELD_TPLTYPE') ?></label>
        <input onClick="javascript:setsOnChangeTemplateType('addTpl');" type="radio" class="required" id="templatetype" name="templatetype" value="plaintext" checked="checked" />PlainText
        <input onClick="javascript:setsOnChangeTemplateType('addTpl');" type="radio" class="required" id="templatetype" name="templatetype" value="html" />HTML
      </div>

      <div>
        <label for="templatesubject" class=""><?php echo $am->M('FIELD_TPLSUBJECT') ?></label>
        <input id="templatesubject" name="templatesubject" type="text" maxlength="255" class="" />
      </div>

      <div>
        <label for="templatebody" class=""><?php echo $am->M('FIELD_TPLBODY') ?></label>
      </div>
      <div>
            <textarea class="htmlcontent" id="addTpl_templatebody" name="templatebody" wrap="off"></textarea>
      </div>
      <div id='addTpl_templatevar'>
            <?php echo $am->M('INSERT') . ': ';  echo htmlSelectTemplateVar('addTpl_templatebody'); ?>
      </div>      
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="page" value="settings" />
        <input type="hidden" name="action" value="addtemplate" />
        <?php echo htmlSubmit('submit') ?>
        <input type="submit" id="cancel" value="<?php echo $am->M('CANCEL') ?>" 
               onClick="javascript:setsShowForm('addTpl','hide'); return false;" />
    </div>
    </form>
</div>

<div id="editTpl" style="display: none;">
    <form id="editTpl_form" name="editTpl_form" method="post">
    <p id="editTpl_form_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>
    <fieldset>
      <legend><?php echo $am->M('LEGEND_TEMPLATEEDIT') ?></legend>
      <div>
        <label for="templatename" class="required"><?php echo $am->M('FIELD_TPLNAME') ?></label>
        <input id="templatename" name="templatename" type="text" maxlength="255" class="required" />
      </div>
      <div>
        <label for="type" class="required"><?php echo $am->M('FIELD_TPLTYPE') ?></label>
        <input onClick="javascript:setsOnChangeTemplateType('editTpl');" type="radio" class="required" id="templatetype" name="templatetype" value="plaintext" />PlainText
        <input onClick="javascript:setsOnChangeTemplateType('editTpl');" type="radio" class="required" id="templatetype" name="templatetype" value="html" />HTML
      </div>

      <div>
        <label for="templatesubject" class=""><?php echo $am->M('FIELD_TPLSUBJECT') ?></label>
        <input id="templatesubject" name="templatesubject" type="text" maxlength="255" class="" />
      </div>

      <div>
        <label for="templatebody" class=""><?php echo $am->M('FIELD_TPLBODY') ?></label>
      </div>
      <div>
            <textarea class="htmlcontent" id="editTpl_templatebody" name="templatebody" wrap="off"></textarea>
      </div>
      <div id='editTpl_templatevar'>
            <?php echo $am->M('INSERT') . ': ';  echo htmlSelectTemplateVar('editTpl_templatebody'); ?>
      </div>
    </fieldset>

    <div class="buttonrow">
        <div id="editTpl_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" id="templateid" name="templateid" value="" />
        <input type="hidden" name="page" value="settings" />
        <input type="hidden" name="action" value="updatetemplate" />
        <?php echo htmlSubmit('submit') ?>
        <?php echo htmlReset('reset', 'onClick="javascript:setsResetForm(\'editTpl\'); return false;"') ?>
        <?php echo htmlInput( 'submit', 'delete_submit', $am->M('BTN_DELETE'), 'onClick="javascript:return setsOnDeleteTemplate();"' ) ?>
        <input type="submit" id="cancel" value="<?php echo $am->M('CANCEL') ?>" 
               onClick="javascript:setsShowForm('editTpl','hide'); return false;" />
    </div>
    </form>
</div>


<div class="spacer30"></div>

<?php if ($am->hasFeature('tinymcejs')) { ?>
<script type="text/javascript" xml:space="preserve">
//<![CDATA[
	tinyMCE.init({
	    mode : "none",
        <?php if ($am->isDemo()) { echo "plugins : \"noneditable\",\n"; } ?>
	    theme : "simple"
	});
//]]>
</script>
<?php } ?>

<script type="text/javascript" xml:space="preserve">
//<![CDATA[
<?php
    // Javascript code: updateadmin 
    if ($action == 'updateadmin' && (isset($error) || isset($message))) {
        print "setsShowForm('editAdmin','show');\n";
    }

    echo getJsArray('emailTemplates');
    
    // Edit template js
    if ($action == 'updatetemplate' && isset($templateid) && (isset($error) || isset($message))) {
        print "setsSetChooseTpl('$templateid');\n";
        print "setsShowForm('editTpl','" . $templateid . "');\n";
    }

    // Add template js
    if ($action == 'addtemplate' && isset($error)) {
        print "setsShowForm('addTpl','show');\n";
    }
  
    // Delete template js
    if ($action == 'deletetemplate' && (isset($error) || isset($message))) {
        print "setsShowForm('chooseTpl','show');\n";
    }
?>
//]]>
</script>

<!-- end of righty -->
</div>

<?php
    web_footer();
}
/* the end of the settings module */


/**
 * Files module
 * Edit raw file: .htaccess, .htpasswd
 *
 * @return void
 */
function showPage_files()
{
    global $am;

    // check access rights
    if (!$am->isAdmin()) {
    	$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
        showPage_401( true );
        exit;
    }

    $message = $error = null;
    $action = getParam('action', null, 'attribute');
    
    // Action: 'downloadfile': download raw file
    if ($action == 'downloadfile') {
        $filetype = getParam('filetype', null, 'attribute');

        $contents = $am->getFileContentsByType($filetype, true); 
        if (!$am->isError()) {
            header('Content-Type: text/plain');
            header('Content-Disposition: attachment; filename='.$filetype.'.txt');

            echo $contents;
            return;
        }
        $error = $am->getError();
    }

    // Action: 'savefile': save raw file
    if ($action == 'savefile') {
        $filetype = getParam('filetype', null, 'attribute');
        $contents = getParam('contents', null, 'htmlcode');

        $filepath = $am->getPathByType($filetype);

        if (!$am->isManualEdit()) {
            $error = $am->W('MANUALEDITOFF');
        }
 
        // save contents as raw data
        if (!isset($error)) {
            $results = $am->setFileContentsByType( $filetype, $contents, true, true); 
            if (!$results) {
                $error = $am->getError();
            }
        }

        if (!isset($error)) {
            $message = $am->M('FILEUPDSUCCESS', $filepath);
        }

        // authuserfile and authgroupfile are updated via ajax request
        if ($filetype == 'authuserfile' || $filetype == 'authgroupfile') {
            if (empty($error)) {
                print '<div class="message">' . fmt_message($message) . "</div>\n";
            } else {
                print '<div class="warning">' . fmt_message($error) . "</div>\n";
            }
            return;
        }

    }
   
    web_header( $am->M('SUBTITLE_FILES') );
    web_menu();
?>
    <div id="righty">

    <div class="pagenotes">
    <h4><?php echo $am->M('SUBTITLE_FILES') ?></h4>
        <div><div class="name">DocumentRoot</div>
        <div class="value"><?php echo $_SERVER['DOCUMENT_ROOT']; ?></div></div>
        
        <div><div class="name">IP Address</div>
        <div class="value"><?php echo $_SERVER['REMOTE_ADDR']; ?></div></div>
    </div>
<?php  
    // ------------- Message ----------------
    web_message( $message, $error );

    // ------------- AccessFile ----------------
    $filepath = $am->getPathByType('accessfile');
    print '<div id="accessfile_message">';
    if (!is_file($filepath)) {
        print '<div class="block">';
        print '<img src="images/icon_warning.gif" width="20" height="20" align="left" />';
        print $am->M('NOTES_NOACCESSFILE', $filepath);
        print '</div>';
    } 
    print '</div>';
?>
    <form id="accessfile_form" name="accessfile_form" method="post" action="index.php">
    <fieldset>
        <legend>AccessFile</legend>
        <div>
            <textarea wrap="off" name="contents" id="accessfile_contents" 
                <?php echo $am->isDemo() || !$am->isManualEdit() ?  ' readonly' : ''; ?>
                class="filecontent"><?php echo $am->getFileContentsByType('accessfile', true); ?></textarea>
        </div>
        <div class="fieldnotes">File: <?php echo $filepath ?></div>
    </fieldset>
    <div class="buttonrow">
        <input type="hidden" name="filetype" value="accessfile">
        <input type="hidden" name="action" value="savefile">
        <input type="hidden" name="page" value="files">
<?php 
    if ($am->isManualEdit()) {
        echo htmlSubmit('accessfile_submit');
        echo htmlReset('accessfile_reset');
        if (is_file($filepath)) {
            echo '| <a href="javascript:filesDownload(\'accessfile\');">' . $am->M('DOWNLOAD') . '</a>';
        }
    }
?>
    </div>
    </form>

    <div class="spacer30"></div>
<?php

    $filepath = $am->getPathByType('authuserfile');

    print '<div id="authuserfile_message">';
    if (false == $filepath) {
        print '<div class="block">';
        print $am->M('NOTES_NOAUTHFILEDEF', 'AuthUserFile');
        print '</div>';

    } else if (!is_file($filepath)) {
        print '<div class="block">'. $am->E('NOSUCHFILE', $filepath) .'</div>';
    }
    print '</div>';

    if (false != $filepath) {
?>
    <form id="authuserfile_form" name="authuserfile_form" method="post" action="index.php">
    <fieldset>
        <legend>AuthUserFile</legend>
        <div>
            <textarea wrap="off" name="contents" id="authuserfile_contents" 
                <?php echo $am->isDemo() || !$am->isManualEdit() ?  ' readonly' : 'onKeyUp="javascript:filesOnChangeContents(\'authuserfile\');"'; ?>
                class="filecontent"><?php echo $am->getFileContentsByType('authuserfile', true); ?></textarea>
        </div>
        <div class="fieldnotes">File: <?php echo $filepath ?></div>
    </fieldset>
    <div class="buttonrow">
        <div id="authuserfile_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" name="filetype" value="authuserfile" />
        <input type="hidden" name="action" value="savefile" />
        <input type="hidden" name="page" value="files" />
<?php 
    if ($am->isManualEdit()) {
        echo htmlSubmit('authuserfile_submit', 'onClick="javascript:filesUpdateContents(\'authuserfile\'); return false;"');
        echo htmlReset('authuserfile_reset');
        if (is_file($filepath)) {
            echo '| <a href="javascript:filesDownload(\'authuserfile\');">' . $am->M('DOWNLOAD') . '</a>';
        }
    }
?>
    </div>
    </form>
<?php
    }
    // ------------- Auth Group ----------------
    echo '<!--';

    $filepath = $am->getPathByType('authgroupfile');

    print '<div id="authgroupfile_message">';
    if (false == $filepath) {
        print '<div class="warning">'. $am->w('NOTDEFINED', 'AuthGroupFile') .'</div>';

    } else if (!is_file($filepath)) {
        print '<div class="warning">'. $am->E('NOSUCHFILE', $filepath) .'</div>';
    }
    print '</div>';

    if (false != $filepath) {
?>
    <form id="authgroupfile_form" name="authgroupfile_form" method="post" action="index.php">
    <fieldset>
        <legend>AuthGroupFile</legend>
        <div>
            <textarea wrap="off" nowrap name="contents" id="authgroupfile_contents" 
                <?php echo $am->isDemo() || !$am->isManualEdit() ?  ' readonly' : 'onKeyUp="javascript:filesOnChangeContents(\'authgroupfile\');"'; ?>
                class="filecontent"><?php echo $am->getFileContentsByType('authgroupfile', true); ?></textarea>
        </div>
        <div class="fieldnotes">File: <?php echo $filepath ?></div>
    </fieldset>
    <div class="buttonrow">
        <div id="authgroupfile_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" name="filetype" value="authgroupfile" />
        <input type="hidden" name="action" value="savefile" />
        <input type="hidden" name="page" value="files" />
        <input type="submit" id="authgroupfile_submit" 
        <?php
            echo 'value="'.(is_file($filepath) ? $am->M('UPDATEFILE') : $am->M('CREATEFILE')).'"';
            echo $am->isDemo() || !$am->isManualEdit() ?  ' disabled' : '';
        ?> onClick="javascript:filesUpdateContents('authgroupfile'); return false;" />
        <input type="reset"  id="authgroupfile_reset" 
        <?php
            echo 'value="' . $am->M('RESET') . '"'; 
            echo $am->isDemo() || !$am->isManualEdit() ?  ' disabled' : '';
        ?>/> 

        | <a href="javascript:filesDownload('authgroupfile');"><?php echo $am->M('DOWNLOAD') ?></a>

    </div>
    </form>
<?php
    }
    echo '-->';

    echo '<div class="spacer30"></div>';
    echo '</div>'; // righty
    web_footer();
}
/* the end of files module */


/**
 * Access module
 *
 * @return void
 */
function showPage_access()
{
    global $am;

    // check access rights
    if (!$am->isAdmin()) {
    	$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
        showPage_401( true );
        exit;
    }

    $message = $error = null;
    $action = getParam('action', null, 'attribute');

    // Ajax Action: updateaccess
    if ($action == 'updateaccess') {
        $filepath = $am->getPathByType( 'accessfile' );

        $authtype = getParam('authtype', null, 'attribute');
        if ($authtype != 'basic') {
            $error = $am->E('INVALIDREQUEST') . ' [authtype]';
        }

        // RequireRules
        $requireargs = 'valid-user';
        
        // authname
        if (!isset($error)) {
	        $authname = getParam('authname', null, 'htmlcode');
	        if ($authname == '') {
	            $error = $am->E('INVALIDREQUEST') . ' [authname]';
	        }
        }
        
        // authuserfile
        if (!isset($error)) {
	        $authuserfile = getParam('authuserfile', null, 'filepath');
	        if ($authuserfile == '' && hasParam('defauthuserfile')) {
	        	$authuserfile = $am->getDefaultFilePathByType('authuserfile');	
	        }
		    $am->setFilePathByType('authuserfile', $authuserfile);
             
             if (!is_file($authuserfile) && !hasParam('createmissingfiles')) {
                 $error = $am->E('NOSUCHFILE', $authuserfile);
             }
        }
        
        // error document 401
        if (!isset($error)) {
        	$errordocument401 = null;
        	if (hasParam('enableerrordocument401')) {
		        $errordocument401 = getParam('errordocument401', null, 'url');
		        if ($errordocument401=='' && hasParam('deferrordocument401')) {
		        	$errordocument401 = $am->getUrlByType('errordocument401');	
		        }
        	}
			$am->setFilePathByType('errordocument401', $errordocument401);
        }
        
        // ip/domain access rules
        $order = getParam('order', null, 'attribute');
        if ($order != 'deny' && $order != 'allow') {
            $order  = null;
        }
        
        if (!isset($error)) {
	        $am->setRecordByType( 'accessfile', 'require', array('require'=>$requireargs));

	        $authArray = array('authtype', 'authname', 'authuserfile', 'errordocument401', 'order');
            foreach( $authArray as $auth) {
            	if (!isset($$auth) || $$auth=='') {
	                $am->setRecordByType( 'accessfile', $auth, null );
            	} else {
                	$am->setRecordByType( 'accessfile', $auth, array($auth=>$$auth));
            	}
            }

            // ip/domain access rules            
            $allowRecs = getParam('access_allow');
            if (is_array($allowRecs) && count($allowRecs)) {
                $am->setRecordByType( 'accessfile', 'allow', $allowRecs);
            }
            
            $denyRecs = getParam('access_deny');
            if (is_array($denyRecs) && count($denyRecs)) {
                $am->setRecordByType( 'accessfile', 'deny',  $denyRecs);
            }
        }
        
        // creating empty files
        if (!isset($error) && hasParam('createmissingfiles')) {
            $authuserfile = $am->getPathBytype('authuserfile');
            if (!is_file($authuserfile) && !$am->saveFileByType( 'authuserfile' )) {
                $error  = $am->getError();
            }
        }
        
        // saving
        if (!isset($error)) {
            if (!$am->saveFileByType( 'accessfile' )) {
                $error  = $am->getError();
            }
        }
        
        if (isset($error)) {
            print '<div class="warning">' . fmt_message($error) . "</div>\n";
            return;
        }
        
        $message = $am->M('FILEUPDSUCCESS', $filepath);
        print '<div class="message">' . fmt_message($message) . "</div>\n";
        return; // ajax
    }

    web_header( $am->M('SUBTITLE_ACCESS') );
    web_menu();
?>
    <div id="righty">
  
    <!-- Top page information -->
    <div class="pagenotes">
    <h4><?php echo $am->M('SUBTITLE_ACCESS') ?></h4>
    <div><div class="name">AccessFile</div>
    <div class="value"><?php echo $am->getPathByType('accessfile') ?></div></div>
    <div><div class="name">IP Address</div>
    <div class="value"><?php echo $_SERVER['REMOTE_ADDR']; ?></div></div>
    </div>
    
<?php

    // ------------- Message ----------------
    web_message( $message, $error );
    
    $noaccessfile = false;
    
    $accessFilePath = $am->getPathByType('accessfile');
    if (!is_file($accessFilePath)) {
    	echo '<div id="noaccessfile_msg" class="block">';
        echo '<img src="images/icon_warning.gif" width="20" height="20" align="left" />';
    	echo $am->M('NOTES_NOACCESSFILE', $accessFilePath);
		echo '</div>';
		$noaccessfile = true;
    } else {
    	echo '<div id="noaccessfile_msg" class="block" style="display:none;"></div>';
    }
?>

    <form id="edit_form" name="edit_form" onSubmit="javascript:accessOnSubmit('edit_form'); return false;">
    
<!-- ACCESS RULES -->
    <fieldset>
      <legend><?php echo $am->M('LEGEND_ACCESSRULES') ?></legend>

      <div>
        <label for="authtype" class="required">AuthType</label>
        <select id="authtype" name="authtype">
            <option value="basic" class="cyan-item" selected="selected">Basic</option>
        </select>
      </div>

<?php
	$authname =  $am->getAccessRuleByType('accessfile', 'authname');
	if ($noaccessfile || !isset($authname) || $authname=='') {
		$authname = $am->getConfigValue('authname');
	}
?>
      <div>
        <label for="authname" class="required">AuthName</label>
        <input id="authname" type="text" name="authname" size="40" maxlength="255" class="required" 
            value="<?php echo htmlEncode($authname) ?>" />
      </div>

      <div>
        <label for="required" class="required">Require Policy</label>
        <select id="require" name="require" class="required">
            <option value="valid-user" class="green-item"><?php echo $am->M('REQUIRE_ALLVALIDUSERS') ?></option>
        </select>
      </div>

<?php
	$def_authuserfile = $am->getDefaultFilePathByType('authuserfile');	
	$authuserfile = htmlEncode($am->getPathByType('authuserfile'));
	if ($noaccessfile) {
		$authuserfile = $def_authuserfile;
	}
?>
    
      <div>
        <label for="authuserfile" class="required">AuthUserFile</label>
        <input id="authuserfile" type="text" name="authuserfile" size="40" maxlength="255" class="required" 
               <?php if ($def_authuserfile==$authuserfile) { echo " disabled "; } ?>
			   value="<?php echo $authuserfile ?>" />
        <nobr>
        <input type="checkbox" id="defauthuserfile" name="defauthuserfile" value="1"
			   <?php if ($def_authuserfile==$authuserfile) { echo " checked=\"checked\" "; } ?> 
               onClick="javascript:accessOnDefAuthUserFile('edit_form');" /> <?php echo $am->M('USEDEFAULT') ?>
        </nobr>
      </div>
    </fieldset>

<!-- MISSING FILES -->    
    <fieldset>
       <legend><?php echo $am->M('LEGEND_MISSINGFILES') ?></legend>     
        <div>
            <label for="createmissingfiles" class=""><?php echo $am->M('FIELD_MISSINGFILES') ?></label>
            <input type="checkbox" id="createmissingfiles" name="createmissingfiles" value="1"
             <?php if (!hasParam('createmissingfiles')) { echo ' checked="checked"'; } ?> />
             <?php echo $am->M('FLDNOTES_MISSINGFILES') ?>
        </div>
    </fieldset> 

<!-- IP/DOMAIN Access rules -->    
    <fieldset>
       <legend><?php echo $am->M('LEGEND_IPDOMAINRULES') ?></legend>     
        <div>
            <label for="order" class="optional"><?php echo $am->M('FIELD_DEFAULTRULE') ?></label>
            <select name="order" id="order" class="optional">
                <option value="">| Not set... </option>
                <option <?php 
                        if ($am->getAccessRuleByType('accessfile', 'order') == 'allow') {
                            echo ' selected="selected" ';
                        } ?>  
                        style="background: #efe;" value="allow">Allow from all</option>
                <option <?php 
                        if ($am->getAccessRuleByType('accessfile', 'order') == 'deny') {
                            echo ' selected="selected" ';
                        } ?>
                        style="background: #fee;" value="deny">Deny from all</option>
            </select>
            <a id="help-order" href="javascript:void(0);" onClick="$('access-order-description').show(); $('help-order').hide();"><img src="images/help.png" alt="Help" /></a>
        </div>

        <div id="access-order-description" class="block-help" style="display: none;">
            Controls the default access state and the order in which Allow and Deny are evaluated.
            <a target="_blank" class="linkexternal" href="http://httpd.apache.org/docs/2.2/mod/mod_authz_host.html#order">more</a>
        </div>
<div>
<table width="100%">
<tr>
<td><?php echo $am->M('FIELD_ALLOWEDHOSTS') ?></td>
<td><?php echo $am->M('FIELD_DENIEDHOSTS') ?></td>
</tr>
<td width="50%"><select id="access_allow" name="access_allow[]" size="5" multiple class="optional" style='width: 100%; background: #efe;' onChange='accessListChoose("access_allow");'>
                <option value="">-- add new --</option>
            </select>
</td>
<td width="50%"><select id="access_deny" name="access_deny[]" size="5" multiple class="optional" style='width: 100%; background: #fee;' onChange='accessListChoose("access_deny");'>
                <option value="">-- add new --</option>
            </select>
</td>
</tr>

<tr>
<td>
    <input id="access_allow_edit" name="access_allow_edit" type="text" />
    <a href="javascript:void(0);" onClick="javascript:accessListEdit('access_allow'); return false;"><img src="images/approve.gif" alt="Approve" /></a>
    <a href="javascript:void(0);" onClick="javascript:accessListDelete('access_allow'); return false;"><img src="images/decline.gif" alt="Delete" /></a>
</td>
<td>
    <input id="access_deny_edit" name="access_deny_edit" type="text" />
    <a href="javascript:void(0);" onClick="javascript:accessListEdit('access_deny'); return false;"><img src="images/approve.gif" alt="Approve" /></a>
    <a href="javascript:void(0);" onClick="javascript:accessListDelete('access_deny'); return false;"><img src="images/decline.gif" alt="Delete" /></a>
</td>
</tr>

</table>
</div>
    </fieldset> 

<script type="text/javascript" language="javascript">
<?php 
$allowRecs = $am->getAccessRuleByType('accessfile', 'allow');
if (is_array($allowRecs)) {
    foreach ($allowRecs as $k=>$v) {
        echo '$(\'access_allow\').options[ $(\'access_allow\').options.length ] = ';
        echo ' new Option( "' . addslashes($v) . '", "' . addslashes($v) . '", false );';
    }
}
$denyRecs = $am->getAccessRuleByType('accessfile', 'deny');
if (is_array($denyRecs)) {
    foreach ($denyRecs as $k=>$v) {
        echo '$(\'access_deny\').options[ $(\'access_deny\').options.length ] = ';
        echo ' new Option( "' . addslashes($v) . '", "' . addslashes($v) . '", false );';
    }
}
?>
</script>
            

<!-- 401 handler -->    
    <fieldset>
       <legend><?php echo $am->M('LEGEND_HANDLER401') ?></legend>     
<?php
	// ErrorDocument 401 <path>
	$deferrordocument401 = $am->getUrlByType('errordocument401');	
	$errordocument401 = htmlEncode($am->getAccessRuleByType('accessfile', 'errordocument401'));
	if ($noaccessfile) {
		$errordocument401 = $deferrordocument401;
	}
?>
      <div>
        <label for="enableerrordocument401" class=""><?php echo $am->M('FIELD_HANDLER401') ?></label>
        <input type="checkbox" id="enableerrordocument401" name="enableerrordocument401" value="1"
			   <?php if ($errordocument401!='') { echo " checked=\"checked\" "; } ?> 
               onClick="javascript:accessOnEnableErrorDocument401('edit_form');" /> <?php echo $am->M('ENABLE') ?>
      </div>

      <div id="editErrordocument401" style="display:<?php echo ($errordocument401=='' ?'none' : 'block') ?>;">
<?php
	if ($errordocument401 == '') {
		$errordocument401 = $deferrordocument401;
	}
?>
        <label for="errordocument401" class=""><?php echo $am->M('FIELD_LOCATION401') ?></label>
        <input type="text" id="errordocument401" name="errordocument401" size="40" maxlength="255" class="" 
               <?php if ($deferrordocument401==$errordocument401) { echo " disabled "; } ?>
			   value="<?php echo $errordocument401 ?>" />

        <nobr>
        <input type="checkbox" id="deferrordocument401" name="deferrordocument401" value="1"
			   <?php if ($deferrordocument401==$errordocument401) { echo " checked=\"checked\" "; } ?> 
               onClick="javascript:accessOnDefErrorDocument401('edit_form');" /> <?php echo $am->M('USEDEFAULT') ?>
        </nobr>
      </div>
            
    </fieldset>

    <div class="buttonrow">
        <div id="edit_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('SAVING') ?></span></div>
        <input type="hidden" name="page" value="access" />
        <input type="hidden" name="action" value="updateaccess" />
        <?php echo htmlSubmit('submit') ?>
        <?php echo htmlReset('reset') ?>
    </div>
    </form>

    <div class="spacer30"></div>
    
<?php
    echo '</div>'; // righty
    web_footer();
}
/* the end of 'access' module */


/**
 * Output support information
 *
 * @return void
 */
function showPage_support()
{
    global $am;

    // check access rights
    if (!$am->isAuthenticated()) {
    	$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
        showPage_401( true );
        exit;
    }

    $message = $error = null;
    $action = getParam('action', null, 'attribute');
    
    // ------------- Ajax Actions ----------------    

    // Ajax Action: send email to administrator 
    if ($action == 'supportrequest' && !$am->isAdmin()) {
		$userData = $am->getAuthenticatedUser();
		
		$userData['requestsubject'] = getParam('subject', null, 'subject');
		$userData['requestbody'] = getParam('body', null, array('htmlcode','max4k'));
		
		// sending email to adminitrator
		if (!$am->sendMail('memberreq', $userData)) {
			$error = $am->getError();
		}
		
        if (isset($error)) {
            echo '<div class="warning">' . fmt_message($error) . "</div>\n";
        } else {
            $message = $am->M('INFO_EMAILSENT');
            echo '<div class="message">' . fmt_message($message) . "</div>\n";
        }
        exit;
	}
   
    web_header( $am->M('SUBTITLE_SUPPORT') );
    web_menu();
    echo '<div id="righty">'; // righty

    // ------------- Information ----------------
    echo '<div class="pagenotes">';
    echo '<h4>' . $am->M('SUBTITLE_SUPPORT') . '</h4>';

    echo '<div><div class="name">'.$am->M('PROJECTHOMEPAGE').'</div>';
    echo '<div class="value"><a target="_blank" href="http://www.authman.com/">www.authman.com</a></div></div>';

    echo '<div><div class="name">'.$am->M('VERSION').'</div>';
    echo '<div class="value">' . $am->getVersion() . '</div></div>';
    echo '</div>';

    // ------------- Message ----------------
    web_message( $message, $error );

    // ------------- AccessFile ----------------
    $filepath = $am->getPathByType('accessfile');
    print '<div id="accessfile_message">';
    if (!is_file($filepath)) {
        print '<div class="block">';
        print $am->E('NOSUCHFILE', $filepath);
        print '</div>';
    } 
    print '</div>';
?>

<?php if ($am->isAuthenticated() && !$am->isAdmin()) { ?>
<!-- Support FORM: contact request -->
<div id="supportRequest_container" style="display: block;">
    <h1><?php echo $am->M('SUBTITLE_SUPPORTREQUEST') ?></h1>

    <p id="supportRequest_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>
      
    <form id="supportRequest" name="supportRequest" onSubmit="javascript:return supportOnSubmitForm(this);">
    <fieldset>
    <legend><?php echo $am->M('LEGEND_SUPPORTREQUEST') ?></legend>

      <div>
        <label for="subject" class="required"><?php echo $am->M('FIELD_MAILSUBJECT') ?></label>
        <input name="subject" type="text" maxlength="255" class="required" />
      </div>

      <div>
        <label for="body" class="required"><?php echo $am->M('FIELD_MAILBODY') ?></label>
      </div> 
      
      <div><textarea name="body" class="filecontent required" wrap="off"></textarea>
      </div>
	               
    </fieldset>
    <div class="buttonrow">
          <div id="supportRequest_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" />
            <span><?php echo $am->M('SAVING') ?></span>
          </div>
        <input type="hidden" name="page" value="support" />
        <input type="hidden" name="action" value="supportrequest" />
	    <?php echo htmlInput('submit', 'supportRequest_submit', $am->M('SUBMIT')) ?>
	    <?php echo htmlReset('supportRequest_reset', 'onclick="javascript:supportResetForm(\'supportRequest\');"') ?>

    </div>
    </form>
<div class="spacer30"></div>
</div>
<!-- end of contact form -->	
<?php } // authenticated and not admin ?>

<?php if ($am->isAdmin()) { ?>
    <h1><?php echo $am->M('SUBTITLE_SOFTWARESUPPORT') ?></h1>
    <?php echo $am->M('ADMIN_SUPPORT_TEXT') ?>
    <div class="spacer30"></div>
    <div class="block">
        <a href="http://www.authman.com/bugtrack" target="_blank"><?php echo $am->M('ADMIN_SUPPORT_SUBMITBUG') ?></a>
    </div>
    <div class="spacer30"></div>
<?php } ?>
	
</div>
<?php
    web_footer();
}
/* the end of 'support' module */


/**
 * Member Settings
 *
 * @return void
 */
function showPage_edit()
{
    global $am;

    // Checking access rights
    if (!$am->isAuthenticated()) {
    	$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
        showPage_401();
        exit;
    }

    // This module is for members only
    if ($am->isAdmin()) {
    	showPage_users();
    	exit;
    }

    $message = $error = null;
    $action = getParam('action', null, 'attribute');

    // ------------- Ajax Actions ----------------

    // Ajax Action: Update member account 
    if ($action == 'update') {
    	$userData = $am->getAuthenticatedUser();

        if (!isset($error)) {
            $userData['info'] = getParam('realname', null, 'field');
            $userData['email'] = getParam('email', null, 'email');

            if (hasParam('password')) {
                $rawpass = getParam('password', null, 'password');
                if (isset($rawpass) && $rawpass != '') {
                    // we can't set pass_raw because "login as" function will fail
                    $userData['pass'] = $am->htcrypt($rawpass);
                }
            }

            # update / insert
            if (!$am->setRecordByType('authuserfile', $userData['name'],
                                      $userData, true)) {
                $error = $am->E('MEMBERUPDATEFAILED')
                       . ': ' . $am->getError();
            }
        }

        if (!isset($error) && hasParam('password')) {
    		$am->loginAs($userData['name'], $userData['pass']);
        }

        if (isset($error)) {
            echo '<div class="warning">' . fmt_message($error) . "</div>\n";
            exit;
        }

        $message = $am->M('MEMBERUPDSUCCESS');
        echo '<div class="message">' . fmt_message($message) . "</div>\n";
        exit;

    }

    web_header( $am->M('SUBTITLE_MEMBEREDIT') );
    web_menu();

    $userData = $am->getAuthenticatedUser();

?>
<div id="righty">

<h1><?php echo $am->M('SUBTITLE_MEMBEREDIT') ?></h1>


<!-- Page Message -->
<?php web_message( $message, $error ); ?>

<!-- Member Settings FORM: EDIT Account -->
<div id="memberEdit_container" style="display: display;">
  <form id="memberEdit" name="memberEdit" method="post" onSubmit="javascript:return memberOnSubmitForm(this);">
  <p id="memberEdit_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>

  <fieldset>
    <legend><?php echo $am->M('LEGEND_MEMBEREDIT') ?></legend>
    <div>
      <label for="username" class=""><?php echo $am->M('FIELD_USERNAME') ?></label>
      <?php echo htmlspecialchars($userData['name']) ?>
    </div>
    <div>
      <label for="password"><?php echo $am->M('FIELD_NEWPASSWORD') ?></label>
      <input id="memberEdit_password" type="text" name="password" maxlength="255" />
      <input type="button" value="<?php echo $am->M('GENERATE') ?>" onclick="javascript:mainGeneratePassword2('memberEdit');" />
    </div>
    <div>
      <label for="realname" class="required"><?php echo $am->M('FIELD_REALNAME') ?></label>
      <input id="memberEdit_realname" type="text" name="realname" size="40" maxlength="255"  class="required" value="<?php echo htmlspecialchars($userData['info']) ?>" />
    </div>
    <div>
      <label for="email" class="required"><?php echo $am->M('FIELD_EMAIL') ?></label>
      <input id="memberEdit_email" type="text" name="email" size="40" maxlength="255" class="required" value="<?php echo htmlspecialchars($userData['email']) ?>" />
    </div>
  </fieldset>

  <div class="buttonrow">
      <div id="memberEdit_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" />
        <span><?php echo $am->M('SAVING') ?></span>
      </div>
      <input type="hidden" name="page" value="edit" />
      <input type="hidden" name="action" value="update" />
      <?php echo htmlSubmit('submit') ?>
      <?php echo htmlReset('reset') ?>
  </div>

  </form>
</div>
<div class="spacer30"></div>
<!-- end of member edit form -->

<!-- end of righty -->
</div>

<?php
    web_footer();
}
/* the end of 'member' module */

/**
 * Delete Member Account
 *
 * @return void
 */
function showPage_remove()
{
    global $am;

    // Checking access rights
    if (!$am->isAuthenticated()) {
    	$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
        showPage_401();
        exit;
    }

    // This module is for members only
    if ($am->isAdmin()) {
    	showPage_users();
    	exit;
    }

    $message = $error = null;
    $action = getParam('action', null, 'attribute');

    // Member action: delete account
    if ($action == 'remove') {
    	$userData = $am->getAuthenticatedUser();

		if (!isset($error)) {
			// removing
			if (!$am->setRecordByType('authuserfile', $userData['name'],
			                          null, true )) {
				$error = $am->E('USERDELETEFAILED', $userData['name'])
				       . ': ' . $am->getError();
			}

			// sending email to adminitrator
			if (!$am->sendMail('memberdel', $userData)) {
				$am->clearError();
			}
		}

		if (!isset($error)) {
			showPage_logout();
			exit;
		}
    }

    web_header( $am->M('SUBTITLE_MEMBERDELETE') );
    web_menu();


?>
<div id="righty">

<h1><?php echo $am->M('SUBTITLE_MEMBERDELETE') ?></h1>

<!-- Page Message -->
<?php web_message( $message, $error ); ?>

<!-- Member settings FORM: delete -->
<div id="memberDelete_container" style="display: display;">

<form id="memberDelete" name="memberDelete" method="post">
<fieldset>
<legend><?php echo $am->M('LEGEND_MEMBERDELETE') ?></legend>
   <p id="memberDelete_msg" class="formmessage"><?php echo $am->M('Q_MEMBERDELETE'); ?></p>
</fieldset>
<div class="buttonrow">
    <input type="hidden" name="page" value="remove" />
    <input type="hidden" name="action" value="remove" />
    <input type="submit" id="submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
    <input type="submit" id="cancel" name="cancel" value="<?php echo $am->M('CANCEL') ?>"
       onClick="javascript:document.location='index.php?page=home'; return false;" />
</div>
</form>

</div>
<div class="spacer30"></div>
<!-- end of member delete form -->

<!-- end of righty -->
</div>

<?php
    web_footer();
}
/* the end of 'remove' module */

/**
 * Member Password Recovering (step 1)
 *
 * @return void
 */
function showPage_forgot()
{
    global $am;

    $message = $error = null;
    $action = getParam('action', null, 'attribute');

    // Checking access rights  
    if ($am->isAuthenticated()) {
    	header("Location: index.php?page=home");
        exit;
    }

    // This module is for members only      
    if (!$am->hasFeature('phpmailer')) {
    	$error = $am->M('PASSRECOVERDISABLED');
    }

    // Member action: recover password
    if ($action == 'recover' && !isset($error)) {
    	$username = getParam('username', null, 'username');

		$userData = $am->fetchRecordByType('authuserfile', $username, false);
		if (false == $userData) {
    		$error = $am->E('INCORRECTARGS');
		}

		$email = getParam('email', null, 'email');
    	if (!isset($error)) {
    		if ($email == '') {
		    	$error = $am->E('INCORRECTARGS');
    		} else if (strcasecmp($userData['email'], $email) != 0) {
				$error = $am->E('INCORRECTARGS');
    		}
    	}

    	// sending email request to the user
    	if (!isset($error)) {
    		// $userData['hashofname'] = md5( $userData['name'] );
    		$userData['hashofpass'] = md5( $userData['pass'] );
			if (!$am->sendMail('userfgt', $userData)) {
				$error = $am->getError();
			}
    	}

    	if (!isset($error)) {
	    	$message = $am->M('INFO_EMAILSENT');
    	}
    }

    web_header( $am->M('SUBTITLE_FORGOT') );
    web_menu();

?>
<div id="righty">

<h1><?php echo $am->M('SUBTITLE_FORGOT') ?></h1>

<!-- Page Message -->
<?php web_message( $message, $error ); ?>

<!-- Anonymous FORM: recover password -->
<div id="forgot_container" style="display: <?php echo ($action=='' && !isset($error) && isset($message)) ? 'none' : 'block' ?>;">

<p id="forgot_msg" class="formmessage"><?php echo $am->M('NOTES_FORGOT'); ?></p>

<form id="forgot" name="forgot" method="post" nSubmit="javascript:return memberOnSubmitForm(this);">
<fieldset>
<legend><?php echo $am->M('LEGEND_FORGOT') ?></legend>

    <div>
      <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
      <input type="text" name="username"  maxlength="255"  class="required" value="<?php echo getParam('username',null,'htmlspecialchars') ?>" />
    </div>

    <div>
      <label for="email" class="required"><?php echo $am->M('FIELD_EMAIL') ?></label>
      <input type="text" name="email" maxlength="255" class="required" value="<?php echo getParam('email',null,'htmlspecialchars') ?>" />
    </div>


</fieldset>
<div class="buttonrow">
    <input type="hidden" name="page" value="forgot" />
    <input type="hidden" name="action" value="recover" />
    <input type="submit" id="submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
    <input type="submit" id="cancel" name="cancel" value="<?php echo $am->M('CANCEL') ?>"
       onClick="javascript:document.location='index.php?page=home'; return false;" />
</div>
</form>

</div>
<!-- end of recover form -->

<div class="spacer30"></div>

<!-- end of righty -->
</div>

<?php
    web_footer();
}
/* the end of 'forgot' module */

/**
 * Member Password Recovernig (step 2)
 *
 * @return void
 */
function showPage_recover()
{
    global $am;

    $message = $error = null;
    $action = getParam('action', null, 'attribute');

    if ($am->isAuthenticated()) {
    	header("Location: index.php?page=home");
        exit;
    }

    if (!$am->hasFeature('phpmailer')) {
    	$error = $am->M('PASSRECOVERDISABLED');
    }

    // Member action: recover password
    if (hasParam('username') && hasParam('key')) {
    	$username = getParam('username', null, 'username');
		$userData = $am->fetchRecordByType('authuserfile', $username, false);
		if (false == $userData) {
    		$error = $am->E('INCORRECTARGS');
		}

    	$key = getParam('key');
    	if (!isset($key) || $key == '') {
	    	$error = $am->E('INCORRECTARGS');
    	}

    	if ($key != md5($userData['pass'])) {
    		$error = $am->E('INCORRECTARGS');
    	}

    	// password generator
		$totalChar = 6; // number of chars in the password
		$salt = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789";  // salt to select chars from
		$password=""; // set the inital variable
		for ($i=0;$i<$totalChar;$i++) {
            $password = $password . substr ($salt, rand() % strlen($salt), 1);
		}
    	$pass_enc = $am->htcrypt( $password );

    	$userData['pass'] = $pass_enc;
		$userData['pass_raw'] = $password;

		if (!isset($error)) {
    		// updating user
			if (!$am->setRecordByType('authuserfile', $username,
			                          $userData, true )) {
				$error = $am->E('USERUPDATEFAILED', $username)
				       . ': ' . $am->getError();
			}
    	}

    	if (!isset($error)) {
	    	// sending email to the user
			if (!$am->sendMail('userrcv', $userData)) {
				$error = $am->getError();
			}
    	}

    	if (!isset($error)) {
			// logging in as $username
			$am->loginAs($username, $pass_enc);

    		// redirecting
        	unset($_SESSION['durl']);
    		redirect_header('index.php?page=home', 2, $am->M('MSG_PASSWORDRECOVERED'));

            exit;
    	}
    }

    web_header( $am->M('SUBTITLE_RECOVER') );
    web_menu();

?>
<div id="righty">

<h1><?php echo $am->M('SUBTITLE_RECOVER') ?></h1>

<!-- Page Message -->
<?php web_message( $message, $error ); ?>

<!-- Anonymous FORM: recover password -->
<div id="recover_container" style="display: block;">

<p id="recover_msg" class="formmessage"><?php echo $am->M('NOTES_RECOVER'); ?></p>

<form id="recover" name="recover" method="post" onSubmit="javascript:return memberOnSubmitForm(this);">
<fieldset>
<legend><?php echo $am->M('LEGEND_RECOVER') ?></legend>

    <div>
      <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
      <input type="text" name="username"  maxlength="255"  class="required" value="<?php echo getParam('username',null,'htmlspecialchars') ?>" />
    </div>

    <div>
      <label for="key" class="required">Key</label>
      <input type="text" name="key" maxlength="255" class="required" value="<?php echo getParam('key',null,'htmlspecialchars') ?>" />
    </div>


</fieldset>
<div class="buttonrow">
    <input type="hidden" name="page" value="recover" />
    <input type="hidden" name="action" value="recover" />
    <input type="submit" name="submit" value="<?php echo $am->M('CONFIRM') ?>" />
    <input type="submit" name="cancel" value="<?php echo $am->M('CANCEL') ?>"
       onClick="javascript:document.location='index.php?page=home'; return false;" />
</div>
</form>

</div>
<!-- end of recover form -->

<div class="spacer30"></div>

<!-- end of righty -->
</div>

<?php
    web_footer();
}
/* the end of 'recover' module */

/**
 * Show login form
 *
 * @return void
 */
function showPage_adminpassword()
{
    global $am;
    $error = $message = null;
    $action = getParam('action', null, 'attribute');

    if (!$am->isAdmin()) {
    	$_SESSION['durl'] = $_SERVER['REQUEST_URI'];
        showPage_401( true );
        exit;
    }

	list($admin) = $am->getRecordsByType('authadminfile', null, 1, 0);

    if ($action == 'skip') {
        if (isset($_SESSION['durl']) && $_SESSION['durl'] != '') {
	    	$durl = $_SESSION['durl'];
	    	unset($_SESSION['durl']);
	    	redirect_header( $durl, 1, $am->M('LOGGINGIN') );
	    }

        showPage_home();
        exit;
    }

    if ($action == 'change') {
        $npassword = getParam('npassword', null, 'password');
        $npassword2 = getParam('npassword2', null, 'password');

        if (!isset($error)) {
        	if (!isset($npassword) || $npassword=='') {
            	$error = $am->E('INVUSERORPASS');
            } else if (strlen($npassword) < 4) {
                $error = $am->E('PASSWORDTOOSHORT', 4);
        	} else if (strcmp($npassword, $npassword2)!=0) {
            	$error = $am->W('PASSWORDSMISSMATCHED');
        	}
        }

        if (!isset($error)) {
			$admin['pass_raw'] = $npassword;

			# update / insert
            if (!$am->setRecordByType('authadminfile', null, $admin, true, true)) {
                $error = $am->E('ADMINUPDATEFAILED', $admin['name'])
                       . ': ' . $am->getError();
            }
        }

        if (!isset($error)) {
            $pass_enc = $am->htcrypt( $npassword );

            $am->loginAs($admin['name'], $pass_enc);

            if (isset($_SESSION['durl']) && $_SESSION['durl'] != '') {
            	$durl = $_SESSION['durl'];
            	unset($_SESSION['durl']);
            	redirect_header( $durl, 1, $am->M('ADMINUPDSUCCESS', $data['name']));
            }

            redirect_header( 'index.php?page=home', 1, $am->M('ADMINUPDSUCCESS', $data['name']));
        }
    }

    web_header( $am->M('ADMINCHANGEPASSWORD') );
    // web_menu();

    echo '<div id="righty">'; // righty

    echo '<h3>' . $am->M('ADMINCHANGEPASSWORD') . '</h3>';

    // ------------- Message ----------------
    web_message( $message, $error );

?>

 	<div class="block">
 	  <img src="images/icon_warning.gif" width="20" height="20" align="left" />
 	  <?php echo $am->M('NOTES_ADMINHASDEFAULTPASSWORD') ?>
 	</div>

    <form id="adminpassword" name="adminpassword" action="index.php" method="post" onSubmit="javascript:return homeOnSubmitForm(this);">
    <p id="adminpassword_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>
    <fieldset>
        <legend><?php echo $am->M('ADMINCHANGEPASSWORD') ?></legend>

        <div>
            <label for="npassword" class="required"><?php echo $am->M('FIELD_PASSWORD') ?></label>
            <input id="npassword" type="password" name="npassword" maxlength="255" class="required"
                   value="<?php echo getParam('npassword') ?>" />
        </div>
        <div>
            <label for="npassword2" class="required"><?php echo $am->M('FIELD_PASSWORD2') ?></label>
            <input id="npassword2" type="password" name="npassword2" maxlength="255" class="required"
                   value="<?php echo getParam('npassword2') ?>" />
        </div>
    </fieldset>

    <div class="buttonrow">
        <input type="hidden" name="action" value="change" />
        <input type="hidden" name="page" value="adminpassword" />
        <?php echo htmlInput('submit', 'submit', $am->M('BTN_CONFIRM')) ?>
        <?php echo htmlInput('reset', 'reset', $am->M('BTN_RESET')) ?>
    </div>
    </form>
    <div class="spacer30"></div>

    <div><a href="index.php?page=adminpassword&action=skip"><?php echo $am->M('CHANGEPASSWORDLATER') ?></a></div>

    <div class="spacer30"></div>
<?php

    echo '</div>'; // righty
    web_footer();
}

/* the end of 'adminpassword' module */

if (!function_exists("htmlspecialchars_decode")) {
    function htmlspecialchars_decode($string, $quote_style = ENT_COMPAT) {
        return strtr($string, array_flip(get_html_translation_table(HTML_SPECIALCHARS, $quote_style)));
    }
}

/**
 * Ajax module
 *
 * @return void
 */
function showPage_ajax()
{
	global $am;
	
    $action = getParam('action', null, 'attribute');
    if ($action == 'fix') {
        echo 'TODO';
        return;
    }
    
    if ($action == 'checkupdate') {
    	$url = "http://www.authman.com/vc/free";
    	$fh = @fopen($url, "r");
    	if (false == $fh) {
    		echo 'Request failed: ' . $url;
    		return;
    	}
    	
    	$version = fread($fh, 256);
    	if (false == $version) {
    		echo 'Communication failed';
    		return;
    	}

    	$iversion = version2int( $version );
    	if (false == $iversion) {
    		echo $version;
    		return;
    	}
    	
    	if ($iversion > version2int($am->getVersion())) {
    		$am->setRuntimeValue('updateavailable', $version);
 	    	echo '<div>' . $am->M('MSG_UPDATE_AVAILABLE', $version, 'http://www.authman.com/download/free') . '</div>';
    	} else {
    		$am->setRuntimeValue('updateavailable', null);
	    	echo '<div>' . $am->M('MSG_UPDATE_NONEED') . '</div>';
    	}
    	$am->setRuntimeValue('updatechecked_ts', time(), true);
    	
    	return;
    }
    
    if ($action=='setshowmembernews') {
        $val = (bool)getParam('value', false, 'intval');
        $am->setRuntimeValue('showmembernews', $val, true);
        echo 'DONE';
        return;
    }

    if ($action=='getnews') {
        echo $am->newsFetchAsHTML();        
        return;
    }
}

function version2int( $version ) 
{
	$version = trim($version);

	$matches = array();
	
	if (!preg_match('/^(\d+)\.(\d+)\.(\d+)$/', $version, $matches)) {
		return false;
	}
	
	return intval( sprintf('%d%03d%03d', 
						isset($matches[1]) ? $matches[1] : 0, 
						isset($matches[2]) ? $matches[2] : 0,
						isset($matches[3]) ? $matches[3] : 0)); 
}
	     
################################################################################
### HOME
################################################################################
function showPage_home()
{
    global $am;
    
    $message = $error = null;

    if ($am->isAuthenticated()) {
        web_header( $am->M('SUBTITLE_HOME') );
    } else {
        web_header();
    }

   
    web_menu();
    echo '<div id="righty">';
    
	// ----- Anonymous -------------------------------------
    if (!$am->isAuthenticated()) {
        echo '<h1>' . $am->M('WELCOME') . '</h1>';
        web_message( $message, $error );
        echo '<div>' . $am->M('WELCOME_DEFAULT') . '</div>';
        if ($am->isDemo()) {
            echo '<br />';
            echo '<div><span id="demonote">' . $am->M('WELCOME_DEFAULT_DEMO') . '</span></div>';
        }
        echo '<div class="spacer30"></div>';
        echo '<div class="block">';
        echo '<ul>';
        echo '<li><a href="index.php?page=login">' . $am->M('CLICKHERE') .'</a> ' . $am->M('TOLOGIN') . '</li>';
        echo '<li><a href="../">' . $am->M('CLICKHERE') . '</a> ' . $am->M('TOPROTECTED') . '</li>';
        echo '</ul>';
        echo '</div>';

	// ----- Member -------------------------------------
	} else if (!$am->isAdmin()) {
        echo '<div class="pagenotes">';
        echo '<h4>' . $am->M('SUBTITLE_HOME') . '</h4>';		
        echo '<div><div class="name">' . $am->M('PROTECTEDDIRECTORY') . '</div>';
        echo '<div class="value">' . $am->getUrlByType('protected', true) . '</div></div>';
        echo '</div>';

        echo '<h1>' . $am->M('WELCOME') . '</h1>';
        web_message( $message, $error );
        echo '<div>' . $am->M('WELCOME_MEMBER') . '</div>';
        echo '<div class="spacer30"></div>';

    // ----- Administrator -------------------------------------
	} else {
		
		// ------------- Information ----------------
        echo '<div class="pagenotes">';
        echo '<h4>' . $am->M('SUBTITLE_HOME') . '</h4>';

        $rtErrors = $am->getRuntimeErrors();
        if (count($rtErrors)) {

            $rtCriticals = $am->getRuntimeErrors( true );

            echo '<div><div class="name">'. $am->M('RUNTIMEWARNINGS') .'</div>';
            echo '<div class="value">';

            print 'Found ';
            if (count($rtCriticals)) {
                print count($rtCriticals) . ' critical errors';
                print ', ';
            }
            print (count($rtErrors)-count($rtCriticals)) . ' warnings';

            echo '</div></div>';
        }

        echo '<div><div class="name">' . $am->M('VERSION') . '</div>';
        echo '<div class="value">' . $am->getVersion();
        
        $updateavailable = $am->getRuntimeValue('updateavailable');
        if (isset($updateavailable) && $updateavailable != '') {
        	echo ' [' . $am->M('UPDATEAVAILABLE', $updateavailable) . ']';
        }        
        echo '</div></div>';
        
        echo '<div><div class="name">' . $am->M('TOTALMEMBERS') . '</div>';
        echo '<div class="value"><a href="index.php?page=users">';
        echo $am->getTotalByType('authuserfile');
        echo '</a>';

        $newcount = $am->getTotalByType('signupfile');
        if ($newcount > 0 || $am->getConfigValue('allowsignup')) {
	        echo ' ' . $am->M('AND') . ' ';
            if ($newcount) {
                echo '<a href="index.php?page=signups">' . $newcount . '</a>';
            } else {
                echo '0';
            }
            echo ' ' . $am->M('AWAITINGCONFIRM') . ' ';
        }
        echo '</div></div>';
        
		echo '<div><div class="name">' . $am->M('LASTLOGGEDIN') . '</div>';
		if ($am->hasRuntimeValue('lastloggedin_ts')) {
			echo '<div class="value">';
			echo gmdate('D, d M Y H:i:s', $am->getRuntimeValue('lastloggedin_ts')) . ' GMT';
			if ($am->hasRuntimeValue('lastloggedin_ip')) {
				echo ' ' . $am->M('FROM') . ' ' .  $am->getRuntimeValue('lastloggedin_ip');
			}
			echo '</div></div>';
		} else {
			echo '<div class="value">' . $am->M('NOAVAILABLE') . '</div></div>';
		}

		
        echo '</div>';
	    
        echo '<h1>' . $am->M('WELCOME') . '</h1>';
        echo '<div>' . $am->M('WELCOME_ADMIN') . '</div>';
        echo '<div class="spacer30"></div>';
	    web_message( $message, $error );

        
        // rss, news and updates
        if ($am->hasFeature('magpierss')) {
            ?>
            <div id="home-news">
            <div id="home-news-caption">Authman.com News</div>
            <div id="home-news-content" style="display:none;">
               <?php echo $am->M('LOADING') ?>
            </div>

            <div id="home-news-action">
            <div id="news_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('LOADING') ?></span></div>
            <div id="home-news-hide" style="display:none;"><a href="javascript:void(0)" onClick="javascript:homeShowNews(false,true);return false;"><?php echo $am->M('BTN_HIDENEWS') ?></a></div>
            <div id="home-news-show" style="display:none;"><a href="javascript:void(0)" onClick="javascript:homeShowNews(true,true);return false;"><?php echo $am->M('BTN_SHOWNEWS') ?></a></div>
            </div>
            
            </div>
            <div class="spacer30"></div>
            <?php
        }
   
        // signup requests
        $newcount = $am->getTotalByType('signupfile');
        if ($newcount > 0) {
            echo '<div class="block">';
            echo $am->M('MSG_AWAITINGCONFIRM', $newcount);
            echo '</div>';
            
            echo '<div class="spacer30"></div>';
        }
        
	    // software update 
?>
		<form>
		<fieldset>
		<div>
			<?php echo $am->M('NOTES_SOFTUPDATES') ?>
		</div>
<?php 
		$showUpdate = $am->hasRuntimeValue('updateavailable')
                    && $am->getRuntimeValue('updateavailable') != '';
		
		echo '<div id="update_msg" class="block" style="display:';
		echo $showUpdate ? 'block' : 'none';
		echo ';">';

		if ($showUpdate) {
	    	echo '<div>';
		    echo $am->M('UPDATEAVAILABLE', 
		                 $am->getRuntimeValue('updateavailable'));
	    	echo '</div>';
	    	
		    if ($am->hasRuntimeValue('updatechecked_ts')) {
	    		echo '<div>';
	    		echo $am->M('UPDATECHECKED', date('D dS \of M Y', 
		    		        $am->getRuntimeValue('lastloggedin_ts')));
		    	echo '</div>';
			}
	    }
		echo '</div>';
?>
		</fieldset>
			
	    <div class="buttonrow">
			<div id="update_loading" class="saving" style="display:none"><img src="images/loadinfo.gif" width="16" height="16" /> <span><?php echo $am->M('CHECKING') ?></span></div>
			<input type="submit" value="Check Update" onClick="javascript:homeCheckUpdate(); return false;"/>
		</div>
		</form>
        <div class="spacer30"></div>
<?php
	    // the end of software update

        $rtCriticals = $am->getRuntimeErrors( true );
        foreach ($rtCriticals as $rtErr) {
            print '<div class="warning">';
            print  fmt_message($rtErr['message']);
            print  ' [#'.$rtErr['errno'].'] ';
            if (isset($rtErr['recover']) && $rtErr['recover']) {
                $url = 'javascript:fixAction('.$rtErr['errno'].');';
                print '<div id="fixaction_'.$rtErr['errno'].'">';
                print ' <div class="fixit"><a href="' . $url . '">'.$am->M('FIXIT').'</a></div>';
                print '</div>';
            }
            print '</div>';
        }
    } // the end of admin section
    
    echo '<div class="spacer30"></div>';
    echo '</div>'; // righty
    web_footer();

    // rss, news and updates
    if ($am->hasFeature('magpierss')) {
        // show news if runtime variable shownews isn't set or is equal 1
        $showNews = !$am->hasRuntimeValue('showmembernews') || (bool)$am->getRuntimeValue('showmembernews');
        print '<script type="text/javascript">';
        print 'homeShowNews(' . ($showNews ? 'true' : 'false') . ', false);';
        print '</script>';
    }    
}
                                
################################################################################
### Other pages
################################################################################

/**
 * Show login form
 *
 * @return void
 */
function showPage_login()
{
    global $am;
    $error = $message = null;
    $action = getParam('action', null, 'attribute');

    $username = isset($_SESSION['am_u']) ? base64_decode($_SESSION['am_u']) : '';

    if ($action == 'login' && !$am->isAuthenticated()) {
        $username = getParam('username', null, 'username');
        $password = getParam('password', null, 'password');

        if (!isset($password) || $password=='') {
            $error = $am->E('INVUSERORPASS');
        }

        if (!isset($error)) {
            if ($am->isDemo() && $username=='admin' && $password=='admin') {
                list($user) = $am->getRecordsByType('authadminfile', null, 1, 0);  

            } else {
                $user = $am->fetchRecordByType('authuserfile', $username);
                if (false == $user) {
                    $user = $am->fetchRecordByType('authadminfile', $username, true);
                }
            }
            
            if (false == $user) {
                $error = $am->E('INVUSERORPASS');
            }
        }

        // password checking
        if (!isset($error)) {       
            if ($am->isDemo() && $username=='admin' && $password=='admin') {
                // it's ok to login as admin / admin
            } else {
                $pass_enc = $am->htcrypt( $password, $user['pass'] );
                if ($user['pass'] != $pass_enc) {
                    $error = $am->E('INVUSERORPASS');
                }
            }
        }

        if (!isset($error)) {
            // logging in
            $am->loginAs( $user['name'], $user['pass'] );
            
            if (!($am->isDemo() && $username=='admin' && $password=='admin')) {
                // check for default password
        	    if ($am->isAdmin() && strcmp($password, 'admin')==0) {
        		    showPage_adminpassword();
        		    exit;
        	    }
            } 
            		
            if (isset($_SESSION['durl']) && $_SESSION['durl'] != '') {
            	$durl = $_SESSION['durl'];
            	unset($_SESSION['durl']);
            	redirect_header( $durl, 1, $am->M('LOGGINGIN') );
            }

            showPage_home();
            exit;
        }
    }

    web_header( $am->M('LOGGINGIN') );
    web_menu();

    echo '<div id="righty">'; // righty

    echo '<h3>' . $am->M('LOGGINGIN') . '</h3>';

    // ------------- Message ----------------
    web_message( $message, $error );

    if ($am->isAuthenticated()) {
    	$user = $am->getAuthenticatedUser();
?>
	<div class="block">
		<?php echo $am->M('LOGGEDASALREADY', $user['name']) ?>
	</div>
	<div class="spacer30"></div>
		
<?php
    } else {

        if ($am->isDemo()) {
            echo '<br />';
            echo '<div><span id="demonote">' . $am->M('WELCOME_DEFAULT_DEMO') . '</span></div>';
        }
?>

    <form id="login_form" name="login_form" action="index.php" method="post">
    <p id="login_form_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>
    <fieldset>
        <legend><?php echo $am->M('LOGGINGIN') ?></legend>

        <?php 
        $password = '';
        if ($am->isDemo() && hasParam('demoadmin') && !hasParam('username') && !hasParam('password')) {
                $username = $password = 'admin';
        }
        ?>
        
        <div>
            <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
            <input id="username" type="text" name="username" maxlength="255" class="required" value="<?php echo $username ?>" />
        </div>

        <div>
            <label for="password" class="required"><?php echo $am->M('FIELD_PASSWORD') ?></label>
            <input id="password" type="password" name="password" maxlength="255" class="required" value="<?php echo $password ?>" />
        </div>
    </fieldset>

    <div class="buttonrow">
        <input type="hidden" name="action" value="login" />
        <input type="hidden" name="page" value="login" />
        <?php echo htmlInput('submit', 'submit', $am->M('SUBMIT')) ?>
    </div>
    </form>
    <div class="spacer30"></div>
    
    <div class="block">
    <ul>
<?php if ($am->getConfigValue('allowsignup')) { ?>
        <li><a href="index.php?page=signup"><?php echo $am->M('SIGNUPLINK') ?></a></li>
<?php } ?>
        <li><a href="index.php?page=forgot"><?php echo $am->M('FORGOTLINK') ?></a></li>
    </ul>
    </div>
    <div class="spacer30"></div>
<?php
    } // not isAuthenticated
    
    echo '</div>'; // righty
    web_footer();
}


/**
 * Show signup form
 *
 * @return void
 */
function showPage_signup()
{
    global $am;
    $error = $message = null;
    $action = getParam('action', null, 'attribute');

    if (!$am->getConfigValue('allowsignup')) {
    	$error = $am->W('SIGNUPDISABLED');
    }
    
    if ($action == 'signup' && !isset($error) && !$am->isAuthenticated()) {
        $username = getParam('username', null, 'username');
		if (!isset($username) || $username == '') {
			$error = $am->E('INVALIDREQUEST') . ' [username]';
            
		} else if ($am->isRecordByType('authuserfile', $username)) {
			$error = $am->E('USEREXISTS', $username);
            
		} else if ($am->isRecordByType('signupfile', $username)) {
			$error = $am->E('USEREXISTS', $username);
		}

		$password = getParam('password', null, 'password');
		if (!isset($error) && $password != '') {
			if (strlen($password) < 4) {
				$error = $am->E('PASSWORDTOOSHORT', 4);
			}
		}
        
        // auto approve signup
        if (!isset($error) && $am->getConfigValue('autoapprove')) {
            $data = array('name'=>$username,
                          'pass_raw'=>$password,
                          'info'=>getParam('realname', null, 'field'),
                          'email'=>getParam('email', null, 'email'));            
        
            if (!$am->setRecordByType( 'authuserfile', null,  $data, true )) {
                $error = $am->E('USERINSERTFAILED', $username) . ': ' . $am->getError();
            }
            
            // notify administrator
            if (!isset($error) && !$am->sendMail('memberaa', $data )) {
                $error = $am->getError();
            }
            // notify user
            if (!isset($error) && !$am->sendMail('useradd', $data)) {
                $error = $am->getError();
            }        
            
            if (!isset($error)) {
                redirect_header($am->getUrlByType('protected', true), 6, $am->M('NOTES_SIGNUPAUTOAPPROVE'));
            }            
        }

        // pre-moderated signup
		if (!isset($error) && !$am->getConfigValue('autoapprove')) {
			$referer = isset($_SERVER['HTTP_REFERER']) ? 
			           $_SERVER['HTTP_REFERER'] : '';
			
	        $data = array('name'=>$username,
				 	      'pass'=>$password,
	                      'info'=>getParam('realname', null, 'field'),
  						  'email'=>getParam('email', null, 'email'),
	                      'ts'=>time(),
	                      'referer'=>$referer,
	                      'remoteaddr'=>$_SERVER['REMOTE_ADDR']);

			if (!$am->setRecordByType('signupfile', null,  $data, true)) {
				$error = $am->E('SIGNUPINSERTFAILED', $username)
			 	       . ': ' . $am->getError();
			}
		}
		
		if (!isset($error)) {
			redirect_header( 'index.php?op=home', 10, $am->M('NOTES_SIGNUPWAITRESPONSE'));
		}
    }

    web_header( $am->M('SUBTITLE_SIGNUP') );
    web_menu( true );

    echo '<div id="righty">'; // righty

    echo '<h3>' . $am->M('SUBTITLE_SIGNUP') . '</h3>';

    // ------------- Message ----------------
    web_message( $message, $error );

    if ($am->isAuthenticated()) {
    	$user = $am->getAuthenticatedUser();
?>
	<div class="block">
		<?php echo $am->M('LOGGEDASALREADY', $user['name']) ?>
	</div>
	<div class="spacer30"></div>
		
<?php
    } else {    
?>
	<div id="signup_container">
    <form id="signup" name="signup" action="index.php?page=signup" method="post" 
          onSubmit="javascript:return homeOnSubmitForm(this);">
    <p id="signup_msg" class="formmessage"><?php echo $am->M('FILLFIELDS'); ?></p>
    <fieldset>
        <legend><?php echo $am->M('LEGEND_SIGNUP') ?></legend>

        <div>
            <label for="username" class="required"><?php echo $am->M('FIELD_USERNAME') ?></label>
            <input id="signup_username" type="text" name="username" maxlength="255" class="required" 
				   value="<?php echo getParam('username', null, 'htmlspecialchars') ?>"
        </div>

		<div>
            <label for="password" class=""><?php echo $am->M('FIELD_PASSWORD') ?></label>
            <input id="signup_password" type="text" name="password" maxlength="255" class="" 
                   value="<?php echo getParam('password', null, 'htmlspecialchars') ?>" />
        </div>
                
        <div>
            <label for="realname" class="required"><?php echo $am->M('FIELD_REALNAME') ?></label>
            <input id="signup_realname" type="text" name="realname" maxlength="255" class="required" 
                   value="<?php echo getParam('realname', null, 'htmlspecialchars') ?>" />
        </div>
                
        <div>
            <label for="email" class="required"><?php echo $am->M('FIELD_EMAIL') ?></label>
            <input id="signup_email" type="text" name="email" maxlength="255" class="required"
                   value="<?php echo getParam('email', null, 'htmlspecialchars') ?>"
        </div>
    </fieldset>

    <div class="buttonrow">
        <input type="hidden" name="action" value="signup" />
        <input type="hidden" name="page" value="signup" />
        <?php echo htmlInput('submit', 'submit', $am->M('SUBMIT')) ?>
    </div>
    </form>
	</div>

    <div class="spacer30"></div>
<?php
    } // is not authenticated
    
    echo '</div>'; // righty
    web_footer();
}

/**
 * Logging out
 *
 * @return void
 */
function showPage_logout()
{
    if (isset($_SESSION['am_c'])) {
        unset($_SESSION['am_c']);
    }

    global $am;
    
    if ($am->isAuthenticatedByBasicAuth()) {
        redirect_header('index.php?page=home', 4, $am->M('MSG_NOLOGOUTBASICAUTH'));
    }
    
    redirect_header('index.php?page=home', 1, $am->M('LOGGEDOUT'));
    exit;
}

/**
 * Show 404 error
 *
 * @return void
 */
function showPage_404()
{
    global $am;

    web_header( '404 ERROR');
    web_menu();

    echo '<div id="righty">'; // righty

    echo $am->W('PAGENOTFOUND');

    echo '<div class="block">';
    echo '<a href="index.php">' . $am->M('CLICKHERE') . '</a> To Return to Index Page';
    echo '</div>';

    echo '<div class="spacer30"></div>';
    
    echo '</div>'; // righty
    web_footer();
}

/**
 * Show 401 error
 *
 * @return void
 */
function showPage_401( $showMenu=false )
{
    global $am;
    // $error = $message = null;

    web_header( $am->M('TITLE_AUTHREQUIRED'), $am->getUrlByType('base',true));
    
    if ($showMenu) {
    	web_menu();
    }

    echo '<div id="righty">'; // righty

    echo '<h1>' . $am->M('TITLE_AUTHREQUIRED') . '</h1>';
    echo '<p>' . $am->M('NOTES_AUTHREQUIRED') . '</p>';

    echo '<div class="block">';
    echo '<ul>';
    echo '<li><a href="index.php?page=login">' . $am->M('CLICKHERE') .'</a> ' . $am->M('TOLOGIN') . '</li>';
    echo '<li><a href="../">' . $am->M('CLICKHERE') . '</a> ' . $am->M('TOPROTECTED') . '</li>';
    echo '</ul>';
    echo '</div>';
    echo '<div class="spacer30"></div>';
    
    if ($am->getConfigValue('allowsignup')) { 
        echo '<div class="block">';
        echo '<ul>';
        echo '<li><a href="index.php?page=signup">' . $am->M('SIGNUPLINK') . '</a></li>';
        echo '<li><a href="index.php?page=forgot">' . $am->M('FORGOTLINK') . '</a></li>';
        echo '</ul>';
        echo '</div>';
        echo '<div class="spacer30"></div>';
    }

    echo '</div>'; // righty
    web_footer();
}


/**
 * Shows page header
 *
 * @param string $title
 * @return void
 */
function web_header( $title=null, $basePath='', $optHeaders=array() )
{
    global $am;

?>
<html>
<head>
<title><?php echo (isset($title) ? $title : $am->M('TITLE')) . ' - ' ?>AuthMan Free</title>
  <?php foreach($optHeaders as $hdr) { print trim($hdr) . "\n"; } ?>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="expires" content="-1" />
  <meta name="keywords" content="htaccess,htpasswd,user accounts,web applications,software,site access,secure password,user authentication,script,subscriptions,website software,authman,subscription software,web application,website security,site administration,authman.com,security authentication,web site management,web site management system,php,ajax,protection,security,password,management,membership,login,htaccess management,htpasswd management,subscription script,apache,registration" />
  <meta name="description" content="AuthMan Free is authentication/password protection and membership management system written in PHP and licensed under the GNU GPL. It uses .htpasswd and .htaccess files to protect any web directory. Installation is easy and programming knowledge does not required." />
<?php if ($basePath != '') { echo "  <base href=\"" . $basePath . "\" />\n"; } ?>
  <link rel="stylesheet" href="<?php echo $basePath ?>authman.css" type="text/css" />
  <link rel="shortcut icon" href="<?php echo $basePath ?>images/favicon.ico" type="image/x-icon" />
  <script type="text/javascript" src="<?php echo $basePath ?>prototype.js" xml:space="preserve"></script>
<?php 
if ($am->hasFeature('tinymcejs')) {
	$tinymceUri = $am->getUrlByType('tinymcejs');
    echo '  <script type="text/javascript" src="' . $tinymceUri . '" xml:space="preserve"></script>';
    echo "\n";
} 
?>
<script language="Javascript" type="text/javascript" xml:space="preserve">
//<![CDATA[
<?php 


/**
 * Get Javascript Code
 *
 * @return string
 *
 */
function getJavascript()
{
    global $am;
    list($admin) = $am->getRecordsByType('authadminfile', null, 1, 0);

?>// general js
function mainCleanMessage()
{
    $('mainmessage').innerHTML = '';
}
function mainShowMessage( text )
{
    $('mainmessage').innerHTML = text;
    setTimeout( 'mainCleanMessage();', 5000 );
}
function getRandomNum(lbound, ubound) {
    return (Math.floor(Math.random() * (ubound - lbound)) + lbound);
}
function getRandomChar(number, lower) {
    var numberChars = "0123456789";
    var lowerChars = "abcdefghijklmnopqrstuvwxyz";
    var charSet = '';
    if (number == true)
        charSet += numberChars;
    if (lower == true)
        charSet += lowerChars;
    return charSet.charAt(getRandomNum(0, charSet.length));
}

function mainGeneratePassword( fname )
{
    var newPass = getRandomChar(false, true);

    for (var idx = 1; idx < 8; ++idx)
        newPass = newPass + getRandomChar(true, true);

    $($(fname + '_form').password).value = newPass;
}

function mainGeneratePassword2( fname )
{
    var newPass = getRandomChar(false, true);

    for (var idx = 1; idx < 8; ++idx)
        newPass = newPass + getRandomChar(true, true);

    $($(fname).password).value = newPass;
}

function fixAction( code ) {
    new Ajax.Updater('fixaction_'+code, 'index.php?page=ajax&action=fix&errno='+code);
}

function mainCheckUsername2( fname )
{
	var uname = $F( $(fname).username );
    if (uname == "") {
        alert("<?php echo $am->W('EMPTYUSERNAME') ?>");
        return;
    }

    var msg = $( fname + '_checkusername' );

    var requrl = 'index.php?page=users&action=checkusername&username='+uname;

    var req = new Ajax.Request( requrl,
            {
                method: 'get',
                onLoading: function()
                {
                    $($(fname).check).disable();
                    msg.update('<img src="images/loadinfo.gif"> Checking ...').style.color = 'black';
                },
                onSuccess: function( transport )
                { 
                    var response = transport.responseText || "no response text";
                    $($(fname).check).enable();
                    if (response == "USERFOUND") {
                        msg.update('<?php echo $am->E('USEREXISTS', ''); ?>').style.color = 'red';
                    } else {
                        msg.update('<?php echo $am->M('USERNAMEFREE'); ?>').style.color = 'green';
                    }                    
                },
                onFailure: function()
                { 
                    $($(fname).check).enable();
                    msg.update('<?php echo $am->E('UPDATEFAILED'); ?>').style.color = 'red';
                } 
            }
        );
    return;
}

function mainOnChangeSendMail2( fname )
{
	if ( $($(fname).sendmail).checked ) {
		$(fname + '_showtemplate').show();
	} else {
		$(fname + '_showtemplate').hide();
	}
}

function mainInsertAtCursor(myField, myValue) {
    if (myValue == '') {
        return;
    }

    //IE support
    if (document.selection) {
        try {
            myField.focus();
        } catch (e) { }
        
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    
    //MOZILLA/NETSCAPE support
    else if (myField.selectionStart || myField.selectionStart == '0') {
                     
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
                      + myValue
                      + myField.value.substring(endPos, myField.value.length);
    
    } else {
        myField.value += myValue;
    }
}

// ----------------------------------------------------------------------------
// HOME
// ----------------------------------------------------------------------------

function homeCheckUpdate() {
	var params = 'page=ajax&action=checkupdate';
    var req = new Ajax.Request( 'index.php', 
    {
    	method: 'post', parameters: params,
        onLoading: function()
        {
			$('update_loading').show();
        },
        onSuccess: function( transport )
        { 
			$('update_loading').hide();

			if (transport.responseText == '') {
	            mainShowMessage( '<div class="warning">No response</div>' );
			} else {
            	$('update_msg').show();
            	$('update_msg').update( transport.responseText );
			}
        },
        onFailure: function()
        { 
			$('update_loading').hide();
            mainShowMessage( '<div class="warning">checkiung failed</div>' );
        } 
    });
}

function homeOnSubmitForm(fObj)
{
	if (typeof fObj == 'string') {
		fObj = $(fObj);
	}
    var fname = fObj.name;
    
    if (fname == 'signup') {
        var valid =  $(fObj.username).present()  
                  && $(fObj.realname).present() 
                  && $(fObj.email).present(); 

        if (!valid) {
            $(fname+'_msg').update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }
        $(fname+'_msg').update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';

        return true;
    }
    
    if (fname == 'adminpassword') {
        var valid =  $($(fname).npassword).present()  
                  && $($(fname).npassword2).present();
                  
        if (!valid) {
            $(fname+'_msg').update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }

        var pass = $F($(fname).npassword);
        if (pass.length < 4) {
            $(fname+'_msg').update('<?php echo $am->E('PASSWORDTOOSHORT', 4) ?>').style.color = 'red';
        	return false;
        }        
        
        var pass2 = $F($(fname).npassword2);
        if (pass != pass2) {
            $(fname+'_msg').update('<?php echo $am->W('PASSWORDSMISSMATCHED') ?>').style.color = 'red';
        	return false;
        }
        $(fname+'_msg').update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';

        return true;
    }

    return false;
}

<?php if ($am->hasFeature('magpierss')) { ?>
function homeShowNews( doShow, doSave )
{
    if (doShow) {
        $('home-news-content').show();

//        $('home-news-hide').show();
//        $('home-news-show').hide();

        new Ajax.Updater('home-news-content', 'index.php?page=ajax&action=getnews');
        
    } else {
        $('home-news-content').hide();
        
//        $('home-news-hide').hide();
//        $('home-news-show').show();
    }
    
    if (!doSave) {
        return false;
    }    
    
    params = 'page=ajax&action=setshowmembernews&value=' + (doShow?1:0);
    new Ajax.Request( 'index.php', 
    {
        method: 'post', parameters: params,
        onLoading: function() { $('news_loading').show(); },
        onSuccess: function( transport ) { $('news_loading').hide(); },
        onFailure: function() {  $('update_loading').hide(); }
    });
    
    return false;
}
<?php } ?>

// ----------------------------------------------------------------------------
// USERS
// ----------------------------------------------------------------------------

var usersCheckUsersFlag = false;
function usersCheckUsers( fields )
{
    usersCheckUsersFlag = usersCheckUsersFlag ? false : true;
	$('usersList').getInputs("checkbox","usernames[]").find(function(e) {
		if (usersCheckUsersFlag) {
			e.writeAttribute('checked', 'checked');
		} else {
			e.writeAttribute('checked', null);
		}		
    });
    return;
}

function usersShowForm( fname, opt )
{
	if (fname == "") {
		return false;
	}
	
	if (fname == 'usersMailSel') {
		fname = 'userMail';
		opt = 'SELECTED';
	}
	
	if (opt == '' || opt == 'hide') {
		$(fname).hide();
	} else {
        $(fname).show();
        $(fname + '_form').onsubmit = usersOnSubmitForm;
	 
    	var f = $(fname + '_form');

	  	if (fname == 'userEdit') {
			$(f.oldusername).value = opt;
	    
	  	} else if (fname == 'userDelete') {
       		$(f.username).value = opt;
		
	  	} else if (fname == 'userMail') {
    		$(f.username).value = opt;

	    	// set mailto field
	    	if (opt == 'SELECTED') {
	    		// to selected users
	    		$mailtotext = 'Selected Users';
	    	} else {
	    		// to one user 
				$mailtotext = users[opt]["name"];
				if (users[opt]["info"] != "") {
					$mailtotext = users[opt]["name"];
				}
				$mailtotext = '&quot;' + $mailtotext +'&quot; &lt;'+  users[opt]["email"] +'&gt;';
			}
			$(fname + '_mailto').innerHTML = $mailtotext;
		}
    
		usersResetForm( fname );

    }

    var els = new Array("userEdit","userAdd","userDelete","usersDelSel","usersDelAll","userMail");
    for (var i=0,len=els.length; i < len; ++i) {
        if (fname != els[i])
            $(els[i]).hide();
    }
}

function usersOnSubmitForm()
{
    var form = this.identify();
    var msg = $(form + '_msg');

    if (form == 'userEdit_form') {
        var valid = $(this.username).present();
        
        // sendmail -> check email address
		if ( $(this.sendmail).checked ) {
        	valid = valid && $(this.email).present() && $F(this.template) != "";
        }
        
        if (!valid) {
            msg.update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }

        return true;
    } // end of userEdit_form

    if (form == 'userAdd_form') {
        var valid = $(this.username).present() && $(this.password).present();

        // sendmail -> check email address
		if ( $(this.sendmail).checked ) {
        	valid = valid && $(this.email).present() && $F(this.template) != "";
        }

        if (!valid) {
            msg.update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }

        return true;
    } // end of userAdd_form

    if (form == 'userDelete_form') {
    	return true;
    }

    if (form == 'usersDelSel_form') {
    	var count = 0;
        $('usersList').getInputs("checkbox","usernames[]").find(function(e) {
        	if (e.checked) {
        		count++;
        	}
        });
       	if (count < 1) {
       		alert('<?php echo $am->W('SELUSERSFIRST') ?>');
       		return false;
       	}
    	
       	$($('usersList').action).value = 'deleteselusers';
       	$('usersList').submit();
        return false;
    }

    if (form == 'usersDelAll_form') {
    	return true;
    }

    // Sending e-mail to the user, ajax
    if (form == 'userMail_form') {
    	var valid = $(this.subject).present() && $(this.body).present();

        if (!valid) {
            msg.update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        } else {
	        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';
        }

        var params = $(form).serialize(false);
        
        opt = $($(form).username).value;
        if (opt == 'SELECTED') {
	    	var count = 0;
	        $('usersList').getInputs("checkbox","usernames[]").find(function(e) {
	        	if (e.checked) {
	        		count++;
	        		params += '&usernames[]=' + e.value;
	        	}
	        });        
	       	if (count < 1) {
	       		alert('<?php echo $am->W('SELUSERSFIRST') ?>');
	       		return false;
	       	}
        }
            
        var req = new Ajax.Request( 'index.php',
            { 
                method: 'post',
                parameters: params,
                onLoading: function()
                {
					$('userMail_loading').show();
                },
                onSuccess: function( transport )
                { 
                    $('userMail_loading').hide();
                    mainShowMessage( transport.responseText || '<div class="warning">No response received</div>' );
                	// usersResetForm( 'userMail' );
                },
                onFailure: function()
                { 
                    $('userMail_loading').hide();
                    mainShowMessage( '<div class="warning">form failed</div>' );
                } 
            }
        );
    	return false;
    }
    
    return false;
}

function usersResetForm( fname )
{
    var f = $(fname + '_form');

    if (fname == 'userEdit') {
        var opt = $F(f.oldusername);

        $(f.username).value = opt;
        $(f.realname).value = opt == "" ? "" : users[opt]['info'];
        $(f.password).value = "";
        $(f.email).value    = opt == "" ? "" : users[opt]['email'];

        var msg = $(fname + '_form_msg');
        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';

    } else if (fname == 'userAdd') {
	    $(f.username).value = "";
        $(f.realname).value = "";
        $(f.email).value    = "";
        
        // auto generate new password 
        mainGeneratePassword( fname );

        $(f.check).enable();
        $(fname + '_checkUsername').innerHTML = '';

        var msg = $(fname + '_form_msg');
        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';

    } else if (fname == 'userDelete') {
    	// nothing to do
    } else if (fname == 'usersDelSel') {
    	// nothing to do
    } else if (fname == 'usersDelAll') {
    	// nothing to do
    	
    } else if (fname == 'userMail') {

    	// set values (remove controls)
<?php if ($am->hasFeature('tinymcejs')) { ?>
        if (tinyMCE.getInstanceById(fname+'_body') != null) {
            tinyMCE.execCommand('mceRemoveControl', false, fname+'_body');
        }
<?php } ?>    

    	$($(fname+'_form').subject).value = '';
    	$($(fname+'_form').body).value = '';
    	
    	// set type to plaintext
		$(fname+'_form').getInputs('radio','type').find(function(e)
		{
			if (e.value == 'plaintext') { e.checked = true; }
		});

        var msg = $(fname + '_form_msg');
        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';
    }

}

function usersCheckUsername( fname )
{
    var f = $(fname + '_form');
    if ($F(f.username) == "") {
        alert("<?php echo $am->W('EMPTYUSERNAME') ?>");
        return;
    }

    var msg = $(fname + '_checkUsername');

    var requrl = 'index.php?page=users&action=checkusername&username='+$F(f.username);
    var req = new Ajax.Request( requrl,
            {
                method: 'get',
                onLoading: function()
                {
                    $(f.check).disable();
                    msg.update('<img src="images/loadinfo.gif"> Checking ...').style.color = 'black';
                },
                onSuccess: function( transport )
                { 
                    var response = transport.responseText || "no response text";

                    $(f.check).enable();
                    if (response == "USERFOUND") {
                        msg.update('<?php echo $am->E('USEREXISTS', ''); ?>').style.color = 'red';
                    } else {
                        msg.update('<?php echo $am->M('USERNAMEFREE'); ?>').style.color = 'green';
                    }
                },
                onFailure: function()
                { 
                    $(f.check).enable();
                    msg.update('<?php echo $am->E('UPDATEFAILED'); ?>').style.color = 'red';
                } 
            }
        );

    return;
}

function usersOnDeleteGroup( groupname )
{
    if (!confirm("<?php echo $am->M('Q_DELETEGROUP') ?>")) {
        return;
    }

    var url = 'index.php?page=users&groupname='+groupname+'&action=deletegroup';
    document.location = url;
}

function usersOnChangeSendMail( fname )
{
	var f = $(fname + '_form');
	
	if ( $(f.sendmail).checked ) {
		$(fname + '_showtemplate').show();
	} else {
		$(fname + '_showtemplate').hide();
	}
}

// Mailing
function usersOnChangeTemplate( fname, opt )
{
	var f = $(fname+'_form');
	
	// reset select 
	$(fname + '_default_templateid').selected = true;

	if (opt == '') {
		return false;
	}
	
	// set values (remove controls)
<?php if ($am->hasFeature('tinymcejs')) { ?>
        if (tinyMCE.getInstanceById(fname+'_body') != null) {
            tinyMCE.execCommand('mceRemoveControl', false, fname+'_body');
        }
<?php } ?>    

    $(f).getInputs("radio","type").find(function(e)
    {
        if (e.value == emailTemplates[opt]["type"]) {
            e.checked = true;
        }
    });

	$(f.subject).value = emailTemplates[opt]["subject"];
	$(f.body).value = emailTemplates[opt]["contents"];

	usersOnChangeTemplateType( fname );

	return false;
}

// Changing html -> plaintext or plaintext -> html
function usersOnChangeTemplateType( fname )
{
<?php if (false == $am->hasFeature('tinymcejs')) { ?>
	// no tiny mce found
	return false;
<?php }?>

	var res = $(fname+'_form').getInputs('radio','type').find(function(e){return e.checked;});
    if (res == null) {
        alert('No type selected');
        return false;
    }
    
    var typename = $F(res);
    var id = fname + '_body';
    
    if (typename == 'html') {
        if (tinyMCE.getInstanceById(id) == null) {
            tinyMCE.execCommand('mceAddControl', false, id);
        }
    } else {
        if (tinyMCE.getInstanceById(id) != null) {
            tinyMCE.execCommand('mceRemoveControl', false, id);
        }
    }
    
    return true;
}

// ----------------------------------------------------------------------------
// ACCESS RULES
// ----------------------------------------------------------------------------

function accessOnSubmit( fname ) 
{
    accessListSelect( 'access_allow' );
    accessListSelect( 'access_deny' );
    
    var params = $(fname).serialize(false);

    accessListDeselect( 'access_allow' );
    accessListDeselect( 'access_deny' );
    
    var req = new Ajax.Request( 'index.php', { method: 'post', parameters: params,
            onLoading: function() {
                $('edit_loading').show();
            },
            onSuccess: function( transport ) { 
                $('edit_loading').hide();
                $('noaccessfile_msg').hide();
                mainShowMessage( transport.responseText || '<div class="warning">No response</div>' );
            },
            onFailure: function() { 
                $('edit_loading').hide();
                mainShowMessage( '<div class="warning">ajax updating failed</div>' );
            } 
    });

    return false;
}

function accessOnEnableErrorDocument401( fname ) 
{
	if ($($(fname).enableerrordocument401).checked) {
		$('editErrordocument401').show();
	} else {
		$('editErrordocument401').hide();
	}
}

function accessOnDefErrorDocument401( fname )
{
	if ($($(fname).deferrordocument401).checked) {
		$($(fname).errordocument401).value = '<?php
			echo htmlEncode($am->getUrlByType('errordocument401'),true,true) 
		?>';
		$($(fname).errordocument401).disable();
	} else {
		$($(fname).errordocument401).enable();
	}
	return false;
}

function accessOnDefAuthUserFile( fname )
{
	if ($($(fname).defauthuserfile).checked) {
		$($(fname).authuserfile).value = '<?php
			echo htmlEncode($am->getDefaultFilePathByType('authuserfile'),true,true) 
		?>';
		$($(fname).authuserfile).disable();
	} else {
		$($(fname).authuserfile).enable();
	}
	return false;
}

// List operations
function accessListChoose( listId )
{
    var editId = listId + '_edit';
    var idx = $(listId).selectedIndex;
    if (idx < 0) {
        // $(editId).disable();
        $(editId).value = '';
        return;
    }
    // $(editId).enable();
    $(editId).value = $(listId).options[idx].value;
}

function accessListSelect( listId )
{
    for (var i = $(listId).options.length; i > 1; --i) {
        $(listId).options[i-1].selected = true;
    }
}

function accessListDeselect( listId )
{
    for (var i = $(listId).options.length; i > 0; --i) {
        if ($(listId).options[i-1].selected) {
            $(listId).options[i-1].selected = false;
        }
    }    
}

function accessListEdit( listId )        
{
    var editId = listId + '_edit';
    var idx = $(listId).selectedIndex;

    if ($F(editId) == '') {
        alert('Enter data first');
        return accessListChoose( listId );
    }
    
    if (idx <= 0) {
        // adding new item
        idx = $(listId).options.length;
    }
    $(listId).options[idx] = new Option( $F(editId), $F(editId) );
    
    $(editId).value = '';
    // $(editId).disable();
    
    accessListDeselect( listId );
}

function accessListDelete( listId )
{
    var editId = listId + '_edit';
    $(editId).value = '';
    // $(editId).disable();

    // 0 is registered for create new item action
    for (var i = $(listId).options.length; i > 0; --i) {
        if (i > 1 && $(listId).options[i-1].selected) {
            $(listId).options[i-1] = null;
            continue;
        }
        if ($(listId).options[i-1].selected) {
            $(listId).options[i-1].selected = false;
        }
    }
}

// ----------------------------------------------------------------------------
// FILES
// ----------------------------------------------------------------------------

function filesUpdateContents( filetype )
{
    var params = $(filetype+'_form').serialize(false);
    var req = new Ajax.Request( 'index.php', { method: 'post', parameters: params,
            onLoading: function()
            {
                $(filetype + '_loading').show();
                $(filetype + '_submit').disable();
                $(filetype + '_reset').disable();

                $(filetype + '_message').hide();
            },
            onSuccess: function( transport )
            { 
                $(filetype + '_submit').enable();
                $(filetype + '_reset').enable();
                $(filetype + '_loading').hide();

                mainShowMessage( transport.responseText || 'No response' );
            },
            onFailure: function()
            { 
                mainShowMessage( '<div class="warning">'+filetype+': ajax updating failed</div>' );

                $(filetype + '_submit').enable();
                $(filetype + '_reset').enable();
                $(filetype + '_loading').hide();
            } 
        }
    );

    return false;
}

function filesOnChangeContents( filetype )
{
    $(filetype + '_loading').hide();
    $(filetype + '_submit').enable();
    $(filetype + '_reset').enable();
}
function filesDownload( filetype )
{
    // IE way
    //var contents = escape( $(filetype+'_contents').value );
    //mydoc = document.open();
    //mydoc.write(contents);
    //mydoc.execCommand("saveAs", true, filetype+".txt");
    //mydoc.close();

    document.location = 'index.php?page=files&action=downloadfile&filetype='+filetype;
}

// ----------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------------------------------------------

function setsShowForm( fname, opt )
{
    if (opt == '' || opt == 'hide') {
        $(fname).hide();
    } else {
        $(fname).show();

        if (fname=='editAdmin' || fname=='chooseTpl' || fname=='addTpl' || fname=='editTpl') {
            setsResetForm( fname );
        }

        if (fname=='editAdmin' || fname=='resetProtected' || fname=='addTpl' || fname=='editTpl') {
            var f = $(fname + '_form');
            $(f).onsubmit = setsOnSubmitForm;
        }
    }

    var els = new Array("editAdmin", "downFiles", "resetProtected", "prefFiles");
    for (var i=0,len=els.length; i < len; ++i) {
        if (fname != els[i]) {
            $(els[i]).hide();
        }
    }

    if (fname == "chooseTpl" && opt != 'hide' ||
        fname == "addTpl" && opt=='hide' ||
        fname == "editTpl" && opt=='hide') {
        $('chooseTpl').show();
        setsResetForm('chooseTpl');

    } else if (fname == "addTpl" && opt!='hide') {
        $('chooseTpl').show();
        $('addTpl').show();
        $('editTpl').hide();
    } else if (fname == "editTpl" && opt!='' && opt!='hide') {
        $('chooseTpl').show();
        $('addTpl').hide();
        $('editTpl').show();
    } else {
        $('chooseTpl').hide();
        $('addTpl').hide();
        $('editTpl').hide();
    }
}

function setsResetForm( fname )
{
    var f = $(fname + '_form');

    if (fname == 'editAdmin') {
        $(f.username).value = "<?php echo addslashes($admin['name']) ?>";
        $(f.realname).value = "<?php echo addslashes($admin['info']) ?>";
        $(f.password).value = "<?php echo isset($admin['pass_raw']) ? addslashes($admin['pass_raw']) : '' ?>";
        $(f.email).value    = "<?php echo addslashes($admin['email']) ?>";

        var msg = $(fname + '_form_msg');
        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';
    }

    if (fname=='chooseTpl' || fname=='addTpl') {
        $('chooseTpl_default').selected = true;
    }

    if (fname=='addTpl') {
        var msg = $(fname + '_form_msg');
        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';
        setsOnChangeTemplateType( fname ) ;
    }

    if (fname=='editTpl') {
        var cf = $('chooseTpl_form');
        var opt = $(cf.templateid).value.toString();

        if (opt != '') {
            $(f.templateid).value = opt;
            $(f.templatename).value = emailTemplates[opt]["name"];
            $(f.templatesubject).value = emailTemplates[opt]["subject"];
            // type
            $('editTpl_form').getInputs("radio","templatetype").find(function(e)
            {
                if (e.value == emailTemplates[opt]["type"]) {
                    // e.writeAttribute('checked', 'checked');
                    e.checked = true;
                }
            });

            // enable / disable delete button for system templates
            if (emailTemplates[opt]['role'] == 'undefinied') {
            	$(f.delete_submit).enable();
            } else {
            	$(f.delete_submit).disable();
            }
            
<?php if ($am->hasFeature('tinymcejs')) { ?>
            if (tinyMCE.getInstanceById(fname + '_templatebody') != null) {
                tinyMCE.execCommand('mceRemoveControl', false, fname + '_templatebody');
            }
<?php } ?>
            $(fname + '_templatebody').value = emailTemplates[opt]["contents"];

            setsOnChangeTemplateType( fname );
        }
    }

}

function setsOnSubmitForm()
{
    var fname = this.identify();

    if (fname == 'editAdmin_form') {
        var valid = $(this.username).present();
        var msg = $(fname + '_msg');

        if (!valid) {
            msg.update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }

        return true;
    } 

    if (fname == 'resetProtected_form') {
        var params = $(fname).serialize(false);
        var req = new Ajax.Request( 'index.php',
            { 
                method: 'post',
                parameters: params,
                onLoading: function()
                {
                    $('resetProtected_loading').show();
                },
                onSuccess: function( transport )
                { 
                    $('resetProtected_loading').hide();
                    mainShowMessage( transport.responseText || '<div class="warning">No response received</div>' );
                },
                onFailure: function()
                { 
                    $('resetProtected_loading').hide();

                    mainShowMessage( '<div class="warning">form updating failed</div>' );
                } 
            }
        );

        return false;
    }

    if (fname=='addTpl_form' || fname=='editTpl_form') {
        var valid = $(this.templatename).present();
        var msg = $(fname + '_msg');

        if (!valid) {
            msg.update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }
        return true;
    } 

    return false;
}

function setsPrefFilesUpdate( filetype )
{
    var params = $(filetype+'_form').serialize(false);
    var req = new Ajax.Request( 'index.php',
        { 
            method: 'post',
            parameters: params,
            onLoading: function()
            {
                $(filetype + '_loading').show();
                $(filetype + '_message').hide();
            },
            onSuccess: function( transport )
            { 
                $(filetype + '_loading').hide();
                mainShowMessage( transport.responseText || '<div class="warning">No response received</div>' );
            },
            onFailure: function()
            { 
                $(filetype + '_loading').hide();
                mainShowMessage( '<div class="warning">'+filetype+': updating failed</div>' );
            } 
        }
    );
    return false;
}

function setsPrefFilesDownload( filetype )
{
    document.location = 'index.php?page=settings&action=downloadfile&filetype='+filetype;
}

function setsOnNewTemplate( val )
{
    setsShowForm('addTpl', 'show');
}

function setsOnChangeTemplate( val )
{
    if (val == '') {
        setsShowForm('editTpl', 'hide');
        setsShowForm('chooseTpl', 'show');
        return;
    }
    setsShowForm('editTpl', val);
}

function setsOnChangeTemplateType( fname  ) 
{
<?php if (false == $am->hasFeature('tinymcejs')) { ?>
	// no tiny mce found
	return false;
<?php }?>
    var res = $(fname+'_form').getInputs('radio','templatetype').find(function(e){return e.checked;});
    if (res == null) {
        alert('No type selected');
        return false;
    }
    var typename = $F(res);
    
    var id = fname + '_templatebody';
    if (typename == 'html') {
        if (tinyMCE.getInstanceById(id) == null) {
            tinyMCE.execCommand('mceAddControl', false, id);
        }
        // $(fname + '_templatevar').hide();
    } else {
        if (tinyMCE.getInstanceById(id) != null) {
            tinyMCE.execCommand('mceRemoveControl', false, id);
        }
        // $(fname + '_templatevar').show();
    }
    
    return true;
}

function setsSetChooseTpl( id ) 
{
    var f = $('chooseTpl_form');
    $(f.templateid).select("option").each( function(e) {
        if (e.value == id ) {
        	e.selected = true;
        }
    });
    $('chooseTpl').show();
}

function setsOnDeleteTemplate() 
{
    if (!confirm("<?php echo $am->M('Q_DELETETEMPLATE'); ?>")) {
        return false;
    }

    var f = $('editTpl_form');
    $(f.action).value = 'deletetemplate';
    return true;
}

// ----------------------------------------------------------------------------
// MEMBER
// ----------------------------------------------------------------------------

function memberOnSubmitForm( fObj )
{
	if (typeof fObj == 'string') {
		fObj = $(fObj);
	}
    var fname = fObj.name;
    
    if (fname == 'memberEdit') {
        var valid = $(fObj.realname).present() && $(fObj.email).present(); 
        if (!valid) {
            $(fname+'_msg').update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }
        $(fname+'_msg').update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';

        var params = $(fname).serialize(false);
        var req = new Ajax.Request( 'index.php',
            { 
                method: 'post',
                parameters: params,
                onLoading: function()
                {
                    $(fname+'_loading').show();
                },
                onSuccess: function( transport )
                { 
                    $(fname+'_loading').hide();
                    mainShowMessage( transport.responseText || '<div class="warning">No response received</div>' );
                    $($(fname).password).value = '';
                },
                onFailure: function()
                { 
                    $(fname+'_loading').hide();
                    mainShowMessage( '<div class="warning">updating failed</div>' );
                } 
            }
        );

        return false;
    }
    
    if (fname == 'forgot') {
        var valid = $(fObj.username).present() && $(fObj.email).present();
        if (!valid) {
            $(fname+'_msg').update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }
        
        $(fname+'_msg').update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black'; 
        
        return true;
    }
    
    if (fname == 'recover') {
        var valid = $(fObj.username).present() && $(fObj.key).present();
        if (!valid) {
            $(fname+'_msg').update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }
        
        $(fname+'_msg').update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black'; 
        
        return true;
    }
        
    return false;
}

// ----------------------------------------------------------------------------
// SUPPORT
// ----------------------------------------------------------------------------

function supportResetForm( id )
{
    var fname = typeof(id) == 'object' ? id.name : id; 
    
    if (fname == 'supportRequest') {
        $(fname+'_msg').update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';
        
        $($(fname).subject).value = "";
        $($(fname).body).value = "";
    }
    
    return false;
}

function supportOnSubmitForm(fObj)
{
	if (typeof fObj == 'string') {
		fObj = $(fObj);
	}
    var fname = fObj.name;
    
    if (fname == 'supportRequest') {
        var valid = $(fObj.subject).present() && $(fObj.body).present(); 
        var msg = $(fname + '_msg');

        if (!valid) {
            msg.update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }
        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';

        var params = $(fname).serialize(false);
        
        var req = new Ajax.Request( 'index.php',
            { 
                method: 'post',
                parameters: params,
                onLoading: function()
                {
                    $(fname+'_loading').show();
                },
                onSuccess: function( transport )
                { 
                    $(fname+'_loading').hide();
                    mainShowMessage( transport.responseText || '<div class="warning">No response received</div>' );
                    supportResetForm( fname );
                },
                onFailure: function()
                { 
                    $(fname+'_loading').hide();
                    mainShowMessage( '<div class="warning">updating failed</div>' );
                } 
            }
        );

        return false;
    }

    return false;
}

// ----------------------------------------------------------------------------
// SIGNUPS
// ----------------------------------------------------------------------------

function signupsShowForm( fname, opt )
{
    if (opt == '' || opt == 'hide') {
        $(fname+'_container').hide();
    } else {
        $(fname+'_container').show();

        if (fname == 'approve' || fname == 'delete') {
	        $($(fname).username).value = opt;
        }

        signupsResetForm( fname );
        $(fname).onsubmit = signupsOnSubmitForm;
    }

    var els = new Array("approve", "delete", "deleteall", "deleteselected");
    for (var i=0,len=els.length; i < len; ++i) {
        if (fname != els[i]) {
            $(els[i]+'_container').hide();
        }
    }
}

function signupsResetForm( fname )
{
    if (fname == 'approve') {

	    var uname = $F($(fname).username);
        $($(fname).realname).value = signups[uname]['info'];
        $($(fname).password).value = signups[uname]['pass'];
        $($(fname).email).value    = signups[uname]['email'];

        $(fname+'_datetime').innerHTML = signups[uname]['ts'];
        $(fname+'_remoteaddr').innerHTML = signups[uname]['remoteaddr'];
        $(fname+'_referer').innerHTML = signups[uname]['referer'];
        
        var msg = $(fname + '_msg');
        msg.update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';
    }
    
    return true;
}

function signupsOnSubmitForm( event )
{
	if (typeof event == 'string') {
		fObj = $(fObj);
	} else {
		fObj = this;
	}
    
	var fname = fObj.identify();
    
 	if (fname == 'delete' || fname == 'deleteall') {
 		return true;
    }

    if (fname == 'deleteselected') {
    	var count = 0;
        $('usersList').getInputs("checkbox","usernames[]").find(function(e) {
        	if (e.checked) {
        		count++;
        	}
        });
       	if (count < 1) {
       		alert('<?php echo $am->W('SELUSERSFIRST') ?>');
       		return false;
       	}
    	
       	$($('usersList').action).value = 'deleteselected';
       	$('usersList').submit();
        return false;
    }    

    if (fname == 'approve') {
        var valid =  $($(fname).username).present() && $($(fname).password).present();

        if (!valid) {
            $(fname+'_msg').update('<?php echo $am->W('FILLOUTALL') ?>').style.color = 'red';
            return false;
        }
        $(fname+'_msg').update('<?php echo $am->M('FILLFIELDS'); ?>').style.color = 'black';
    	
    	return true;
    }

    return false;
}

<?php
}

/**
 * Return Javascript Array
 *
 * @param $string $type
 * @return $string
 */
function getJsArray( $type )
{
	global $am;
	
	$s = '// JS code: ' . $type . "\n";
	
	if ($type == 'emailTemplates') {
		$tplsArray = $am->getTemplates();
			
	    $s .= "var emailTemplates = new Array();\n";
	    foreach ($tplsArray as $id=>$tpl) {
	        $id = addslashes($id);
	
	        $s .= "emailTemplates[\"$id\"] = new Array();\n";
	        foreach ($tpl as $k=>$v) {
	            $v = $v != '' ? addslashes($v) : '';

	            $v = preg_replace("/(\n|\r\n)/", "\\n", $v);
	            $s .= " emailTemplates[\"$id\"][\"$k\"] = \"$v\";\n";
	        }
	    }
	}
	
	return $s;
}

// end of js
getJavascript();
?>
//]]>
</script>
</head>
<body>
<div id="wrapper">
  <?php 
    if ($am->isAuthenticated()) {
        $user = $am->getAuthenticatedUser();
        $info = isset($user['info']) && $user['info'] != '' ? $user['info'] : $user['name'];

        echo '<div id="headermember">';
        echo $am->M('LOGGEDAS', $info) . ', ';
        echo '<a href="http://fakeuser:fakepass@roster.biosupport.se/logout/index.php">' . $am->M('CLICKHERE') . '</a> ';
        echo $am->M('TOLOGOUT');  
        echo '</div>';
    }
  ?>

 <div id="header">
  <div id="site_logo"> AuthMan <span class="high">Free</span> </div>
<!--  <div id="languages"><a href="">How to change language</a></div> -->
 </div>

 <div id="content">

 <?php
    if ($am->isDemo()) {
        print '<div id="demomessage">'.$am->M('NOTES_DEMOMODE').'</div>';
    }
}

/**
 * Shows footer
 *
 * @return void
 */
function web_footer()
{
?>
 </div> <!-- end of content div -->
 <div id="footer">Powered by <a href="http://www.authman.com">AuthMan Free</a> &copy; 2008 Authman.com. All rights reserved.</div>
</body>
</html>
<?php
}

/**
 * Shows main menu
 *  
 * @return void
 */
function web_menu()
{
    global $am;

    echo '<div id="lefty"><ul>';

    echo '<li><a href="index.php?page=home">' . $am->M('MENU_HOME') .'</a></li>';

    if (!$am->isAuthenticated()) {
        echo '<li><a href="index.php?page=login">' . $am->M('MENU_LOGIN') .'</a></li>';
        
        if ($am->getConfigValue('allowsignup')) {
	        echo '<li><a href="index.php?page=signup">' . $am->M('MENU_SIGNUP') .'</a></li>';    }
    	}
    
    if ($am->isAuthenticated()) {
	    if (!$am->isAdmin()) {
    	    echo '<li><a href="index.php?page=edit">' . $am->M('MENU_MEMBEREDIT') . '</a></li>';
    	    echo '<li><a href="index.php?page=remove">' . $am->M('MENU_MEMBERDELETE') . '</a></li>';
	    }
    }
    if ($am->isAdmin()) {
        echo '<li><a href="index.php?page=access">' . $am->M('MENU_ACCESS') . '</a></li>';
        echo '<li><a href="index.php?page=users">' . $am->M('MENU_USERS') . '</a></li>';
        echo '<li><a href="index.php?page=files">' . $am->M('MENU_FILES') . '</a></li>';
//        echo '<li><a href="index.php?page=mailing">' . $am->M('MENU_MAILING') . '</a></li>';
        echo '<li><a href="index.php?page=settings">' . $am->M('MENU_SETTINGS') . '</a></li>';
    }

    if ($am->isAuthenticated()) {
	    echo '<li><a href="index.php?page=support">' . $am->M('MENU_SUPPORT') . '</a></li>';
	    if (!$am->isAdmin()) {
    	    echo '<li><a href="http://fakeuser:fakepass@roster.biosupport.se/logout/index.php">' . $am->M('MENU_LOGOUT') . '</a></li>';
	    }
    }
        
    echo '</ul></div>';
}

function fmt_message( $msg )
{
    if (!isset($msg)) {
        return false;
    }

    $sout = '';
    foreach (split("[\r\n\t ]+", $msg) as $s) {
        if (strlen($s) > 30) {

            if (substr($s,0,1)=='/') {
                $ss = strstr( substr($s,-40), '/' );
                if ($ss == false || strlen($ss) < 10) {
                    $s = '<u>...</u>' . substr($s, -30);
                } else {
                    $s = '<u>...</u>' . $ss;
                }

            } else if (substr($s,1,2)==':\\') {
                $ss = strstr( substr($s,-40), '\\' );
                if ($ss == false || strlen($ss) < 10) {
                    $s = '<u>...</u>' . substr($s, -30);
                } else {
                    $s = '<u>...</u>' . $ss;
                }
                                
            } else {
                $s = substr($s, 0, 30) . '<u>.</u>';
            }

        }
        $sout .= ' ' . $s;
    }
    return $sout;
}

function web_message( $msg, $err )
{
    print '<div id="mainmessage">';
    $count = 0;
    if (isset($msg)) {
        if (!is_array($msg)) {
            $msg = array( $msg );
        }

        foreach($msg as $text) {
            if (isset($text)) {
                print '<div class="message">' . fmt_message($text) . '</div>';
                $count++;
            }
        }
    }

    if (isset($err)) {
        if (!is_array($err)) {
            $err= array( $err);
        }

        foreach($err as $text) {
            if (isset($text)) {
                print '<div class="warning">' . fmt_message($text) . '</div>';
                $count++;
            }
        }
    }

    print '</div>';

    if ($count) {
        print "\n<script type=\"text/javascript\" xml:space=\"preserve\">\n//<![CDATA[\n";
        print "setTimeout(\"mainCleanMessage();\",5000);"; 
        print "//]]>\n</script>\n";
    }
}

################################################################################
function web_stepper( $prefixurl, $total, $limit=10, $page=1 )
{
    global $am;

    if (empty($limit)) {
        $total_pages = 1;
        $page = 1;

        $last = $total;
        $offset = 0;

    } else {
        $total_pages = ceil( $total / $limit );
        if ($total_pages < 1)
            $total_pages = 1;

        if ($page > $total_pages)
            $page = $total_pages;

        $last = $page * $limit;
        if ($last > $total)
            $last = $total;

        $offset = ($page-1) * $limit;
        if ($offset > $last)
            $offset = $last;
    }

    print '<div class="stepper">';

    // Showing N-N of N   Records <select> per page
    print '<div class="pageinfo">';
    print $am->M('STEPPER', ($total > 0 ? $offset+1 : 0), $last, $total);

    echo ' ' . $am->M('WITH') . ' ';

    $values = array(10=>10, 20=>20, 50=>50, '< all >'=>999999);
    print "<select id=\"limit\" name=\"limit\" onChange=\"document.location='".$prefixurl."p=1&limit='+\$F($(limit));\">\n";
    foreach($values as $t=>$i) {
        printf(" <option value=\"%d\"%s>%s</option>\n", $i, ($limit == $i ? ' selected=selected' : ''), $t);
    }
    print "</select>" . $am->M('RECORDSPERPAGE');
    print '</div>';

    // 1 2 3 ...
    print '<div class="pages">';
    if ($page > 1)
        printf('<a href="%sp=%d">%s</a>', $prefixurl, $page-1, '&lt;');

    $prev_p = null; 
    for ($p = 1; $p <= $total_pages; $p++) {

        if ($p == $page) {
            printf('<a class="active" href="%sp=%d">%d</a>', $prefixurl, $p, $p);
            $prev_p = $p;
            continue;
        }

        if ($p<3 || $p > $total_pages-2 || $p > $page-2 && $p < $page+2) {
            printf('<a href="%sp=%d">%d</a>', $prefixurl, $p, $p);
            $prev_p = $p;
            continue;
        } 
        
        if ($prev_p) // && $prev_p+1 != $p) 
        { 
            print '<span>...</span>';
            $prev_p = null;
        }
    }
    if ($page < $total_pages)
        printf('<a href="%sp=%d">%s</a>', $prefixurl, $page+1, '&gt;');
    print '</div>';

    print '</div>';
}

/**
 * Header redirect wrapper
 *
 * @param string $url
 * @param int $time default 3
 * @param string $message
 * @return void
 */
function redirect_header($url, $time = 3, $message = '')
{
    define("_IFNOTRELOAD","If the page does not automatically reload, please click <a href='%s'>here</a>");

    if (!defined("ENT_QUOTES")) { 
    	define("ENT_QUOTES", 3);
    }
    
    $url = preg_replace("/&amp;/i", '&', htmlspecialchars($url, ENT_QUOTES));
    $headers = array(
    	'<meta http-equiv="Refresh" content="'.$time.'; url='.$url.'" />'
    );

    web_header( 'REDIRECT MESSAGE', null, $headers );
       
    echo '<div id="righty">'; // righty
    echo '<div align="center">
          <div class="redirect">
          <span>'.$message.'</span>
          <hr />
          <p>'.sprintf(_IFNOTRELOAD, $url).'</p>
          </div>
          </div>';
    echo '</div>'; // righty        

    web_footer();
    exit();
}
################################################################################

function htmlDecode( $val, $addSlashes=false )
{
	$val = trim($val);
	$val = @html_entity_decode($val, ENT_COMPAT, 'UTF-8');
	if ($addSlashes) {
		$val = addslashes($val);
	}
	return $val;
}

function htmlEncode( $val, $addSlashes=false, $quoteBackSlashes=false )
{
	$val = @htmlentities($val, ENT_COMPAT, 'UTF-8');
	if ($addSlashes) {
		$val = addSlashes($val);		
	}
	if ($quoteBackSlashes) {
		$val = preg_replace('/[\\\]/', '\\\\', $val);
	}
	return $val;
}

/**
* Return true if there is variable with given name
* 
* @param string $name
* @return bool
*/
function hasParam( $name )
{
    if (array_key_exists($name, $_GET)) {
        return true;
    }

    if (array_key_exists($name, $_POST)) {
        return true;
    }

    return false;
}

/**
 * Return GET or POST variable.
 *
 * @param string $name
 * @param string $default
 * @param mixed $filters
 * @return mixed
 */
function getParam( $name, $default=null, $filters=array() )
{
    global $am;
    
    $val = null;

    if (array_key_exists($name, $_POST)) {
        $val = $_POST[$name];
    } else if (array_key_exists($name, $_GET)) {
        $val = $_GET[$name];
    }

    if (!isset($val)) {
        return addslashes($default);
    }

    if (is_array($val)) {
    	foreach( $val as $k=>$v ) {
    		$val[$k] = get_magic_quotes_gpc() ? stripslashes($v) : $v;
    	}
    } else {
    	$val = get_magic_quotes_gpc() ? stripslashes($val) : $val;
    }
    
    if (!is_array($filters)) {
        $filters = array($filters);
    }
    
    $realFilters = array();
    
    foreach($filters as $f) {
        if ($f == 'field') {
            $realFilters[] = 'trim';
            $realFilters[] = '_htmldecode';
            $realFilters[] = 'strip_tags';
            $realFilters[] = 'not2dots';
            $realFilters[] = 'max255';
        } elseif ($f == 'username') {
            $realFilters[] = 'trim';
            $realFilters[] = '_htmldecode';
            $realFilters[] = 'strip_tags';
            $realFilters[] = 'not2dots';
            $realFilters[] = 'strtolower';
            $realFilters[] = 'max255';
        } elseif ($f == 'password') {
            $realFilters[] = 'trim';
            $realFilters[] = 'max255';
        } elseif ($f == 'email') {
            $realFilters[] = 'trim';
            $realFilters[] = '_htmldecode';
            $realFilters[] = 'strip_tags';
            $realFilters[] = 'not2dots';
            $realFilters[] = 'max255';
        } elseif ($f == 'subject') {
            $realFilters[] = 'trim';
            $realFilters[] = 'strip_tags';
            $realFilters[] = 'htmlspecialchars_decode';
            $realFilters[] = 'max255';
        } elseif ($f == 'attr' || $f == 'attribute') {
            $realFilters[] = 'strtolower';
        } elseif ($f == 'htmlcode') {
            $realFilters[] = 'trim';
            $realFilters[] = '_htmldecode';
        } elseif ($f == 'url') {
            $realFilters[] = 'trim';
            $realFilters[] = '_htmldecode';
            $realFilters[] = 'strip_tags';
        } elseif ($f == 'filepath') {
            $realFilters[] = 'trim';
            $realFilters[] = '_htmldecode';
            $realFilters[] = 'strip_tags';
            $realFilters[] = '_path';
        } else {
            $realFilters[] = $f;
        }
    }
    
    foreach($realFilters as $fname) {
        if ($fname == 'alpha') {
            $val = preg_replace('/[^a-z\_]+/i', '', $val);

        } else if ($fname == 'alnum') {
            $val = preg_replace('/[^a-z0-9\_]+/i', '', $val);
        
        } else if ($fname == 'not2dots') {
            $val = preg_replace('/[:]+/i', '', $val);

        } else if ($fname == '_htmldecode') {
           if (is_array($val)) {
                foreach ($val as $k=>$v) {
                    $val[$k] = @html_entity_decode($v, ENT_COMPAT, 'UTF-8');
                }
            } else {
                $val = @html_entity_decode($val, ENT_COMPAT, 'UTF-8');
            }

        } else if ($fname == '_path') {
            $val = $am->getNormalizedPath( $val );

        } else if ($fname == 'max255') {
            if (is_array($val)) {
                foreach ($val as $k=>$v) {
                    $val[$k] = substr($v, 0, 255);
                }
            } else {
                $val = substr($val, 0, 255);
            }
            
        } else if ($fname == 'max4k') {
            if (is_array($val)) {
                foreach ($val as $k=>$v) {
                    $val[$k] = substr($v, 0, 4096);
                }
            } else {
                $val = substr($val, 0, 4096);
            }

        } else if (function_exists($fname)) {
        	if (!is_array($val)) {
            	$val = call_user_func($fname, $val);
        	} else {
        		foreach($val as $k=>$v) {
	            	$val[$k] = call_user_func($fname, $v);
        		}
        	}
    	}
    }

    return $val;
}
################################################################################
function htmlSubmit( $id, $opts=null ) 
{
    global $am;
    return htmlInput( 'submit', $id, $am->M('SAVECHANGES'), $opts );
}
function htmlReset( $id, $opts=null ) 
{
    global $am;
    return htmlInput( 'reset', $id, $am->M('RESET'), $opts );
}
function htmlInput( $type, $id, $title, $opts=null, $disableInDemo=false ) 
{
    global $am;

    $html = '<input type="' . $type . '"';
    if ($am->isDemo() && $disableInDemo) {
        $html .= ' disabled ';
    }
    if (isset($opts)) {
        $html .= ' ' . $opts;
    }

    $html .= " id=\"$id\" value=\"$title\" />";

    return $html;
}

function htmlSelectTemplateVar( $dst ) 
{
    global $am;
    
    echo '<select onchange="javascript:mainInsertAtCursor($(\''.$dst.'\'), this.value); $(\''.$dst.'_deftvar\').selected=true;">
                <option id="'.$dst.'_deftvar" value="">' . $am->M('SELECTONE') . '</option>
                <option value="%DATETIME%">Current Date/Time (GMT)</option>
                <option value="%REMOTEADDR%">Remote IP address</option>
                <option value="%PROTECTEDURL%">URL of protected directory</option>
                <option value="%MEMBERURL%">Member URL</option>
                <option value="">-------| User Variables |-------</option>
                <option value="%USERNAME%">Username</option>
                <option value="%USERREALNAME%">Real name</option>
                <option value="%USERPASSWORD%">User\'s password</option>
                <option value="%USERMAIL%">E-mail address of the user</option>
                <option value="">-------| Special variables |-------</option>
                <option value="%USERREQUESTSUBJECT%">Request Subject (contact form)</option>
                <option value="%USERREQUESTBODY%">Request Body (contact form)</option>
                <option value="%USERHASHOFPASS%">Used in password recovery form</option>
            </select>';
    return;
}
            
################################################################################
function utils_strip_path( $path, $dirsep = DIRECTORY_SEPARATOR )
{
    $path = str_replace('\\', '/', $path);
    $path = preg_replace('/\/\.+/', '', $path );

    if ($dirsep != '/') {
        $path = str_replace('/',  $dirsep, $path);
   }

    return $path;
}

##############################################################################

header('Pragma: public');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate'); 
header('Cache-Control: pre-check=0, post-check=0, max-age=0');
header('Pragma: no-cache');

session_start();

global $am;
$am = new Authman();

$page = getParam('page', 'home', 'attribute');

$fn = 'showPage_'.$page;
if (function_exists($fn)) {
    call_user_func( $fn );
} else {
    showPage_404();
}

exit;

/* the end of the file */
