<?php

### SETTINGS
include("../roster_functions.php");
$reps_file = "../reps.txt";


### define functions

# mail to the rep starting NOW
function mail_now($row){
	
	# php does not support accessing the elements from explode in one line, so you have to use the intermediate variable $tmp..
	$tmp = explode("\t", $row);
	$email = $tmp[1];
	
	$tmp = explode(" ", $tmp[0]);
	$firstname = $tmp[0];
	
	
	mail($email, 'Biosupport.se: Your support duty starts NOW', "Hello $firstname\n\nThis is a reminder that your support duty at http://biosupport.se starts now. During this week, it is your responsability to make sure every question asked gets a support representative assigned to it. To view all future support weeks, please see the roster homepage, http://roster.biosupport.se\n\nIf you have not done so already, you will need an account in the BILS issue tracker, http://projects.bils.se, and make sure you are added to the Biosupport-project.\n\nAfter you have done that you should make sure you will get an email for each new question at Biosupport:\nhttp://projects.bils.se/my/account\nunder the heading \"Email notifications\". If you set it to \"For any event on all my projects\", you should be covered. You can also set it to \"For any event on the selected projects only...\", and make sure \"Biosupport.se questions\" is ticked.\n\nFor more detailed instructions, please see this page: http://roster.biosupport.se/info\n\n\nIf you have any questions, don't hesitate to contact me!\n\nBest regards\nMartin Dahlö\nmartin.dahlo@scilifelab.uu.se\n+46 18 611 59 59\n");
	
}
 

# mail to the rep starting later
function mail_alert($row, $weeks){
	
	# php does not support accessing the elements from explode in one line, so you have to use the intermediate variable $tmp..
	$tmp = explode("\t", $row);
	$email = $tmp[1];
	
	$tmp = explode(" ", $tmp[0]);
	$firstname = $tmp[0];
	
	mail($email, 'Biosupport.se: Your support duty starts in '.($weeks*7)." days.", "Hello $firstname\n\nThis is a reminder that your support duty at http://biosupport.se starts in ".($weeks*7)." days. To view all future support weeks, please see the roster homepage, http://roster.biosupport.se\n\nIf you are unable to have support duty during that week, please switch week with someone else. That can be done using the support roster homepage, http://roster.biosupport.se/switch.php, or just work it out on your own.\n\nDuring the support duty week, it is your responsability to make sure every question asked gets a support representative assigned to it.\n\nIf you have not done so already, you will need an account in the BILS issue tracker, http://projects.bils.se, and make sure you are added to the Biosupport-project.\n\nAfter you have done that you should make sure you will get an email for each new question at Biosupport:\nhttp://projects.bils.se/my/account\nunder the heading \"Email notifications\". If you set it to \"For any event on all my projects\", you should be covered. You can also set it to \"For any event on the selected projects only...\", and make sure \"Biosupport.se questions\" is ticked.\n\nFor more detailed instructions, please see this page: http://roster.biosupport.se/info/\n\n\nIf you have any questions, don't hesitate to contact me!\n\nBest regards\nMartin Dahlö\nmartin.dahlo@scilifelab.uu.se\n+46 18 611 59 59\n");
	
}




# read the reps file to an array
$reps = array(); # initiate
$i = 0; # initiate
$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	$reps[$i] = trim($line); # remove all whitespace padding and newlines
	
	# increase the index if the previous line was not empty
	if($reps[$i]){
		$i++; # increase the index
	}
}
fclose($file_handle); # close the file handle


# prepare the data
array_pop($reps); # removes the last empty line (where does it come from?)


# take a backup of the rep file
copy("$reps_file", "reps_archive/".date("Y-W", time()).".txt");


# take one step
$old = array_shift($reps);
array_push($reps, $old);

# write the new file
# open the reps file for writing
$file_handle = fopen($reps_file, 'w') or die("Can't open file for reading\n"); # open the file
for($i = 0; $i < count($reps); $i++){
	
	# write the line
	fwrite($file_handle, $reps[$i]."\n");
}
fclose($file_handle); # close the file handle


# alert the user who has the comming week
mail_now($reps[0]);

# alert the other intervals
foreach($alerts as $alert){

	mail_alert($reps[$alert], $alert);
	
}




?>
