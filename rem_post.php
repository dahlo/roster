<?php

include("roster_functions.php");
require('htpasswd.php'); 

### SETTINGS
global $reps_file;
global $adminEmail;



# mail to the reps with ids in $r1 and the admin
function mail_remAndAdmin($reps, $r1, $adminEmail){
	
	# save removed users
	$remed = "";
	
	### For all removed reps
	foreach($r1 as $r_idx){
		
		$tmp = explode("\t", $reps[$r_idx]);
		$email = $tmp[1];
		$fullname = $tmp[0];
		$tmp = explode(" ", $tmp[0]);
		$firstname = $tmp[0];
		
		# save removed users
		$remed .= "<br>$fullname\n";
		
		mail($email, "Biosupport.se roster: You have been removed.", "Hello $firstname\n\nYou have been removed from the Biosupport roster.\n\n\nHave a nice day.");
		mail($adminEmail, "Biosupport.se roster: $fullname has been removed.", "Hello\n\n$fullname has been removed from the Biosupport roster.\n\n\nHave a smashing day.");
		
		
	}
	
	# redirect
	Print "<html><h1>Users removed:</h1>$remed<br><br></html>";
	
	# debug
	//~ print "Hello $firstname_r1\n\nYou have now switched weeks with $fullname_r2. The next week you will have support duty is $nextWeek_r1.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.\n\n\n\n";
	//~ print "Hello $firstname_r2\n\nYou have now switched weeks with $fullname_r1. The next week you will have support duty is $nextWeek_r2.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.\n";
	
}



# email the people who are affected by the removal
function mail_updatedWeeks($orgReps, $reps, $alerts){
	
	global $url;
	
	
	
	# get the list of reps that already have been mailed
	# returns an array with 2 slots, 0 is an array with rep rows, 1 is an array with the index numbers of the reps in 0
	$already_mailed = getRepInInterval($orgReps, max($alerts));
	
	# get the list of reps who should be mailed in the new list
	# returns an array with 2 slots, 0 is an array with rep rows, 1 is an array with the index numbers of the reps in 0
	$should_mail = getRepInInterval($reps, max($alerts));
	
	$remed = "";
	
	# find affected reps
	for($i = 0; $i < count($already_mailed); $i++){
		
		# if there is a shift in the arrays, mail everyone affected
		if($already_mailed[$i] != $should_mail[$i]){
			
			# get the persons info
			$affected = $should_mail[$i];
			
			
			$tmp = explode("\t", $affected);
			$email = $tmp[1];
			$fullname = $tmp[0];
			$tmp = explode(" ", $tmp[0]);
			$firstname = $tmp[0];
			$next_week = getRepWeeks($reps, $i, 1);
			
			$remed .= "$fullname\t$next_week<br>\n";
			
			# check if it was the rep who has support duty right now that was removed. Send a special mail to the one who has to take over
			if($i == 0){
				
				# mail the affected person with updated week numbers
				mail($email, "Biosupport.se roster: You have to take over the current support duty.", "Hello $firstname\n\nThere has been a change in the Biosupport roster, and it seems as if the person who currently have support duty has been removed from the list. Someone else has to take over, and you are next in line.\n\nIf this is a problem for you, please find someone to change weeks with and report the change here: $url/switch.php\n\n\nHave a nice day.");
				
			# else, send the ordinary mail to the others
			}else{
				
				# mail the affected person with updated week numbers
				mail($email, "Biosupport.se roster: Your duty week has been changed (Next week you'll have support duty: $next_week)", "Hello $firstname\n\nThere has been a change in the Biosupport roster, and your week has been changed. The next week you will have support duty is $next_week.\n\nIf this is a problem for you, please find someone to change weeks with and report the change here: $url/switch.php\n\n\nHave a nice day.");
			}
		}
	}
		
	
	# redirect
	Print "<html><h1>Users mailed:</h1>$remed<br><br>Redirecting to addrem page in 4 seconds.</html>";
	
	# debug
	//~ print "Hello $firstname_r1\n\nYou have now switched weeks with $fullname_r2. The next week you will have support duty is $nextWeek_r1.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.\n\n\n\n";
	//~ print "Hello $firstname_r2\n\nYou have now switched weeks with $fullname_r1. The next week you will have support duty is $nextWeek_r2.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.\n";
	
}





# function to remove users from the htpasswd file
function htpasswd_rem($rep){
	
	# import the htpasswd functions
	#require('htpasswd.php'); 
	
	$pwmanager = new htpasswd('.htpasswd'); 
	//~ $pwmanager->create('test', 'pass');
	
	# get the first name
	$tmp = explode("\t", $rep);
	$email = strtolower($tmp[1]);
	
	
	# check if the username is already taken
	if( (in_array($email, $pwmanager->emails())) ){
		
		# get the index of the user
		$i = array_search($email, $pwmanager->emails());
		
		# remove the user
		$tmp = $pwmanager->users();
		$pwmanager->remove($tmp[$i]);
		
	}
	
	# save the file
	$pwmanager->save(); 
	
	return 'done';
	
}






# check that both reps are selected
if(!isset($_POST['rem1'])){
	die("Nothing in leftmost checkboxes selected.");
}
if(!isset($_POST['rem2'])){
	die("Nothing in rightmost checkboxes selected.");
}

# rename variables
$r1 = $_POST['rem1'];
$r2 = $_POST['rem2'];


### make sure both boxes are ticked for each name

# for each ticked box in leftmost column
foreach($r1 as $r1_idx){
	
	# if the values is not in the other array
	if(!(in_array($r1_idx, $r2))) {
	    die("Failed to tick both boxes for a name!");
	}
	
}

# for each ticked box in rightmost column
foreach($r2 as $r2_idx){
	
	# if the values is not in the other array
	if(!(in_array($r2_idx, $r1))) {
	    die("Failed to tick both boxes for a name!");
	}
	
}


# read the reps file to an array
$reps = array(); # initiate
$i = 0; # initiate
$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	$reps[$i] = trim($line); # remove all whitespace padding and newlines
	
	# increase the index if the previous line was not empty
	if($reps[$i]){
		$i++; # increase the index
	}
}
fclose($file_handle); # close the file handle

# prepare the data
array_pop($reps); # removes the last empty line

# save a copy for later
$orgReps = $reps;



# remove the selected positons
# for each ticked box in leftmost column
foreach($r1 as $r1_idx){
	
	# remove the rep from the htpasswd file
	$test = htpasswd_rem($reps[$r1_idx]);
	
	# remove if the previous step was successful
	if($test == 'done'){
		# delete the rep
		unset($reps[$r1_idx]);
	}
}



# print the new file
$file_handle = fopen($reps_file, 'w') or die("Can't open file for reading\n"); # open the file
foreach($reps as $line){
	
	# write the line
	fwrite($file_handle, $line."\n");
}
fclose($file_handle); # close the file handle

# mail the affected users
mail_remAndAdmin($orgReps, $r1, $adminEmail);




# read the new reps file to an array
$reps = array(); # initiate
$i = 0; # initiate
$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	$reps[$i] = trim($line); # remove all whitespace padding and newlines
	
	# increase the index if the previous line was not empty
	if($reps[$i]){
		$i++; # increase the index
	}
}
fclose($file_handle); # close the file handle

# prepare the data
array_pop($reps); # removes the last empty line




### mail update weeks to affected users
mail_updatedWeeks($orgReps, $reps, $alerts);

header('Refresh: 4; url=addrem.php');

?>
