<?php

include("roster_functions.php");


### SETTINGS
$reps_file = "reps.txt";
if(array_key_exists('nrPerWeek', $_POST)){$nrWeeksPerWeek = min((integer)$_POST['nrPerWeek'],100); }else{$nrWeeksPerWeek = 8;}; # get the number of weeks, or set it to 8
$dev = 0; # devel variable. Adds $dev weeks to the time
$week = 604800; # seconds in a week
$currentDate = time() + $dev*$week;
global $reps_file;



# read the reps file to an array
$reps = array(); # initiate
$i = 0; # initiate
$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	$reps[$i] = trim($line); # remove all whitespace padding and newlines
	
	# increase the index if the previous line was not empty
	if($reps[$i]){
		$i++; # increase the index
	}
}
fclose($file_handle); # close the file handle


# prepare the data
array_pop($reps); # removes the last empty line (where does it come from?)



# print the roster
#print "<table style='width:30%;margin:0 0 10px 0;text-align:left;border-collapse: collapse; margin-right: auto; margin-left: auto;'>"; # start the table
print "<h4>Per week</h4>";
print "<p align=\"right\">Current week: ".date("W", $currentDate)."</p>";
print "<i>Sorted by date</i>";
print "<table class='perWeek'>\n"; # start the table
print "<tr><td><center><b>Week</b></center></td><td><center><b>Name</b></center></td></tr>\n"; # print header

# text settings
$boldTextSize = 4;
$dateTextSize = 0;

for($i = -1 ; $i < $nrWeeksPerWeek ; $i++){ # for each week that should be plotted
	
	# (date("W", $currentDate + $i*$week)) = the current week, minnus $i number of weeks (epoch time)
	# $reps[( ($iterator+$i) %  (count($reps) - 2) + 1)] = gets the index in the array with modulus and shifts it to avoid the first row in the file, and the last row in the file
	# explode(.....) get the name only. Don't print the email
	
	
	# highlight the current week
	if((date("Y - W", $currentDate + $i*$week)) == date("Y - W", time() + $dev*$week) ){

		
		# print the current week in bold and large text
		print "<tr><td><center><b><font size='$boldTextSize'>".(date("Y - W", $currentDate + $i*$week))."</font></b><br><font size='$dateTextSize'>".date('d M', strtotime(date("Y", $currentDate + $i*$week)."W".date("W", $currentDate + $i*$week)."1"))." - ".date('d M', strtotime(date("Y", $currentDate + $i*$week)."W".date("W", $currentDate + $i*$week)."7"))."</font></center></td><td><center><b><font size='$boldTextSize'>".getRepName($reps, $i)."</font></b></center></td></tr>\n";
		
	}else{ # if it is any other week
	
		# print the week and name with normal text
		print "<tr><td><center>".(date("Y - W", $currentDate + $i*$week))."<br><font size='$dateTextSize'>".date('d M', strtotime(date("Y", $currentDate + $i*$week)."W".date("W", $currentDate + $i*$week)."1"))." - ".date('d M', strtotime(date("Y", $currentDate + $i*$week)."W".date("W", $currentDate + $i*$week)."7"))."</font></center></td><td><center>".getRepName($reps, $i)."</center></td></tr>\n";
	}
}

# end the table and print the length control
print "</table>

<center><form action='index.php' method='post' enctype='multipart/form-data'>

	Show me <input type='text' name='nrPerWeek' size='3' value='$nrWeeksPerWeek'> weeks

	<input type='submit' value='Go ' style='height: auto; width: auto'>

</form></center>";


# print a break
print "<br><br><br><br>\n";



# print for each user which weeks they have
print "<h4>Per user</h4>";
print "<i>Sorted by users name</i>";
print "<table class='perUser'>\n"; # start the table
print "<tr><td><center><b>Name</b></center></td><td><center><b>Weeks</b></center></td></tr>\n"; # print header

# sort the array after name, keeping the name index numbers
asort($reps);

# get how many weeks sould be displayed
if(array_key_exists('nrPerUser', $_POST)){$nrWeeksPerUser = min((integer)$_POST['nrPerUser'], 100); }else{$nrWeeksPerUser = 4;}; # get the number of weeks, or set it to 8

# for each name
foreach($reps as $pos => $name){
	
	# remove the email
	$tmp = explode("\t", $name);
	$name = $tmp[0];
	
	# print the name
	print "<tr><td><center>$name</center></td>";
	
	# print the weeks
	print "<td><center>".getRepWeeks($reps, $pos, $nrWeeksPerUser)."</center></td></tr>\n";
	
}


# end the table and print the length control
print "</table>

<center><form action='index.php' method='post' enctype='multipart/form-data'>

	Show me <input type='text' name='nrPerUser' size='3' value='$nrWeeksPerUser'> weeks

	<input type='submit' value='Go ' style='height: auto; width: auto'/>

</form></center>\n";



?>
