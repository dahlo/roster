<?php

include("roster_functions.php");

### SETTINGS
global $reps_file;



# mail to the reps with id $r1 and $r2
function mail_both($reps, $r1_idx, $r2_idx){
	
	global $url;
	
	# get the next week they will have
	$nextWeek_r1 = getRepWeeks($reps, $r1_idx, 1);
	$nextWeek_r2 = getRepWeeks($reps, $r2_idx, 1);
	
	# get the rep rows
	$r1 = $reps[$r1_idx];
	$r2 = $reps[$r2_idx];
	
	### For both reps
	
	$tmp = explode("\t", $r1);
	$email_r1 = $tmp[1];
	$fullname_r1 = $tmp[0];
	$tmp = explode(" ", $tmp[0]);
	$firstname_r1 = $tmp[0];
	
	$tmp = explode("\t", $r2);
	$email_r2 = $tmp[1];
	$fullname_r2 = $tmp[0];
	$tmp = explode(" ", $tmp[0]);
	$firstname_r2 = $tmp[0];
	
	mail($email_r1, "Biosupport.se roster: changed weeks (Next is $nextWeek_r1)", "Hello $firstname_r1\n\nYou have now switched weeks with $fullname_r2. The next week you will have support duty is $nextWeek_r1.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.");
	mail($email_r2, "Biosupport.se roster: changed weeks (Next is $nextWeek_r2)", "Hello $firstname_r2\n\nYou have now switched weeks with $fullname_r1. The next week you will have support duty is $nextWeek_r2.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.");
	
	Print "<html>\n<h1>$fullname_r1 switched with $fullname_r2.</h1>\nBoth users have been emailed about the change.<br><br>\nRedirecting to main page in 3 seconds.\n</html>";

	header('Refresh: 3; url=index.php');
	
	# debug
	//~ print "Hello $firstname_r1\n\nYou have now switched weeks with $fullname_r2. The next week you will have support duty is $nextWeek_r1.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.\n\n\n\n";
	//~ print "Hello $firstname_r2\n\nYou have now switched weeks with $fullname_r1. The next week you will have support duty is $nextWeek_r2.\n\nTo see all the weeks you have, please visit the support duty roster: $url\n\n\nHave a nice day.\n";
	
}



# check that both reps are selected
if(!isset($_POST['rep1'])){
	die("Person 1 not selected");
}
if(!isset($_POST['rep2'])){
	die("Person 2 not selected");
}


# read the reps file to an array
$reps = array(); # initiate
$i = 0; # initiate
$file_handle = fopen($reps_file, 'r') or die("Can't open file for reading\n"); # open the file
while (!feof($file_handle)) { # for each line in the file
	$line = fgets($file_handle); # get the line
	$reps[$i] = trim($line); # remove all whitespace padding and newlines
	
	# increase the index if the previous line was not empty
	if($reps[$i]){
		$i++; # increase the index
	}
}
fclose($file_handle); # close the file handle

# prepare the data
array_pop($reps); # removes the last empty line

# switch the positions
$temp = $reps[$_POST['rep1']];
$reps[$_POST['rep1']] = $reps[$_POST['rep2']];
$reps[$_POST['rep2']] = $temp;


# print the new file
$file_handle = fopen($reps_file, 'w') or die("Can't open file for reading\n"); # open the file
foreach($reps as $line){
	
	# write the line
	fwrite($file_handle, $line."\n");
}
fclose($file_handle); # close the file handle

# mail the affected users
mail_both($reps, $_POST['rep1'], $_POST['rep2']);

?>
