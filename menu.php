<?php

# get the current page you are on
$currentFile = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentFile);
$current = $parts[count($parts) - 1];



# print the menu, highlighting the page you are on
# Just mimic the way I have written them
print '

    <div class="col_12">
    <!-- Menu Horizontal -->
        <ul class="menu">
        <li';if ($current=='index.php') {print ' class="current"';} print '><a href="index.php"><span class="icon" data-icon="6"></span>Roster</a></li>
        <li';if ($current=='skills.php') {print ' class="current"';} print '><a href="skills.php"><span class="icon" data-icon="u"></span>Skills list</a></li>
        <li';if ($current=='switch.php') {print ' class="current"';} print '><a href="switch.php"><span class="icon" data-icon="S"></span>Switch weeks</a></li>
        <li';if ($current=='faq.php') {print ' class="current"';} print '><a href="http://roster.biosupport.se/info/" target="_blank"><span class="icon" data-icon="i"></span>FAQ</a></li>
       ';# <li';if ($current=='addrem.php') {print ' class="current"';} print '><a href="addrem.php"><span class="icon" data-icon="Z"></span>Add/Remove users</a></li>
print '        </ul>';


?>
