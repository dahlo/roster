<!DOCTYPE html>
<html><head>

<!-- change the title of the page -->
<title>SBS - Support Duty Roster</title>

<!-- edit the top of the page in head.php -->
<?php include('head.php');?>

<!-- edit menu in menu.php -->
<?php include('menu.php');?>


<!-- include the roster itself -->
<?php include('roster.php');?>

<br>

<!-- edit footer in foot.php -->
<?php include('foot.php');?>


<!-- You can comment things out (make them disappear) like this -->
<?php #include("foot.php");?>
